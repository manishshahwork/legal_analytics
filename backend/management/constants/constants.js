module.exports = {
    ENV: 'local', // local, dev, prod
    USER_ROLES: {
        ADMIN: 'admin',
        VENDOR: 'vendor',
        CONSUMER: 'consumer',
    },
    COUPON_CATEGORY: {
        GENERAL: 'general'
    }, 
    DISCOUNT_TYPE: {
        DISCOUNT: 'discount',
        PRICE_SLASH: 'price'
    },
    COOKIE: {
        NAME: {
            ROLE: 'r',
            USER: 'user'
        },
        VALUE: {
            VENDOR: 'v',
            CONSUMER: 'c'
        }
    },
    COUPON_TYPE: {
        COUPON: 'coupon',
        REWARD: 'reward'
    },
    TRANSACTION_TYPE: {
        EARNING: 'earning',
        EXPENSE: 'expense'
    },
    SECRET_KEY : "KRYPTO_SECRET"

}