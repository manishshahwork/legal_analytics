let constants = require('./constants');
class Keys {
    constructor() { }

    static getGoogleKeys() {
        if (constants.ENV === 'prod') {
            return KEYS.PROD.GOOGLE;
        } else if (constants.ENV === 'dev') {
            return KEYS.DEV.GOOGLE;
        } else {
            return KEYS.LOCAL.GOOGLE;
        }
    }

    static getFacebookKeys() {
        if (constants.ENV === 'prod') {
            return KEYS.PROD.FACEBOOK;
        } else if (constants.ENV === 'dev') {
            return KEYS.DEV.FACEBOOK;
        } else {
            return KEYS.LOCAL.FACEBOOK;
        }
    }

    static getMongoDBURL() {
        if (constants.ENV === 'prod') {
            return KEYS.PROD.MONGO_DB_URL;
        } else if (constants.ENV === 'dev') {
            return KEYS.DEV.MONGO_DB_URL;
        } else {
            return KEYS.LOCAL.MONGO_DB_URL;
        }
    }

    static getPorts() {
        return {
            http: KEYS.LOCAL.PORT.HTTP,
            https: KEYS.LOCAL.PORT.HTTPS
        }
    }

    static getSSLFiles() {
        if (constants.ENV === 'prod') {
            return {
                crtFile: KEYS.PROD.SSL_CERT_FILE,
                keyFile: KEYS.PROD.SSL_KEY_FILE
            }
        } else if (constants.ENV === 'dev') {
            return {
                crtFile: KEYS.DEV.SSL_CERT_FILE,
                keyFile: KEYS.DEV.SSL_KEY_FILE
            }
        } else {
            return {
                crtFile: KEYS.LOCAL.SSL_CERT_FILE,
                keyFile: KEYS.LOCAL.SSL_KEY_FILE
            }
        }
    }

    static getSessionCookieKeys() {
        return KEYS.SESSION.COOKIE_KEYS;
    }

    static getUnprotectedRoutes() {
        return KEYS.UNPROTECTED_ROUTES;
    }

    static getJWTTokenSecrete() {
        return KEYS.TOKEN_SECRETE;
    }
}

module.exports = Keys;

let KEYS = {
    PROD: {
        GOOGLE: {
            clientID: '',
            clientSecret: '',
            callbackURL: ''
        },

        FACEBOOK: {
            clientID: '',
            clientSecret: '',
            callbackURL: ''
        },
        MONGO_DB_URL: ''
    },
    DEV: {
        GOOGLE: {
            clientID: '169403599546-hqh6upgm4qvp6j8jl0r7lo4r4ip8m0p9.apps.googleusercontent.com',
            clientSecret: 'RilIRpSVAarnz6n0siraFPN5',
            callbackURL: 'https://beta.rewardx.io/auth/login/google/callback'
        },

        FACEBOOK: {
            clientID: '247970072621425',
            clientSecret: 'b1ccc4b0e7fb15a5f6fc58015e0e965c',
            callbackURL: 'https://beta.rewardx.io/auth/login/facebook/callback'
        },
        MONGO_DB_URL: 'mongodb://rewardx:rewardx$111@13.127.179.135:27017/admin',  // for aws need to uncomment
        SSL_CERT_FILE: '/etc/letsencrypt/live/beta.rewardx.io/fullchain.pem',
        SSL_KEY_FILE: '/etc/letsencrypt/live/beta.rewardx.io/privkey.pem',
        //MONGO_DB_URL: 'mongodb://localhost:27017/admin', // for local need to uncomment
    },
    LOCAL: {
        GOOGLE: {
            clientID: '169403599546-8th6keuuhdoe298si8sacsmmu4hrccc5.apps.googleusercontent.com',
            clientSecret: 'INEAH6DOib7coeaAnzcQG9AU',
            callbackURL: 'https://localhost:8443/auth/login/google/callback'
        },

        FACEBOOK: {
            clientID: '384290672053822',
            clientSecret: 'cdf5dc366b166c4fe224905cc34e750b',
            callbackURL: 'https://localhost:8443/auth/login/facebook/callback'
        },
        //MONGO_DB_URL: 'mongodb://rewardx:rewardx$111@beta.rewardx.io:27017/admin', // for aws need to uncomment
        MONGO_DB_URL: 'mongodb://localhost:27017/admin',   // for local need to uncomment
        PORT: {
            HTTP: 3000,
            HTTPS: 8443
        },
        SSL_CERT_FILE: '../dev-files/key.pem',
        SSL_KEY_FILE: '../dev-files/cert.pem'
    },
    SESSION: {
        COOKIE_KEYS: ['krypto12345']
    },
    TOKEN_SECRETE: 'krypto12345',
    UNPROTECTED_ROUTES: ['/auth/login', '/auth/login/vendor', '/auth/login/google', '/auth/login/google/callback', '/auth/login/facebook', '/auth/login/facebook/callback']
}