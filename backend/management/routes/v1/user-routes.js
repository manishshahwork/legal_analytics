let express = require('express');
let router = express.Router();
let User = require('./../../models/User');
let Image = require('./../../models/Image');
let logger = require('./../../libs/Logger');
let Coupon = require('./../../models/Coupon');
let Vendor = require('./../../models/Vendor');
let response = require('./../../libs/Response');
let Subscription = require('./../../models/Subscription');
let constants = require('./../../constants/constants');
let UserProfile = require('../../models/UserProfileSchema');
let NewVendorProfile = require('../../models/NewVendorProfileSchema');
let CompanyCategory = require('../../models/CompanyCategory');
let CompanyRole = require('../../models/CompanyRole');
let PagePermission = require('../../models/PagePermission');
let Utils = require('./../../utils/Utils');
const jwt = require('jwt-simple');

router.get('/me', async function (req, res) {
    console.log('req.user' + JSON.stringify(req.query));
    try {
        let result = await User.findOne(req.query);
        console.log('result' + result);
        response.success(req, res, null, result);
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, null, null);
    }
});


router.put('/update/me', async function (req, res) {
    console.log(req.body.uuid);
    try {
        if (!req.body.uuid) {
            response.badRequest(req, res, null, null);
        } else {
            // ** warning: add object filtering for update properties

            let result = await User.update({ uuid: req.body.uuid }, req.body.update_props);
            logger.success(req, res, req.body);
            response.success(req, res, null, result);
        }
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, null, null);
    }
});

router.get('/coupons', async function (req, res) {
    try {
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, null, null);
    }
});

router.post('/subscriptions/vendors', async function (req, res) {
    try {
        let result = await Subscription.subscribeUserToVendor(req.body.user_uuid, req.body.vendor_uuid, req.body.type);
        logger.success(req, res, req.body);
        response.success(req, res, null, result);
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, null, null);
    }
});

router.delete('/subscriptions/vendors', async function (req, res) {
    try {
        let props = { user_uuid: req.query.user_uuid, vendor_uuid: req.query.vendor_uuid };
        // let props = { user_uuid: req.body.user_uuid, vendor_uuid: req.body.vendor_uuid };
        // console.log(props);
        let result = await Subscription.delete(props);
        logger.success(req, res, req.body);
        response.success(req, res, null, result);
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, null, null);
    }
});

router.get('/:userID/subscriptions/vendors', async function (req, res) {
    try {
        let arrDistinctVendorUUID = await Subscription.getUniqueVendorUUIDSubscribedByUser(req.params.userID);
        let vendors = [];
        for (let i in arrDistinctVendorUUID) {
            let tempObj = {
                vendor: {},
                profile: {}
            }
            tempObj.vendor = (await Vendor.getAllVendorsWithFilter({ uuid: arrDistinctVendorUUID[i] }))[0];
            tempObj.profile = (await Vendor.getProfile({ vendor_uuid: arrDistinctVendorUUID[i] }))[0];
            vendors.push(tempObj);
        }
        response.success(req, res, null, vendors);
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, null, null);
    }
});

// return vendors according to their distance from user
router.get('/:userID/vendors/near', async function (req, res) {
    try {
        let profile = await Vendor.getProfile({});
        response.success(req, res, null, profile);
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, null, null);
    }
});

// return vendors according to their distance from user
router.get('/:userID/vendors/:vendorID/details', async function (req, res) {
    try {
        let vendorUUID = req.params.vendorID;
        let userUUID = req.params.userID;
        let result = (await Vendor.getProfile({ vendor_uuid: vendorUUID }))[0];
        let vendor = (await Vendor.getAllVendorsWithFilter({ uuid: vendorUUID }))[0];
        let isUserSubscribedToThisVendor = await Subscription.isSubscriptionExists(userUUID, vendorUUID);
        let details = Object.assign(vendor.toObject());
        details.profile = result;
        details.isSubscribed = isUserSubscribedToThisVendor ? true : false;
        console.log(details);
        response.success(req, res, null, details);
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, null, null);
    }
});


// returns all coupons of subscribed vendors
router.get('/:userID/subscriptions/coupons', async function (req, res) {
    try {
        let arrDistinctVendorUUID = await Subscription.getUniqueVendorUUIDSubscribedByUser(req.params.userID);
        let vendors = [];
        for (let i in arrDistinctVendorUUID) {
            let tempObj = {
                vendor: {},
                profile: {},
                coupons: []
            }
            tempObj.vendor = (await Vendor.getAllVendorsWithFilter({ uuid: arrDistinctVendorUUID[i] }))[0];
            tempObj.profile = (await Vendor.getProfile({ vendor_uuid: arrDistinctVendorUUID[i] }))[0];
            tempObj.coupons = await Coupon.getCouponsUploadedByVendor(arrDistinctVendorUUID[i])
            vendors.push(tempObj);
        }
        //let result = await new User().getOffersFromRegisteredVendors(req.params.userID, constants.COUPON_TYPE.COUPON);
        response.success(req, res, null, vendors);
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, null, null);
    }
});

// returns all rewards of subscribed vendors
router.get('/:userID/subscriptions/rewards', async function (req, res) {
    try {
        let arrDistinctVendorUUID = await Subscription.getUniqueVendorUUIDSubscribedByUser(req.params.userID);
        let vendors = [];
        for (let i in arrDistinctVendorUUID) {
            let tempObj = {
                vendor: {},
                profile: {},
                rewards: []
            }
            tempObj.vendor = (await Vendor.getAllVendorsWithFilter({ uuid: arrDistinctVendorUUID[i] }))[0];
            tempObj.profile = (await Vendor.getProfile({ vendor_uuid: arrDistinctVendorUUID[i] }))[0];
            tempObj.rewards = await Coupon.getRewardsUploadedByVendor(arrDistinctVendorUUID[i])
            vendors.push(tempObj);
        }
        //let result = await new User().getOffersFromRegisteredVendors(req.params.userID, constants.COUPON_TYPE.COUPON);
        response.success(req, res, null, vendors);
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, null, null);
    }
});

//////////////////////
//////////////////////
router.post("/authenticateGoogleUser", async function (req, res) {
    logger.logString("Enter:authenticateGoogleUser()");
    logger.logObj(req.body);
    let isUserAvailable = false;
    //check if the google email exists and if not then add it 
    //send 1 if email exists and 0 otherwise
    try {
        let foundUser = await UserProfile.findOne({ 'google_id': req.body.google_id });
        logger.logString("Before foundUser:: " + foundUser);
        logger.logObj(foundUser);
        if (foundUser == null) {
            //google user not found in db
            isUserAvailable = false;
            foundUser = new UserProfile(req.body);
            //get unique uuid
            foundUser.uuid = Utils.getUUID();
            await foundUser.save();
            logger.logString("Middle foundUser:: " + foundUser);
            //create new vendor details for this new user
            let newVendorProfile = new NewVendorProfile();
            logger.logString("newVendorProfile:: " + newVendorProfile);
            newVendorProfile.uuid = Utils.getUUIDV4();
            newVendorProfile.user_uuid = foundUser.uuid;
            logger.logString("New Vendor Profile to be created: " + newVendorProfile);
            let t = await newVendorProfile.save();
            logger.logString("Saved vendor Profile:::" + t);
        } else {
            //google user found in db
            isUserAvailable = true;
        }
        logger.logString("After: foundUser:: " + foundUser);
        logger.logObj(foundUser);

        //build secret token
        let payload = { user_uuid: foundUser.uuid };
        let secret = constants.SECRET_KEY;
        // encode
        var token = jwt.encode(payload, secret);
        response.success(req, res, token, foundUser);
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, "error", error);
    }

});


//set profile update flag of user
router.post("/updateCompletedProfile/:userId", async function (req, res) {
    logger.logString("Enter: profileUpdated");
    try {
        logger.logObj(req.params);
        logger.logObj(req.body);
        let userId = req.params.userId;
        let updateFlag = req.body.updateFlag;
        await UserProfile.findOneAndUpdate({ 'uuid': userId }, { 'has_completed_profile': updateFlag });
        response.success(req, res, "Success in updating user profile flag");
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, "Error in updating user profile flag", error);
    }
});

//get user profile
router.get("/profile/:user_uuid", async function (req, res) {
    logger.logString("Enter: user-profile");
    logger.logString(req.params);
    try {
        // let token = req.params.token;
        // logger.logString(token);
        // // decode
        // var decoded = jwt.decode(token, constants.SECRET_KEY);
        // console.log("DECODED...");
        // logger.logObj(decoded);
        // //get the user id from it and get info from db
        // let userId = decoded.user_uuid;
        // logger.logString("userId: " + userId);

        // let userProfile = await UserProfile.findById(req.params.user_uuid);
        let userProfile = await UserProfile.findOne({ 'uuid': req.params.user_uuid });
        logger.logString("userProfile:");
        logger.logObj(userProfile);
        response.success(req, res, "user profile", userProfile);
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, "error", error);
    }

});

// //get user profile
// router.get("/profile/:token", async function (req, res) {
//     logger.logString("Enter: user-profile");
//     try {
//         let token = req.params.token;
//         logger.logString(token);
//         // decode
//         var decoded = jwt.decode(token, constants.SECRET_KEY);
//         console.log("DECODED...");
//         logger.logObj(decoded);
//         //get the user id from it and get info from db
//         let userId = decoded.user_uuid;
//         logger.logString("userId: " + userId);
//         let userProfile = await UserProfile.findById(userId);
//         logger.logString("userProfile:");
//         logger.logObj(userProfile);
//         logger.logString(userProfile.email);

//         response.success(req, res, "user profile", userProfile);
//     } catch (error) {
//         logger.error(req, res, req.body, error);
//         response.serverError(req, res, "error", error);
//     }

// });


//get global data from token
router.get("/getGlobalData/:token", async function (req, res) {
    logger.logString("Enter: getGlobalData");
    try {
        let token = req.params.token;
        logger.logString(token);

        // decode
        var decoded = jwt.decode(token, constants.SECRET_KEY);
        console.log("DECODED...");
        logger.logObj(decoded);
        //get the user id from it and get info from db
        let userId = decoded.user_uuid;
        logger.logString("userId: " + userId);

        //get vendor id and other details as required
        let vendor = await NewVendorProfile.findOne({ 'user_uuid': userId });
        // logger.logStringWithObj("vendor:", vendor);
        // logger.logString("uuid:" + vendor.uuid);
        // logger.logString("created_on:" + vendor.created_on);
        // logger.logString("user_uuid:" + vendor.user_uuid);
        let vendor_uuid = vendor.uuid;
        logger.logStringWithObj("vendor id:", vendor_uuid);
        let data1 = { 'user_uuid': userId, 'vendor_uuid': vendor_uuid };
        response.success(req, res, "Got Global Data", data1);
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, "error", error);
    }
});

//create new user
router.post("/createNewUser", async function (req, res) {
    logger.logString("Enter: createNewUser");
    try {
        logger.logObj(req.params);
        logger.logObj(req.body);
        let userProfile = new UserProfile(req.body);
        //first check if the user exists
        let result = await UserProfile.findOne({
            'login_name': userProfile.login_name,
            'password': userProfile.password,
            'role': userProfile.role
        });
        logger.logStringWithObj("createNewUser result", result);
        if (result != null) {
            throw new Error('User Already Exists');
        }
        //set uuid for user
        userProfile.uuid = await Utils.getUUIDV4();
        logger.logStringWithObj("createNewUser new uuid", userProfile.uuid);
        result = await userProfile.save();
        logger.logStringWithObj("save user result", result);
        response.success(req, res, "Success in creating new user", result);

    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, "Error in createNewUser", error);
    }
});

//update user
router.post("/updateUser", async function (req, res) {
    logger.logString("Enter: updateUser");
    try {
        logger.logObj(req.params);
        logger.logObj(req.body);
        let result = await UserProfile.updateOne({ 'uuid': req.body.uuid }, req.body);
        logger.logStringWithObj("update user", result);
        response.success(req, res, "Success in updateUser", JSON.stringify(result));

    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, "Error in updateUser", error);
    }
});

//authenticate user based on login credentials
router.post("/authenticateUser", async function (req, res) {
    logger.logString("Enter: authenticateUser");
    try {
        logger.logObj(req.params);
        logger.logObj(req.body);
        let userProfile = new UserProfile(req.body);
        //first check if the user exists
        let result = await UserProfile.findOne({
            'login_name': userProfile.login_name,
            'password': userProfile.password,
            'role': userProfile.role
        });
        logger.logStringWithObj("authenticateUser result", result);
        if (result != null) {
            //build secret token
            let payload = { user_uuid: userProfile.uuid };
            let secret = constants.SECRET_KEY;
            // encode
            var token = jwt.encode(payload, secret);
            response.success(req, res, token, result);
        } else {
            response.success(req, res, null, null);
        }
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, "Error in authenticateUser", error);
    }
});

//////////////////////page permissions
//get all company categories
router.get("/getCompanyCategories/:user_uuid", async function(req, res){
    logger.logString("Enter: getCompanyCategories");
    logger.logString(req.params);
    try {
        let result = await CompanyCategory.find({'user_uuid' : req.params.user_uuid});
        logger.logStringWithObj("getCompanyCategories : result:", result);
        response.success(req, res, "All Company Categories", result);
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, "error", error);
    }
});

//get all company roles for category
router.get("/getCompanyRoles/:company_category_uuid", async function(req, res){
    logger.logString("Enter: getCompanyRoles");
    logger.logString(req.params.company_category_uuid);
    try {
        let result = await CompanyRole.find({'company_category_uuid' : req.params.company_category_uuid});
        logger.logStringWithObj("getCompanyRoles : result:", result);
        response.success(req, res, "All Company Roles", result);
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, "error", error);
    }
});

//get all permissions for role
router.get("/getPermissions/:company_role_uuid", async function(req, res){
    logger.logString("Enter: getPermissions");
    logger.logString(req.params.company_role_uuid);
    try {
        let result = await PagePermission.find({'company_role_uuid' : req.params.company_role_uuid});
        logger.logStringWithObj("getPermissions : result:", result);
        response.success(req, res, "All Company Roles", result);
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, "error", error);
    }
});

//updatePermissionsArray
router.post("/updatePermissionsArray", async function(req, res){
    logger.logString("Enter: updatePermissionsArray");
    logger.logObj(req.body);
    try {
        let pagePermissionArr = req.body;
        //iterate over each pagepermission and update it
        for (let k = 0; k < pagePermissionArr.length; k++) {
            var mod = new PagePermission(pagePermissionArr[k]);
            PagePermission.findOneAndUpdate({ '_id': pagePermissionArr[k]._id }, mod, function (err, c) {
                if (err) {
                    console.log(err);
                }
            });
        }
        // let result = await PagePermission.updateMany({}, req.body);
        response.success(req, res, "Success in updating Permissions", null);
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, "error", error);
    }
});

module.exports = router;