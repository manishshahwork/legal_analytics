// let express = require('express');
// let Coupon = require('./../../models/Coupon');
// let User = require('./../../models/User');
// let logger = require('./../../libs/Logger');
// let response = require('./../../libs/Response');
// let Utils = require('./../../utils/Utils');
// let Image = require('./../../models/Image');
// let router = express.Router();
// const request = require('request');
// let Blockchain = require('./../../models/BlockchainManager');
// const uuid = require('uuid');

// var createRewardData = {};
// var createCouponData = {};

// // request('http://13.127.179.135:3010/api/v1/vendors/tb', function (error, response, body) {
// //   console.log('error:', error); // Print the error if one occurred
// //   console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received
// //   console.log('body:', body); // Print the HTML for the Google homepage.
// // });

// router.post('/', async function (req, res) {
//     console.log("5555555555555555555555");
//     logger.logObj(req.body);
//     try {
//         if (!req.body.uuid) {
//             logger.logString("error as uuid is not found");
//             return response.badRequest(req, res, 'Bad request', null);
//         }
//         req.body.reward_points = req.body.reward_points || 0;
//         req.body.reward_points = parseFloat(req.body.reward_points);
//         let couponCount = parseInt(req.body.numberOfCoupons);
//         createCouponData.rewardnameProps = req.body.name;
//         createCouponData.tokenamountProps = req.body.reward_points;
//         createCouponData.validityProps = 10;
//         // console.log('coupon count '+ couponCount);
//         // delete req.body.numberOfCoupons;
//         // let couponArray=[];

//         //  for(var i=0 ;i<couponCount;i++){

//         //     let result = await Coupon.save(req.body);
//         //     couponArray.push(result);

//         //  }

//         //  console.log(req.body);
//         //  console.log(couponArray);
//         console.log('createCouponData:', createCouponData);
//         //await Blockchain.createReward(createCouponData);
//         let result = await Coupon.save(req.body);
//         logger.success(req, res, req.body);
//         response.success(req, res, null, result);
//     } catch (error) {
//         logger.error(req, res, req.body, error);
//         response.serverError(req, res, null, null);
//     }
// });

// // get all coupons (incentives)
// router.get('/', async function (req, res) {
//     try {
//         let props = req.query || {};
//         let result = await Coupon.getAllCoupons(props);
//         let updatedResult = [];
//         for (let i in result) {
//             let images = await Image.get({ belongs_to: result[i].uuid });
//             updatedResult.push(Object.assign(result[i].toObject(), { images: images }));
//         }
//         logger.success(req, res, req.body);
//         response.success(req, res, null, updatedResult);
//     } catch (error) {
//         logger.error(req, res, req.body, error);
//         response.serverError(req, res, null, null);
//     }
// });

// // get coupons details based on ID
// router.get('/:couponID/details', async function (req, res) {
//     try {
//         let result = await Coupon.getByUUID(req.params.couponID);
//         logger.success(req, res, req.body);
//         response.success(req, res, null, result);
//     } catch (error) {
//         logger.error(req, res, req.body, error);
//         response.serverError(req, res, null, null);
//     }
// });

// // get rewards details based on ID
// router.get('/rewards/:rewardID/details', async function (req, res) {
//     try {
//         let result = await Coupon.getByUUID(req.params.rewardID);
//         logger.success(req, res, req.body);
//         response.success(req, res, null, result);
//     } catch (error) {
//         logger.error(req, res, req.body, error);
//         response.serverError(req, res, null, null);
//     }
// });

// // update coupon
// router.post('/update', async function (req, res) {
//     logger.logString("inside update of coupon");
//     logger.logString('is batch? ' + req.body.is_batch);
//     logger.logObj(req.body.update_props);
//     try {
//         if (!req.body.uuid) {
//             response.badRequest(req, res, null, null);
//         } else {
//             // ** warning: add object filtering for update properties
//             //let result = await Coupon.update({ uuid: req.body.uuid }, req.body.update_props);
//             let result = await Coupon.update({ uuid: req.body.uuid }, req.body);
//             logger.success(req, res, req.body);
//             response.success(req, res, null, result);
//         }
//     } catch (error) {
//         logger.error(req, res, req.body, error);
//         response.serverError(req, res, null, null);
//     }
// });



// // as angular doesn' support delete method with body
// // I have created the api named 'delete.
// router.post('/delete', async function (req, res) {
//     logger.logString("incentive will be deleted.... ");
//     try {
//         if (!req.body.uuid) {
//             response.badRequest(req, res, null, null);
//         } else {
//             let result = await Coupon.delete({ uuid: req.body.uuid });
//             logger.logString("ddddddddddddddd... ");
//             logger.success(req, res, req.body);
//             response.success(req, res, null, { uuid: req.body.uuid });
//         }
//     } catch (error) {
//         logger.error(req, res, req.body, error);
//         response.serverError(req, res, null, null);
//     }
// });

// router.get('/temp/tempId', function (req, res) {
//     try {
//         let uuid = Utils.getUUID();
//         logger.success(req, res, null);
//         response.success(req, res, null, { uuid: uuid });
//     } catch (error) {
//         logger.error(req, res, req.body, error);
//         response.serverError(req, res, null, null);
//     }
// });

// //rewards
// // get rewards
// router.get('/rewards', async function (req, res) {
//     try {
//         console.log('inside rewards');
//         let props = req.query || {};
//         let result = await Coupon.getAllRewards(props);
//         console.log('rewards', result);
//         let updatedResult = [];
//         for (let i in result) {
//             let images = await Image.get({ belongs_to: result[i].uuid });
//             updatedResult.push(Object.assign(result[i].toObject(), { images: images }));
//         }
//         logger.success(req, res, req.body);
//         response.success(req, res, null, updatedResult);
//     } catch (error) {
//         logger.error(req, res, req.body, error);
//         response.serverError(req, res, null, null);
//     }
// });
// //batch insert 
// router.post('/createCouponsInBatch', async function (req, res) {
//     // need to write code for batch insert
//     console.log('req.body of coupon routes' + JSON.stringify(req.body));
//     let couponArray = [];
//     let code = "";

//     let couponCount = parseInt(req.body.numberOfCoupons);
//     console.log('Coupon Count in coup route:' + couponCount);
//     for (let index = 0; index < couponCount; index++) {
//         let data = {
//             batch_name: req.body.batch_name,
//             created: req.body.created,
//             one_time_use: req.body.oneTimeUse,
//             summary: req.body.summary,
//             created_by: req.body.createdBy,
//             uuid: uuid.v4(),
//             code: uuid.v4(),
//         }
//         couponArray.push(data);
//         // console.log('req.body of coup route in loop'+JSON.stringify(req.body));
//         // console.log('couponArray of coup route in loop'+JSON.stringify(couponArray));
//     }
//     console.log('couponArray in coup route: ' + JSON.stringify(couponArray));
//     try {
//         // if (!req.body.uuid) {
//         //     return response.badRequest(req, res, 'Bad request', null);
//         // }

//         let result = await Coupon.saveMany(couponArray, req.body);
//         logger.success(req, res, req.body);
//         response.success(req, res, null, result);
//     } catch (error) {
//         console.log('catch route');
//         logger.error(req, res, req.body, error);
//         response.serverError(req, res, null, null);
//     }
// });

// router.get('/getAllBatchCoupons', async function (req, res) {
//     try {
//         let props = req.query || {};
//         let result = await Coupon.getAllBatchCoupons(props);

//         logger.success(req, res, req.body);
//         response.success(req, res, null, result);
//     } catch (error) {
//         logger.error(req, res, req.body, error);
//         response.serverError(req, res, null, null);
//     }
// });


// //////////////////////////
// //get rewards details
// const Incentive = require('./../../models/IncentiveSchema');
// router.get('/:vendorId/:searchStartTime/:searchEndTime/getAllRewardsSpecificDetails', async function (req, res) {
//     logger.logString("inside getAllRewardsSpecificDetails....");
//     logger.logObj(req.params);
//     try {
//         let rewardsSpecificDetailsObj = {
//             created: -1,
//             active: -1,
//             inactive: -1,
//             deleted: -1,
//             expired: -1,
//             redeemed: 0
//         };
//         let vendorId = req.params.vendorId;
//         let searchStartTime = req.params.searchStartTime;
//         let searchEndTime = req.params.searchEndTime;
//         //got to improve better search methods.... 
//         await Coupon.getSpecificRewardCounts(vendorId, 1, searchStartTime, searchEndTime).then(function (createdValue) {
//             rewardsSpecificDetailsObj.created = createdValue;
//         });
//         await Coupon.getSpecificRewardCounts(vendorId, 2, searchStartTime, searchEndTime).then(function (activeValue) {
//             rewardsSpecificDetailsObj.active = activeValue;
//         });
//         await Coupon.getSpecificRewardCounts(vendorId, 3, searchStartTime, searchEndTime).then(function (inactiveValue) {
//             rewardsSpecificDetailsObj.inactive = inactiveValue;
//         });
//         await Coupon.getSpecificRewardCounts(vendorId, 4, searchStartTime, searchEndTime).then(function (deletedValue) {
//             rewardsSpecificDetailsObj.deleted = deletedValue;
//         });
//         await Coupon.getSpecificRewardCounts(vendorId, 5, searchStartTime, searchEndTime).then(function (expiredValue) {
//             rewardsSpecificDetailsObj.expired = expiredValue;

//             logger.logString("Reward specific to be sent");
//             logger.logObj(rewardsSpecificDetailsObj);
//             response.success(req, res, null, rewardsSpecificDetailsObj);
//         });
//     } catch (error) {
//         response.serverError(req, res, error, null);
//     }
// });

// router.get('/getAllIncentivesSpecificDetails/:vendorId/:incentiveType/:searchStartTime/:searchEndTime', async function (req, res) {
//     logger.logString("Inside getAllIncentivesSpecificDetails....");
//     logger.logObj(req.params);
//     try {
//         let rewardsSpecificDetailsObj = {
//             created: -1,
//             active: -1,
//             inactive: -1,
//             deleted: -1,
//             expired: -1,
//             redeemed: 0
//         };
//         let vendorId = req.params.vendorId;
//         let searchStartTime = req.params.searchStartTime;
//         let searchEndTime = req.params.searchEndTime;
//         let incentiveType = req.params.incentiveType;
//         //got to improve better search methods.... 
//         await Coupon.getSpecificIncentiveCounts(vendorId, incentiveType, 1, searchStartTime, searchEndTime).then(function (createdValue) {
//             rewardsSpecificDetailsObj.created = createdValue;
//         });
//         await Coupon.getSpecificIncentiveCounts(vendorId, incentiveType, 2, searchStartTime, searchEndTime).then(function (activeValue) {
//             rewardsSpecificDetailsObj.active = activeValue;
//         });
//         await Coupon.getSpecificIncentiveCounts(vendorId, incentiveType, 3, searchStartTime, searchEndTime).then(function (inactiveValue) {
//             rewardsSpecificDetailsObj.inactive = inactiveValue;
//         });
//         await Coupon.getSpecificIncentiveCounts(vendorId, incentiveType, 4, searchStartTime, searchEndTime).then(function (deletedValue) {
//             rewardsSpecificDetailsObj.deleted = deletedValue;
//         });
//         await Coupon.getSpecificIncentiveCounts(vendorId, incentiveType, 5, searchStartTime, searchEndTime).then(function (expiredValue) {
//             rewardsSpecificDetailsObj.expired = expiredValue;
//             logger.logString("Reward specific to be sent");
//             logger.logObj(rewardsSpecificDetailsObj);
//             response.success(req, res, null, rewardsSpecificDetailsObj);
//         });
//     } catch (error) {
//         response.serverError(req, res, error, null);
//     }
// });

// router.get('/:vendorId/getSuggestedRewards', async function (req, res) {
//     logger.logString("GGEET SUGGESTED REWARDS....");
//     logger.logObj(req.params);
//     logger.logString("vendor ID: " + req.params.vendorId);
//     let result = Coupon.getSuggestedRewardsForVendor(req.params.vendorId);
//     return result;
// });


// //insert incentives from array (through file uploads)
// router.post('/createIncentivesFromUpload', async function (req, res) {
//     // need to write code for batch insert
//     logger.logString("Inside createIncentivesFromUpload....");
//     console.log('req.body of incentive routes' + JSON.stringify(req.body));

//     let incentiveArray = req.body;
//     let totalIncentives = req.body.length;
//     for (let k = 0; k < totalIncentives; k++) {
//         logger.logString("following incentive(s) will be added....");

//         //fetch new UUID
//         req.body[k].uuid = Utils.getUUID();
//         logger.logObj(req.body[k]);
//     }

//     try {

//         let result = await Coupon.saveManyIncentives(req.body);
//         logger.logString("SUCCESS IN CREATING UPLOAD INCENTIVESS");
//         logger.success(req, res, req.body);
//         response.success(req, res, null, result);

       
//     } catch (error) {
//         logger.error(error);
//         response.serverError(req, res, error, null);
//     }

// });


// //create incentives in batch
// router.post('/createIncentivesInBatch', async function (req, res) {
//     logger.logString('req.body of incentive routes' + JSON.stringify(req.body));
//     response.success(req, res, null, "success");

//     let incentiveArray = [];
//     let batchCount = parseInt(req.body.batch_count);
//     logger.logString('Total number of incentives :' + batchCount);
//     for (let k = 0; k < batchCount; k++) {
//         //make array of total incentives
//         tmpIncentive = new Incentive(req.body);
//         // tmpIncentive = req.body;
//         logger.logString("UUID Before: " + tmpIncentive.uuid);
//         //get unique uuid
//         tmpIncentive.uuid = Utils.getUUIDV4();
//         logger.logString("UUID After: " + tmpIncentive.uuid);
//         //get unique coupon code
//         tmpIncentive.code = Utils.getUUIDV4();
//         incentiveArray.push(tmpIncentive);
//     }
//     logger.logString("Fresh incentives to be added.....");
//     logger.logObj(incentiveArray);

//     try {

//         let result = await Coupon.saveManyIncentives(incentiveArray);
//         logger.logString("SUCCESS IN CREATING BATCH INCENTIVESS");
//         logger.success(req, res, req.body);
//         response.success(req, res, null, result);

//         // await Coupon.insertMany(incentiveArray, function (err, data) {
//         //     if (err) {
//         //         logger.logString("Error in batch insertttttttttt");
//         //         logger.logObj(err);
//         //         response.serverError(req, res, null, null);
//         //     } else {
//         //         logger.logString("SUCCESS in batch insertttttttttt");
//         //         logger.logObj(data);
//         //         response.success(req, res, null, null);
//         //     }
//         // })
//     } catch (error) {
//         logger.logString(error);
//         logger.logObj(response);
//         response.serverError(req, res, error, null);
//     }
// });



// module.exports = router;
