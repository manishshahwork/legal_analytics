let express = require('express');
let logger = require('./../../libs/Logger');

let response = require('./../../libs/Response');
let Utils = require('./../../utils/Utils');
let Image = require('./../../models/Image');
let router = express.Router();
// let Coupon = require('./../../models/Coupon');
const uuid = require('uuid');
const Incentive = require('./../../models/IncentiveSchema');
let Operations = require('../../libs/Operations');

router.get("/test/:type", async function (req, res) {
    logger.logString("test: Entered");
    try {
        response.success(req, res, "Success in accessing incentive routes", null);
    } catch (error) {
        logger.error(req, res, null, error);
        response.serverError(req, res, "Error in accessing incentive routes", error);
    }
});

//get all incentives (coupons/rewards/)
router.get("/getAllIncentives/:vendor_uuid/:type", async function (req, res) {
    logger.logString("getAllIncentives: Entered");
    try {
        logger.logObj(req.params);
        let vendor_uuid = req.params.vendor_uuid;
        let type = req.params.type;
        let query = Incentive.find();
        query.where('vendor_uuid').equals(vendor_uuid);
        query.where('type').equals(type);
        let result = await query.exec();
        logger.logStringWithObj("Results got", result);
        response.success(req, res, "Success in getting all incentives", result);
    } catch (error) {
        logger.error(req, res, null, error);
        response.serverError(req, res, "Error in fetching incentives", error);
    }
});

// get coupons details based on ID
router.get('/getSingleIncentive/:incentiveId', async function (req, res) {
    logger.logString("getSingleIncentive: Entered");
    try {
        logger.logObj(req.params);
        let result = await Incentive.findOne({ 'uuid': req.params.incentiveId })
        logger.logStringWithObj("Results got", result);
        response.success(req, res, "Success in getting single incentive", result);
    } catch (error) {
        logger.error(req, res, null, error);
        response.serverError(req, res, "Error in fetching single incentive", error);
    }
});

//add incentive
router.post("/addIncentive", async function (req, res) {
    try {
        logger.logObj(req.params);
        logger.logObj(req.body);
        //1st generate unique id for the incentive
        let newIncentive = new Incentive(req.body);
        newIncentive.uuid = Utils.getUUID();
        let result = await newIncentive.save();
        response.success(req, res, "Success in adding incentive", result);
    } catch (error) {
        logger.error(req, res, null, error);
        response.serverError(req, res, "Error in adding incentive", error);
    }
});

//update incentive
router.post("/updateIncentive", async function (req, res) {
    logger.logString("Incentive will be Updated.");
    try {
        logger.logObj(req.params);
        logger.logObj(req.body);
        if (!req.body.uuid) {
            response.badRequest(req, res, null, null);
        } else {
            let result = await Incentive.updateOne({ uuid: req.body.uuid }, req.body);
            logger.success(req, res, req.body);
            response.success(req, res, "Success in updating incentive", result);
        }
    } catch (error) {
        logger.error(req, res, null, error);
        response.serverError(req, res, "Error in adding incentive", error);
    }
});

//delete incentive
//as angular doesn't support delete method with body
router.post('/deleteIncentive', async function (req, res) {
    logger.logString("Incentive will be Deleted. ");
    try {
        logger.logObj(req.params);
        logger.logObj(req.body);
        if (!req.body.uuid) {
            response.badRequest(req, res, null, null);
        } else {
            //1st way
            // let query = Incentive.updateOne();
            // query.where('uuid').equals(req.body.uuid);
            // query.update({ 'is_deleted': true });
            // let result = await query.exec();  
            //or you can use direct query.          
            //2nd way
            let result = await Incentive.updateOne({ 'uuid': req.body.uuid }, { 'is_deleted': true })
            logger.logStringWithObj("Results got", result);
            response.success(req, res, "Success in deleting incentive", result);
        }
    } catch (error) {
        logger.error(req, res, null, error);
        response.serverError(req, res, "Error in deleting incentive", error);
    }
});

//create batch incentives
router.post('/createIncentivesInBatch', async function (req, res) {
    logger.logString('req.body of incentive routes' + JSON.stringify(req.body));
    response.success(req, res, null, "success");

    let incentiveArray = [];
    let batchCount = parseInt(req.body.batch_count);
    logger.logString('Total number of incentives :' + batchCount);
    for (let k = 0; k < batchCount; k++) {
        //make array of total incentives
        tmpIncentive = new Incentive(req.body);
        // tmpIncentive = req.body;
        logger.logString("UUID Before: " + tmpIncentive.uuid);
        //get unique uuid
        tmpIncentive.uuid = Utils.getUUIDV4();
        logger.logString("UUID After: " + tmpIncentive.uuid);
        //get unique incentive code
        tmpIncentive.code = Utils.getUUIDV4();
        incentiveArray.push(tmpIncentive);
    }
    logger.logString("Fresh incentives to be added.....");
    logger.logObj(incentiveArray);

    try {
        let result = await Incentive.insertMany(incentiveArray);
        logger.logString("SUCCESS IN CREATING BATCH INCENTIVES");
        logger.success(req, res, req.body);
        response.success(req, res, null, result);
    } catch (error) {
        logger.logString(error);
        response.serverError(req, res, error, null);
    }
});

//insert incentives from array (through file uploads)
router.post('/createIncentivesFromUpload', async function (req, res) {
    // need to write code for batch insert
    logger.logString("Inside createIncentivesFromUpload....");
    console.log('req.body of incentive routes' + JSON.stringify(req.body));
    let totalIncentives = req.body.length;
    for (let k = 0; k < totalIncentives; k++) {
        logger.logString("following incentive(s) will be added....");

        //fetch new UUID
        req.body[k].uuid = Utils.getUUID();
        logger.logObj(req.body[k]);
    }
    try {
        let result = await Incentive.insertMany(req.body);
        logger.logString("SUCCESS IN CREATING UPLOAD INCENTIVESS");
        logger.success(req, res, req.body);
        response.success(req, res, null, result);
    } catch (error) {
        logger.error(error);
        response.serverError(req, res, error, null);
    }

});

// returns specific incentives uploaded by a vendor
router.get('/getSpecificSearchIncentives/:incentiveType/:vendor_uuid/:searchNameString/:primarySearchType/:searchType/:searchStartTime/:searchEndTime', async function (req, res) {
    logger.logString("Inside Specific New Incentives");
    logger.logObj(req.params);
    try {
        let vendor_uuid = req.params.vendor_uuid;
        let incentiveType = req.params.incentiveType;
        let searchNameString = req.params.searchNameString;
        let primarySearchType = req.params.primarySearchType;
        let searchType = req.params.searchType;
        let searchStartTime = req.params.searchStartTime;
        let searchEndTime = req.params.searchEndTime;
        
        if (!vendor_uuid) throw new Error('Invalid Vendor UUID');
        logger.logString("searchNameString:" + searchNameString);
        logger.logString("searchType: " + searchType);
        if (typeof (searchNameString) == null || typeof (searchNameString) == undefined || searchNameString.trim() == "" || searchNameString === "NULL") {
            logger.logString("NNNNNNNNNNNNUUUUUUUUULLLLLLLLLLLLLL");
            searchNameString = "";
        }

        //build a string query
        let query = Incentive.find();
        query.where('vendor_uuid').equals(vendor_uuid);
        query.where('type').equals(incentiveType);
        query.where('name').equals({ $regex: '.*' + searchNameString + '.*', $options: 'i' });


        //based on primary search type, kindly change the logic
        if (primarySearchType == "Batch")
            query.where('is_batch').equals("true");
        else if (primarySearchType == "Normal")
            query.where('is_batch').equals("false");
        // if (searchType == "Batch") query.where('is_batch').equals("true");

        if (searchType == "Active") query.where('is_active').equals("true");
        if (searchType == "Inactive") query.where('is_active').equals("false");
        if (searchType == "Expired") query.where('end_time').lte(Date.now());
        if (searchType == "Deleted") query.where('is_deleted').equals("true");

        if (searchStartTime != 0) query.where('created_on').gte(searchStartTime);
        if (searchEndTime != 0) query.where('created_on').lte(searchEndTime);

        //execute the query
        logger.logStringWithObj("query object:", query);
        let result = await query.exec();
        logger.logStringWithObj("result from specific search:", result);
        let updatedResult = [];
        for (let i in result) {
            let images = await Image.getCouponImages(result[i].uuid)
            updatedResult.push(Object.assign(result[i].toObject(), { images: images }));
        }
        logger.success(req, res, req.body);
        response.success(req, res, null, updatedResult);
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, null, null);
    }
});

//dashboard - get all incentive counts
router.get('/getAllIncentiveCounts/:vendorId/:incentiveType/:searchStartTime/:searchEndTime', async function (req, res) {
    logger.logString("Inside getAllIncentiveCounts....");
    logger.logObj(req.params);
    try {
        let rewardsSpecificDetailsObj = {
            created: -1,
            active: -1,
            inactive: -1,
            deleted: -1,
            expired: -1,
            redeemed: 0
        };
        let vendorId = req.params.vendorId;
        let searchStartTime = req.params.searchStartTime;
        let searchEndTime = req.params.searchEndTime;
        let incentiveType = req.params.incentiveType;
        //got to improve better search methods....  
        logger.logStringWithObj("Operations...:: ", Operations);
        let k = Operations.getSpecificIncentiveCounts(vendorId, incentiveType, 1, searchStartTime, searchEndTime);
        logger.logStringWithObj("kkkkk:: ", k);
        await Operations.getSpecificIncentiveCounts(vendorId, incentiveType, 1, searchStartTime, searchEndTime).then(function (createdValue) {
            rewardsSpecificDetailsObj.created = createdValue;
        });
        await Operations.getSpecificIncentiveCounts(vendorId, incentiveType, 2, searchStartTime, searchEndTime).then(function (activeValue) {
            rewardsSpecificDetailsObj.active = activeValue;
        });
        await Operations.getSpecificIncentiveCounts(vendorId, incentiveType, 3, searchStartTime, searchEndTime).then(function (inactiveValue) {
            rewardsSpecificDetailsObj.inactive = inactiveValue;
        });
        await Operations.getSpecificIncentiveCounts(vendorId, incentiveType, 4, searchStartTime, searchEndTime).then(function (deletedValue) {
            rewardsSpecificDetailsObj.deleted = deletedValue;
        });
        await Operations.getSpecificIncentiveCounts(vendorId, incentiveType, 5, searchStartTime, searchEndTime).then(function (expiredValue) {
            rewardsSpecificDetailsObj.expired = expiredValue;
            logger.logString("Reward specific to be sent");
            logger.logObj(rewardsSpecificDetailsObj);
            response.success(req, res, null, rewardsSpecificDetailsObj);
        });
    } catch (error) {
        response.serverError(req, res, error, null);
    }
});

module.exports = router;




 