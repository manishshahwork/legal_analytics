let express = require('express');
let router = express.Router();
let Transaction = require('./../../models/Transaction');
let logger = require('./../../libs/Logger');
let response = require('./../../libs/Response');
let Coupon = require('./../../models/Coupon');

module.exports = router;

router.post('/earn', async function (req, res) {
    try {
        let itemUUID = req.body.item_uuid;
        let userUUID = req.body.user_uuid;
        let itemDetails = await Coupon.getByUUID(req.body.item_uuid);
        let props = getTransactionObjectFromItemObject(itemDetails, userUUID, null);
        let result = await Transaction.createEarningEntry(props);
        response.success(req, res, null, result);
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, null, null);
    }
});

router.post('/expense', async function (req, res) {
    try {
        let itemUUID = req.body.item_uuid;
        let userUUID = req.body.user_uuid;
        let itemDetails = await Coupon.getByUUID(req.body.item_uuid);
        let earningTransactionDetails = await Transaction.getUsersEarningTransactionForItemUUID(itemUUID, userUUID);
        if(earningTransactionDetails.length < 0 ) {
            response.badRequest(req, res, 'You have not earned this item', null);
        }
        let props = getTransactionObjectFromItemObject(itemDetails, userUUID, earningTransactionDetails[0].uuid)
        let result = await Transaction.createExpenseEntry(props);
        response.success(req, res, null, result);
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, null, null);
    }
});

// helper functions
function getTransactionObjectFromItemObject(itemDetails, userUUID, earningTransactionUUID) {
    return {
        user_uuid: userUUID,
        item_type: itemDetails.type,
        item_uuid: itemDetails.uuid,
        item_name: itemDetails.name,
        item_summary: itemDetails.summary,
        eraning_transaction_uuid: earningTransactionUUID || null,
        reward_points: itemDetails.reward_points,
        coupon_code: itemDetails.code,
        item_start_time: itemDetails.start_time,
        item_end_time: itemDetails.end_time,
    }
}