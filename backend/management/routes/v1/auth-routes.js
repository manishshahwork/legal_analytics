 // var express = require('express');
// var router = express.Router();
// let constants = require('./../../constants/constants');
// // let passport = require('./../../models/auth/passport');
// let passportGoogle = require('./../../models/auth/google');
// let passportFacebook = require('./../../models/auth/facebook');
// let User = require('./../../models/mngmnt/User');
// let SessionHandler = require('./../../models/mngmnt/SessionHandler');
// let Util = require('./../../models/utils/Utils');
// let TokenManager = require('./../../models/auth/TokenManager');
// let logger = require('./../../models/libs/Logger');
// let response = require('./../../models/libs/Response');

// router.get('/login', function (req, res) {
//     res.cookie(constants.COOKIE.NAME.ROLE, constants.COOKIE.VALUE.CUSTOMER, {});
//     if (Util.isValidReq(req)) {
//         res.redirect('/auth/verify')
//     } else {
//         res.render('login-customer', {});
//     }
// });

// router.get('/login/vendor', function (req, res) {
//     res.cookie(constants.COOKIE.NAME.ROLE, constants.COOKIE.VALUE.VENDOR, {});
//     res.render('login-vendor', {});
// });

// router.get('/verify', async function (req, res) {
//     let role = constants.USER_ROLES.CONSUMER;
//     if (req.cookies[constants.COOKIE.NAME.ROLE] === constants.COOKIE.VALUE.VENDOR) {
//         role = constants.USER_ROLES.VENDOR;
//     }

//     try {
//         let saved = await User.setRole(req.user, role);
//         role = saved.role;
//         let accessToken = TokenManager.createToken({ email: saved.email, uuid: saved.uuid });
//         let temp = {
//             accessToken: accessToken,
//             email: saved.email,
//             uuid: saved.uuid
//         };
//         await SessionHandler.setAcceesCookie(req, res, temp);
//         if (role === constants.USER_ROLES.VENDOR) {
//             res.redirect('/vendors');
//         } else if (role === constants.USER_ROLES.CONSUMER) {
//             res.redirect('/');
//         } else {
//             throw new Error('Unknown role');
//         }
//     } catch (error) {
//         console.log(error);
//         res.redirect('/auth/login');
//     }
// });

// // auth routes for facebook 
// router.get('/login/facebook', passportFacebook.authenticate('facebook', { scope: 'email' }));
// router.get('/login/facebook/callback', passportFacebook.authenticate('facebook', {
//     successRedirect: '/auth/verify',
//     failureRedirect: '/auth/login'
// }));

// // auth roots for google
// router.get('/login/google', passportGoogle.authenticate('google', { scope: ['profile', 'email'] }));
// router.get('/login/google/callback', passportGoogle.authenticate('google', {
//     successRedirect: '/auth/verify',
//     failureRedirect: '/auth/login'
// }));

// // auth routes for native logins
// // create a new user
// // this api is mainly used for social logins from ios/android
// // should not be used for web

// router.post('/login/native/facebook', async function (req, res) {
//     try {

//         if (req.body.facebookID == null || req.body.facebookToken == null || req.body.facebookID == '' || req.body.facebookToken == '') {
//             return response.badRequest(req, res, 'valid facebook ID AND facebook token required', req.body);
//         }

//         let props = {
//             email: req.body.email,
//             name: req.body.name,
//             facebook_id: req.body.facebookID,
//             role: constants.USER_ROLES.CONSUMER
//         }

//         let isValidToken = await TokenManager.isValidFBToken(req.body.facebookToken);

//         if (isValidToken) {
//             let saved = await User.save(props);
//             let result = saved.toObject();
//             result.accessToken = TokenManager.createToken({ email: saved.email, uuid: saved.uuid });
//             result.refreshToken = TokenManager.createRefreshToken({ email: saved.email, uuid: saved.uuid });
//             response.success(req, res, null, result);
//         } else {
//             return response.badRequest(req, res, 'Invalid Facebook token', props);
//         }
//     } catch (error) {
//         logger.error(req, res, req.body, error);
//         response.serverError(req, res, null, null);
//     }
// });

// router.post('/login/native/google', async function (req, res) {
//     try {
//         if (req.body.googleID === null || req.body.googleToken === null || req.body.googleID === '' || req.body.googleToken === '') {
//             return response.badRequest(req, res, 'valid google ID AND google token required', req.body);
//         }

//         let props = {
//             email: req.body.email,
//             name: req.body.name,
//             google_id: req.body.googleID,
//             role: constants.USER_ROLES.CONSUMER
//         }

//         let isValidToken = await TokenManager.isValidGoogleToken(req.body.googleToken);

//         if (isValidToken) {
//             let saved = await User.save(props);
//             let result = saved.toObject();
//             result.accessToken = TokenManager.createToken({ email: saved.email, uuid: saved.uuid });
//             result.refreshToken = TokenManager.createRefreshToken({ email: saved.email, uuid: saved.uuid });
//             response.success(req, res, null, result);
//         } else {
//             return response.badRequest(req, res, 'Invalid Google token', props);
//         }
//     } catch (error) {
//         logger.error(req, res, req.body, error);
//         response.serverError(req, res, null, null);
//     }
// });

// router.post('/refresh-token', async function (req, res) {
//     try {
//         let decodedToken = TokenManager.decodeToken(req.body.refreshToken);
//         if (decodedToken) {
//             let user = await User.findOne({ uuid: req.body.uuid });
//             let accessToken = TokenManager.createToken({ email: user.email, uuid: user.uuid });
//             response.success(req, res, null, { accessToken: accessToken });
//         } else {
//             return response.badRequest(req, res, 'Invalid Refresh token', req.body);
//         }
//     } catch (error) {
//         logger.error(req, res, req.body, error);
//         response.serverError(req, res, null, null);
//     }
// });
// // logout route
// router.get('/logout', function (req, res) {
//     console.log('logging out');
//     req.logout();
//     res.redirect('/auth/login');
// });

// module.exports = router;