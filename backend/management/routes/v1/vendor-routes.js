let express = require('express');
let router = express.Router();
let User = require('./../../models/User');
let Image = require('./../../models/Image');
let logger = require('./../../libs/Logger');
let Coupon = require('./../../models/Coupon');
let Vendor = require('./../../models/Vendor');
let response = require('./../../libs/Response');
let Subscription = require('./../../models/Subscription');
let NewVendorProfile = require('../../models/NewVendorProfileSchema')
let Utils = require('./../../utils/Utils');

// return all vendors based on filter or category
router.get('/category/:category', async function (req, res) {
    try {
        let arrDistinctVendors = await Coupon.getVendorsUUIDBasedOnCouponCategory(req.params.category);
        let vendors = [];
        for (let i in arrDistinctVendors) {
            let tempV = await Vendor.findOne({ uuid: arrDistinctVendors[i] });
            vendors.push(tempV);
        }
        logger.success(req, res, req.params);
        response.success(req, res, null, vendors);
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, null, null);
    }
});

// return coupons uploaded by a vendor
router.get('/:vendorID/:filter/coupons', async function (req, res) {
    logger.logString("Get coupons...... ");
    logger.logObj(req.params);
    try {
        let result = await Coupon.getCouponsUploadedByVendor(req.params.vendorID);
        let updatedResult = [];
        for (let i in result) {
            let images = await Image.getCouponImages(result[i].uuid)
            updatedResult.push(Object.assign(result[i].toObject(), { images: images }));
        }
        logger.success(req, res, req.body);
        response.success(req, res, null, updatedResult);
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, null, null);
    }
});


//########################### START.
// returns all incentvies uploaded by a vendor
router.get('/allIncentives/:vendorId/:incentiveType', async function (req, res) {
    logger.logString("Inside All Incentives Function");
    logger.logObj(req.params);
    let vendorId = req.params.vendorId;
    let incentiveType = req.params.incentiveType;
    try {
        let result = await Coupon.getAllIncentivesUploadedByVendor(vendorId, incentiveType);
        let updatedResult = [];
        for (let i in result) {
            let images = await Image.getCouponImages(result[i].uuid)
            updatedResult.push(Object.assign(result[i].toObject(), { images: images }));
        }
        logger.success(req, res, req.body);
        response.success(req, res, null, updatedResult);
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, null, null);
    }
});

// returns specific incentives uploaded by a vendor
router.get('/incentives/:incentiveType/:vendorID/:searchNameString/:primarySearchType/:searchType/:searchStartTime/:searchEndTime', async function (req, res) {
    logger.logString("Inside Specific New Incentives");
    logger.logObj(req.params);
    try {
        // let result = await Coupon.getSpecificRewardsUploadedByVendor(req.params.vendorID, req.params.searchNameString, req.params.searchType, req.params.searchStartTime, req.params.searchEndTime);
        let result = await Coupon.getSpecificIncentivesUploadedByVendor(req.params.vendorID, req.params.incentiveType, req.params.searchNameString, req.params.primarySearchType, req.params.searchType, req.params.searchStartTime, req.params.searchEndTime);
        let updatedResult = [];
        for (let i in result) {
            let images = await Image.getCouponImages(result[i].uuid)
            updatedResult.push(Object.assign(result[i].toObject(), { images: images }));
        }
        logger.success(req, res, req.body);
        response.success(req, res, null, updatedResult);
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, null, null);
    }
});


//########################### END

// returns consumer details
router.get('/customer/:customerID', async function (req, res) {
    try {
        let result = await User.getProfileForVendor(req.params.customerID);
        result = result.toObject();
        result.rewardPoints = 100;
        logger.success(req, res, req.body);
        response.success(req, res, null, result);
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, null, null);
    }
});

router.post('/', function (req, res) { });

router.put('/', function (req, res) { });

router.delete('/', function (req, res) { });

// update vendor profile
router.put('/updateProfile_old', async function (req, res) {
    logger.logStringWithObj("vendor-routes:updateProfile: Entered");
    logger.logObj(req.body);
    try {
        let result = await Vendor.updateExistingProfile(req.body.vendor_uuid, req.body);
        logger.success(req, res, req.body);
        response.success(req, res, null, result);
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, null, null);
    }
});

// return all vendors profile
router.get('/profiles', async function (req, res) {
    try {
        let result = await Vendor.getProfile({});
        logger.success(req, res, req.body);
        response.success(req, res, null, result);
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, null, null);
    }
});

// return profile of a vendor
router.get('/getVendorProfile/:vendor_uuid', async function (req, res) {
    try {
        let result = await NewVendorProfile.findOne({ 'uuid': req.params.vendor_uuid });
        logger.logStringWithObj("Received Vendor:", result);
        response.success(req, res, "Success in getVendorProfile", result);
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, null, null);
    }
});

// // return profile of a vendor
// router.get('/getVendorProfile/:vendorID', async function (req, res) {
//     try {
//         let result = await Vendor.getProfile({ vendor_uuid: req.params.vendorID });
//         response.success(req, res, null, result);
//     } catch (error) {
//         logger.error(req, res, req.body, error);
//         response.serverError(req, res, null, null);
//     }
// });

// return profile of a vendor
router.get('/:vendorID/details', async function (req, res) {
    try {
        let result = (await Vendor.getProfile({ vendor_uuid: req.params.vendorID }))[0];
        let vendor = (await Vendor.getAllVendorsWithFilter({ uuid: req.params.vendorID }))[0];
        let details = Object.assign(vendor.toObject());
        details.profile = result;
        response.success(req, res, null, details);
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, null, null);
    }
});

// returns vendor images
router.get('/:vendorID/images', async function (req, res) {
    try {
        let result = await Image.getProfileImages(req.params.vendorID);
        response.success(req, res, null, result);
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, null, null);
    }
});

// returns all purchases by vendor
router.get('/:vendorID/purchases', async function (req, res) {
    try {
        let allCoupons = await Coupon.getCouponsUploadedByVendor(req.params.vendorID);
        let allPurchases = [];
        for (let i in allCoupons) {
            let purchaseT = await Purchase.getPurchaseEntriesForCoupons(allCoupons[i].uuid, null);
            allPurchases = [].concat.apply(allPurchases, purchaseT);
        }
        response.success(req, res, null, allPurchases);
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, null, null);
    }
});

// returns all subscribed customers
router.get('/:vendorID/subscriptions/customers', async function (req, res) {
    try {
        let allCustomersUUID = await Subscription.getUniqueCustomerUUIDSubscribedForVendor(req.params.vendorID);
        let customers = [];
        for (let i in allCustomersUUID) {
            let tempC = await User.getAllUsersWithFilter({ uuid: allCustomersUUID[i] });
            let regDetails = await Subscription.getParticularSubscriptionDetails(allCustomersUUID[i], req.params.vendorID);
            let cDetails = Object.assign(tempC[0].toObject(), { subscribed_on: regDetails.created_on });
            customers.push(cDetails);
        }
        response.success(req, res, null, customers);
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, null, null);
    }
});

// subscribe a new customer on shop. 
// create a new user on DB
// subscribe that customer to particular vendor
router.post('/:vendorID/shop/registers', async function (req, res) {
    try {
        let saved = await User.save({ email: req.body.email });
        let result = await Subscription.subscribeUserToVendor(saved.uuid, req.params.vendorID, { email: true, phone: true });
        logger.success(req, res, req.params);
        response.success(req, res, null, saved);
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, null, null);
    }
});


//################################## MHS
//## new code added
// returns all subscribed customers
router.get('/:vendorID/registers/customers', async function (req, res) {
    console.log("#### inside registers but exiting as functionaity is unknown.");

    try {
        //since logic is not defined, return success with hardcoded customer value
        let customers = [];
        response.success(req, res, null, customers);
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, null, null);
    }
});


router.post('/deactivateBatchIncentives/:vendorId', async function (req, tmpRes) {
    logger.logString("Inside Deactivate Incentives");
    logger.logString("Params: ");
    logger.logObj(req.params);
    logger.logString("Body: ");
    logger.logObj(req.body);

    let vendorId = req.params.vendorId;
    if (!vendorId) throw new Error('Invalid Vendor ID');
    try {
        // logger.logString("response before: " + response.success);
        // logger.logObj(response);
        await Coupon.deactivateBatchIncentives(vendorId, req.body);
        // logger.logString("response after: " + response.success); 
        // logger.logObj(response);
        logger.logString("Success in deactivation.... ");
        response.success(req, tmpRes, null, "Success");
    } catch (error) {
        logger.logString("Error in deactivation....." + error);
        logger.error(error);
        response.serverError(req, tmpRes, null, null);
    }
});


router.get("/profileFromUser/:userId", async function (req, res) {
    try {
        logger.logString("Enter: vendor-profileFromUser");
        let userId = req.params.userId;
        logger.logString("userid: " + userId);
        // let newVendorProfile = new NewVendorProfile();
        let query = NewVendorProfile.find();
        query.where('uuid').equals(userId);
        let vendorProfile = await query.exec();
        logger.logString("got vendor::::");
        logger.logObj(vendorProfile);
        response.success(req, res, "Success in getting vendor profile", vendorProfile);
    } catch (error) {
        logger.error(error);
        response.serverError(req, res, "Failure", error);
    }
});


router.post('/updateProfile', async function (req, res) {
    logger.logStringWithObj("new vendor-routes:updateProfile: Entered");
    logger.logObj(req.body);
    try {
        let result = await NewVendorProfile.updateOne({ uuid: req.body.uuid }, req.body);
        // let result = await Vendor.updateExistingProfile(req.body.vendor_uuid, req.body);
        logger.success(req, res, req.body);
        response.success(req, res, "Success in updating profile", result);
    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, "Error in updating profile", error);
    }
});

//create new vendor
router.post("/createNewVendor", async function (req, res) {
    logger.logString("Enter: createNewVendor");
    try {
        logger.logObj(req.params);
        logger.logObj(req.body);
        let vendorProfile = new NewVendorProfile(req.body); 
        
        //set uuid for vendor
        logger.logStringWithObj("before.........", vendorProfile);
        vendorProfile.uuid = await Utils.getUUID();
        logger.logStringWithObj("after.........", vendorProfile);
        
        
        let result = await vendorProfile.save();
        logger.logStringWithObj("save vendor result", result);
        response.success(req, res, "Success in createNewVendor", result);

    } catch (error) {
        logger.error(req, res, null, error);
        response.serverError(req, res, "Error in createNewVendor", error);
    }
});

router.get('/test', function (req, res) {
    logger.logString("Inside Test.");
    res.status(200).json({
        status: 200, message: 'Success in testing'
    });
});



//###################

module.exports = router;