let express = require('express');
let router = express.Router();
let User = require('./../../models/User');
let Image = require('./../../models/Image');
let logger = require('./../../libs/Logger');
let Coupon = require('./../../models/Coupon');
let Vendor = require('./../../models/Vendor');
let response = require('./../../libs/Response');
let Subscription = require('./../../models/Subscription');
let constants = require('./../../constants/constants');
let UserProfile = require('../../models/UserProfileSchema');
let NewVendorProfile = require('../../models/NewVendorProfileSchema');
let CompanyCategory = require('../../models/CompanyCategory');
let CompanyRole = require('../../models/CompanyRole');
let PagePermission = require('../../models/PagePermission');
let LegalDocument = require('../../models/LegalDocument');
let Utils = require('./../../utils/Utils');
const jwt = require('jwt-simple');


router.post("/addLegalDocument", async function (req, res){
    logger.logString("Entered:  addLegalDocument ");
    try {
        logger.logObj(req.params);
        logger.logObj(req.body);
        let legalDocument = new LegalDocument(req.body);
        legalDocument.uuid = await Utils.getUUID();
        let result = await legalDocument.save();
        logger.logStringWithObj("legaldocument added", result);
        response.success(req, res, "Success in addLegalDocument", result);

    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, "Error in addLegalDocument", error);
    }
});

router.post("/updateLegalDocument", async function (req, res){
    logger.logString("Entered:  updateLegalDocument ");
    try {
        logger.logObj(req.params);
        logger.logObj(req.body);
        let result = await LegalDocument.updateOne({ 'uuid': req.body.uuid }, req.body);
        logger.logStringWithObj("legaldocument added", result);
        response.success(req, res, "Success in updateLegalDocument", result);

    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, "Error in updateLegalDocument", error);
    }
});

router.get("/getAllLegalDocuments", async function (req, res){
    logger.logString("Entered:  getAllLegalDocuments");
    try {
        let result = await LegalDocument.find();
        logger.logStringWithObj("result", result);
        response.success(req, res, "Success in getAllLegalDocuments", result);

    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, "Error in getAllLegalDocuments", error);
    }
});


router.get("/getAllUploadedLegalDocuments", async function (req, res){
    logger.logString("Entered:  getAllUploadedLegalDocuments");
    try {
        let result = await LegalDocument.find({'is_document_uploaded' : true});
        logger.logStringWithObj("result", result);
        response.success(req, res, "Success in getAllUploadedLegalDocuments", result);

    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, "Error in getAllUploadedLegalDocuments", error);
    }
});

router.get("/getLegalDocumentById/:uuid", async function (req, res){
    logger.logString("Entered:  getLegalDocumentById");
    try {
        logger.logObj(req.params);
        logger.logObj(req.body);
        let result = await LegalDocument.findOne({'uuid' : req.params.uuid});
        logger.logStringWithObj("result", result);
        response.success(req, res, "Success in getLegalDocumentById", result);

    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, "Error in getLegalDocumentById", error);
    }
});

router.get("/getAllProcessedLegalDocuments", async function (req, res){
    logger.logString("Entered:  getAllProcessedLegalDocuments");
    try {
        let result = await LegalDocument.find({'is_document_processed' : true});
        logger.logStringWithObj("result", result);
        response.success(req, res, "Success in getAllProcessedLegalDocuments", result);

    } catch (error) {
        logger.error(req, res, req.body, error);
        response.serverError(req, res, "Error in getAllProcessedLegalDocuments", error);
    }
});





module.exports = router;