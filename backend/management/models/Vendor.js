// let schema = require('./../libs/Schema');
// let Utils = require('./../utils/Utils');
// let DB = require('./../libs/DB');
// const constants = require('./../constants/constants');
// class Vendor {
//     constructor() { }

//     static async save(props) {
//         props.uuid = Utils.getUUID();
//         let VendorModel = schema.vendorModel;
//         let newVendor = new VendorModel(props);
//         let savedVendor = await this.findOne({ email: props.email });
//         if (savedVendor) return savedVendor;
//         await DB.save(newVendor);
//         return await DB.findOne(VendorModel, { email: newVendor.email }, null);
//     }

//     static async findOne(props) {
//         let VendorModel = schema.vendorModel;
//         return await DB.findOne(VendorModel, props, null);
//     }

//     static async find(props) {
//         let VendorModel = schema.vendorModel;
//         return await DB.find(VendorModel, props, null);
//     }

//     static async update(searchProps, updateProps) {
//         let VendorModel = schema.vendorModel;
//         await DB.updateOne(VendorModel, searchProps, updateProps);
//         return await this.findOne(searchProps);
//     }

//     static async delete(props) {
//         let VendorModel = schema.vendorModel;
//         return await DB.deleteOne(VendorModel, props);
//     }

//     static async createProfile(props) {
//         props.uuid = Utils.getUUID();
//         let VendorProfileModel = schema.vendorProfileModel;
//         let newProfile = new VendorProfileModel(props);
//         await DB.save(newProfile);
//         return await this.getProfile({ vendor_uuid: props.vendor_uuid });
//     }

//     static async getProfile(props) {
//         let VendorProfileModel = schema.vendorProfileModel;
//         return await DB.find(VendorProfileModel, props, null);
//     }

//     static async updateProfile(searchProps, updateProps) {
//         let VendorProfileModel = schema.vendorProfileModel;
//         await DB.updateOne(VendorProfileModel, searchProps, updateProps);
//         return await DB.findOne(VendorProfileModel, searchProps, null);
//     }

//     static async getAllVendorsWithFilter(filter) {
//         filter.role = constants.USER_ROLES.VENDOR;
//         return await this.find(filter);
//     }

//     static async updateExistingProfile(vendorUUID, props) {
//         let existingProfile = await this.getProfile({ vendor_uuid: vendorUUID });
//         if (existingProfile[0]) {
//             if (props.vendor_uuid) delete props.vendor_uuid;
//             return await this.updateProfile({ vendor_uuid: vendorUUID }, props);
//         } else {
//             return await this.createProfile(props);
//         }
//     }

// }

// module.exports = Vendor;