// let schema = require('./../libs/Schema');
// let Utils = require('./../utils/Utils');
// let DB = require('./../libs/DB');
// class Subscription {
//     constructor() { }

//     static async save(props) {
//         props.uuid = props.uuid || Utils.getUUID();
//         let SubscriptionModel = schema.subscriptionModel;
//         let newSubscription = new SubscriptionModel(props);
//         await DB.save(newSubscription);
//         return DB.findOne(SubscriptionModel, { uuid: props.uuid }, null);
//     }

//     static async findOne(props) {
//         let SubscriptionModel = schema.subscriptionModel;
//         return await DB.findOne(SubscriptionModel, props, null);
//     }

//     static async find(props) {
//         let SubscriptionModel = schema.subscriptionModel;
//         return await DB.find(SubscriptionModel, props, null);
//     }

//     static async update(searchProps, updateProps) {
//         let SubscriptionModel = schema.subscriptionModel;
//         await DB.updateOne(SubscriptionModel, searchProps, updateProps);
//         return await this.findOne(searchProps);
//     }

//     static async delete(props) {
//         let SubscriptionModel = schema.subscriptionModel;
//         return await DB.deleteOne(SubscriptionModel, props);
//     }

//     static async getDistinctFeild(searchProps, distinctField) {
//         let SubscriptionModel = schema.subscriptionModel;
//         return await DB.getDistinctFeild(SubscriptionModel, searchProps, distinctField);
//     }

//     static async isSubscriptionExists(userUUID, vendorUUID) {
//         let searchProps = { user_uuid: userUUID, vendor_uuid: vendorUUID };
//         let result = await this.findOne(searchProps);
//         if (result) return result;
//         return false;
//     }

//     static async subscribeUserToVendor(userUUID, vendorUUID, type) {
//         let createProps = { user_uuid: userUUID, vendor_uuid: vendorUUID, type: type };
//         let isExists = await this.isSubscriptionExists(userUUID, vendorUUID);
//         if (isExists) return isExists;
//         let newSubscription = await this.save(createProps);
//         return newSubscription;
//     }

//     static async getUniqueVendorUUIDSubscribedByUser(userUUID) {
//         return await this.getDistinctFeild({ user_uuid: userUUID }, 'vendor_uuid');
//     }

//     static async getUniqueCustomerUUIDSubscribedForVendor(vendorUUID) {
//         return await this.getDistinctFeild({ vendor_uuid: vendorUUID }, 'user_uuid');
//     }

//     static async getParticularSubscriptionDetails(userUUID, vendorUUID) {
//         return await this.findOne({ user_uuid: userUUID, vendor_uuid: vendorUUID });
//     }
// }

// module.exports = Subscription;