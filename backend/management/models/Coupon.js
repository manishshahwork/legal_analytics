// let schema = require('./../libs/Schema');
// let Utils = require('./../utils/Utils');
// let constants = require('./../constants/constants');
// let DB = require('./../libs/DB');

// let Incentive = require('./IncentiveSchema');
// let logger = require('./../libs/Logger');

// class Coupon {
//     constructor() { }

//     static async save(props) {
//         props.uuid = props.uuid || Utils.getUUID();
//         let CouponModel = schema.couponModel;
//         let newCoupon = new CouponModel(props);
//         await DB.save(newCoupon);
//         return await this.findOne(props);
//     }

//     static async saveMany(arrayProps, bodyProps) {
//         //       bodyProps.uuid = bodyProps.uuid || Utils.getUUID();
//         let BatchCouponModel = schema.batchCouponModel;
//         return await DB.saveMany(BatchCouponModel, arrayProps);
//         // return await this.findOne(bodyProps);
//     }
//     static async getAllBatchCoupons(props) {
//         let BatchCouponModel = schema.batchCouponModel;
//         return await DB.find(BatchCouponModel, props, null);
//     }

//     static async findOne(props) {
//         let CouponModel = schema.couponModel;
//         return await DB.findOne(CouponModel, props, null);
//     }

//     static async find(props) {
//         let CouponModel = schema.couponModel;
//         return await DB.find(CouponModel, props, null);
//     }

//     static async update(searchProps, updateProps) {
//         let CouponModel = schema.couponModel;
//         await DB.updateOne(CouponModel, searchProps, updateProps);
//         return await this.findOne(searchProps);
//     }

//     static async delete(props) {
//         let CouponModel = schema.couponModel;
//         return await DB.deleteOne(CouponModel, props);
//     }

//     static async getDistinctFeild(searchProps, distinctField) {
//         let CouponModel = schema.couponModel;
//         return await DB.getDistinctFeild(CouponModel, searchProps, distinctField);
//     }

//     static async getByUUID(uuid) {
//         return await this.findOne({ uuid: uuid });
//     }

//     static async getAllCoupons(filter) {
//         filter.type = constants.COUPON_TYPE.COUPON;
//         return await this.find(filter);
//     }

//     static async getAllRewards(filter) {
//         filter.type = constants.COUPON_TYPE.REWARD;
//         return await this.find(filter);
//     }

//     static async getCouponsUploadedByVendor(vendorUUID) {
//         if (!vendorUUID) throw new Error('Invalid Vendor UUID');
//         let props = {};
//         props.vendor_uuid = vendorUUID;
//         props.type = constants.COUPON_TYPE.COUPON;
//         return await this.find(props);
//     }

//     //################# MHS
//     /**
//      * Get all incentives
//      * @param {*} vendorUUID 
//      * @param {*} incentiveType 
//      */
//     static async getAllIncentivesUploadedByVendor(vendorUUID, incentiveType) {
//         if (!vendorUUID) throw new Error('Invalid Vendor UUID');
//         //build a string query
//         let CouponModel = schema.couponModel;
//         return await CouponModel.find().
//             where('vendor_uuid').equals(vendorUUID).
//             where('type').equals(incentiveType);
//     }

//     /**
//      * 
//      * @param {*} vendorUUID 
//      * @param {*} filter 
//      */
//     static async getRewardsUploadedByVendor(vendorUUID, filter) {
//         if (!vendorUUID) throw new Error('Invalid Vendor UUID');
//         console.log("filter::: " + filter);
//         if (typeof (filter) == null || typeof (filter) == undefined || filter.trim() == "" || filter === "NULL") {
//             let props = {};
//             props.vendor_uuid = vendorUUID;
//             props.type = constants.COUPON_TYPE.REWARD;
//             console.log("filter is null, so returning all rewards");
//             return await this.find(props);
//         }
//         else {
//             //build a string query
//             let CouponModel = schema.couponModel;
//             return await CouponModel.find().
//                 where('vendor_uuid').equals(vendorUUID).
//                 where('type').equals(constants.COUPON_TYPE.REWARD).
//                 where('name').equals({ $regex: '.*' + filter + '.*', $options: 'i' });

//         }
//     }
//     /**
//      * searchType = All, Active, Inactive, Deleted, Expired
//      */
//     static async getSpecificIncentivesUploadedByVendor(vendorUUID, incentiveType, searchNameString, primarySearchType, searchType, searchStartTime, searchEndTime) {
//         if (!vendorUUID) throw new Error('Invalid Vendor UUID');
//         logger.logString("searchNameString:" + searchNameString);
//         logger.logString("searchType: " + searchType);
//         if (typeof (searchNameString) == null || typeof (searchNameString) == undefined || searchNameString.trim() == "" || searchNameString === "NULL") {
//             logger.logString("NNNNNNNNNNNNUUUUUUUUULLLLLLLLLLLLLL");
//             searchNameString = "";
//         }
//         //build a string query
//         let CouponModel = schema.couponModel;
//         let query = CouponModel.find();
//         query.where('vendor_uuid').equals(vendorUUID);
//         query.where('type').equals(incentiveType);
//         query.where('name').equals({ $regex: '.*' + searchNameString + '.*', $options: 'i' });


//         //based on primary search type, kindly change the logic
//         if (primarySearchType == "Batch")
//             query.where('is_batch').equals("true");
//         else if (primarySearchType == "Normal")
//             query.where('is_batch').equals("false");
//         // if (searchType == "Batch") query.where('is_batch').equals("true");

//         if (searchType == "Active") query.where('is_active').equals("true");
//         if (searchType == "Inactive") query.where('is_active').equals("false");
//         if (searchType == "Expired") query.where('end_time').lte(Date.now());
//         if (searchType == "Deleted") query.where('is_deleted').equals("true");

//         if (searchStartTime != 0) query.where('created_on').gte(searchStartTime);
//         if (searchEndTime != 0) query.where('created_on').lte(searchEndTime);

//         //execute the query
//         return await query.exec();

//     }

//     /**
//      * fieldToCount
//         1 - Total
//         2 - Active
//         3 - Inactive
//         4 - Deleted
//         5 - Expired
//         6 - Redeemed
//      * @param {*} vendorUUID 
//      * @param {*} fieldToCount 
//      * @param {*} searchStartTime 
//      * @param {*} searchEndTime 
//      */
//     static getSpecificRewardCounts(vendorUUID, fieldToCount, searchStartTime, searchEndTime) {
//         logger.logString("getSpecificRewardCounts: About to search...");
//         if (!vendorUUID) throw new Error('Invalid Vendor UUID');
//         //build a string query
//         let CouponModel = schema.couponModel;
//         let query = CouponModel.count();
//         query.where('vendor_uuid').equals(vendorUUID);
//         query.where('type').equals(constants.COUPON_TYPE.REWARD);

//         if (searchStartTime != 0) query.where('start_time').gte(searchStartTime);
//         if (searchEndTime != 0) query.where('end_time').lte(searchEndTime);

//         if (fieldToCount == 2) query.where("is_active").equals("true");
//         if (fieldToCount == 3) query.where("is_active").equals("false");
//         if (fieldToCount == 4) query.where("is_deleted").equals("true");
//         if (fieldToCount == 5) {
//             var currentTS = Date.now();
//             query.where("end_time").lte(currentTS);
//         }
//         //execute the query
//         return query.exec();
//     }

//     /**
//      * fieldToCount
//         1 - Total
//         2 - Active
//         3 - Inactive
//         4 - Deleted
//         5 - Expired
//         6 - Redeemed
//      * @param {*} vendorUUID 
//      * @param {*} fieldToCount 
//      * @param {*} searchStartTime 
//      * @param {*} searchEndTime 
//      */
//     static getSpecificIncentiveCounts(vendorUUID, incentiveType, fieldToCount, searchStartTime, searchEndTime) {
//         logger.logString("getSpecificIncentiveCounts: About to search...");
//         if (!vendorUUID) throw new Error('Invalid Vendor UUID');
//         //build a string query
//         let CouponModel = schema.couponModel;
//         let query = CouponModel.count();
//         query.where('vendor_uuid').equals(vendorUUID);
//         query.where('type').equals(incentiveType);

//         if (searchStartTime != 0) query.where('start_time').gte(searchStartTime);
//         if (searchEndTime != 0) query.where('end_time').lte(searchEndTime);

//         if (fieldToCount == 2) query.where("is_active").equals("true");
//         if (fieldToCount == 3) query.where("is_active").equals("false");
//         if (fieldToCount == 4) query.where("is_deleted").equals("true");
//         if (fieldToCount == 5) {
//             var currentTS = Date.now();
//             query.where("end_time").lte(currentTS);
//         }
//         //execute the query
//         return query.exec();
//     }

//     static saveManyIncentives(incentivesArray) {
//         let CouponModel = schema.couponModel;
//         return CouponModel.insertMany(incentivesArray);

//     }

//     static async getVendorsUUIDBasedOnCouponCategory(category) {
//         return await this.getDistinctFeild({ category: category, type: constants.COUPON_TYPE.COUPON }, 'vendor_uuid');
//     }

//     static async deactivateBatchIncentives(vendorUUID, incentivesArr) {
//         if (!vendorUUID) throw new Error('Invalid Vendor UUID');
//         //build a string query
//         let CouponModel = schema.couponModel;
//         let query = CouponModel.updateMany();
//         query.where('vendor_uuid').equals(vendorUUID);
//         // query.where('is_batch').equals(true);//only batch coupons
//         query.where('uuid').in(incentivesArr);
//         query.updateMany({ 'is_active': false });

//         // return new Promise(resolve => {
//         //     resolve(query.exec());
//         // })

//         return await query.exec();
//     }
// }
// module.exports = Coupon;