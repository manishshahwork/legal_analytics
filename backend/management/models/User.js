// let schema = require('./../libs/Schema');
// let Utils = require('./../utils/Utils');
// let DB = require('./../libs/DB');

// class User {
//     constructor() { }

//     static async save(props) {
//         props.uuid = Utils.getUUID();
//         let UserModel = schema.userModel;
//         let newUser = new UserModel(props);
//         let savedUser = await this.findOne({ email: props.email });
//         if (savedUser) return savedUser;
//         await DB.save(newUser);
//         return await DB.findOne(UserModel, { email: props.email }, null);
//     }

//     static async findOne(props, returnFields) {
//         let UserModel = schema.userModel;
//         return await DB.findOne(UserModel, props, returnFields);
//     }

//     static async find(props) {
//         let UserModel = schema.userModel;
//         return await DB.find(UserModel, props, null)
//     }

//     static async update(searchProps, updateProps) {
//         let UserModel = schema.userModel;
//         await DB.updateOne(UserModel, searchProps, updateProps);
//         return this.findOne(searchProps);
//     }

//     static async delete(props) {
//         let UserModel = schema.userModel;
//         return await DB.deleteOne(UserModel, props);
//     }

//     static async getAllUsersWithFilter(filter) {
//         return await this.find(filter);
//     }

//     static async getProfile(props) {
//         let VendorProfileModel = schema.vendorProfileModel;
//         return await DB.find(VendorProfileModel, props, null);
//     }

//     static async updateExistingProfile(vendorUUID, props) {
//         let existingProfile = await this.getProfile({ vendor_uuid: vendorUUID });
//         if (existingProfile[0]) {
//             if (props.vendor_uuid) delete props.vendor_uuid;
//             return await this.updateProfile({ vendor_uuid: vendorUUID }, props);
//         } else {
//             return await this.createProfile(props);
//         }
//     }

//     static setRole(uuid, role) {
//         let UserModel = schema.userModel;
//         return new Promise(async function (resolve, reject) {
//             let saved = await this.findOne({ uuid: uuid });
//             if (saved.role) {
//                 resolve(saved);
//             } else {
//                 let updateResult = await this.update({ uuid: uuid }, { role: role });
//                 let updated = await this.findOne({ uuid: uuid });
//                 resolve(updated);
//             }
//         }.bind(this));
//     }

//     static async getProfileForVendor(uuid) {
//         return await this.findOne({ uuid: uuid }, ['name', 'email', 'uuid']);
//     }

//     getOffersFromRegisteredVendors(userUUID, type) {
//         let UserModel = schema.userModel;
//         return new Promise(function (resolve, reject) {
//             UserModel.aggregate([
//                 { '$match': { 'uuid': userUUID } },
//                 {
//                     '$lookup': {
//                         'from': 'registers',
//                         'localField': 'uuid',
//                         'foreignField': 'user_uuid',
//                         'as': 'registeredVendors'
//                     }
//                 },
//                 {
//                     '$lookup': {
//                         'from': 'users',
//                         'localField': 'registeredVendors.vendor_uuid',
//                         'foreignField': 'uuid',
//                         'as': 'vendorDetails'
//                     }
//                 },
//                 { '$unwind': '$vendorDetails' },
//                 {
//                     '$lookup': {
//                         'from': 'coupons',
//                         'localField': 'vendorDetails.uuid',
//                         'foreignField': 'vendor_uuid',
//                         'as': 'vendorDetails.coupons'
//                     }
//                 },
//                 { '$unwind': '$vendorDetails.coupons' },
//                 { '$match': { 'vendorDetails.coupons.type': type } },
//                 {
//                     '$project': {
//                         'vendorDetails': 1
//                     }
//                 }

//             ]).exec(function (err, result) {
//                 if (err) reject(err);
//                 resolve(result);
//             });
//         });
//     }

//     //1 - google
//     authenticateSocialUser(socialType = 1) {
//         let UserModel = schema.userModel;
//         // return CouponModel.insertMany(incentivesArray);
//         try {
//             if (socialType == 1) {
//                 let UserModel = new schema.UserModel(req.body);
//                 logger.logString("usermodel::: ");
//                 logger.logObj(UserModel);

//                 // let query = UserModel.findOne();
//                 // query.where('google_id').equals(req)    
//                 response.success(req, res, null, null);
//             }
//         } catch (error) {
//             logger.error(req, res, req.body, error);
//             response.serverError(req, res, null, null);
//         }

//     }

// }

// module.exports = User;