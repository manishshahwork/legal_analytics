// let schema = require('./../libs/Schema');
// let Utils = require('./../utils/Utils');
// let DB = require('./../libs/DB');
// let constants = require('./../constants/constants');

// class Transaction {
//     constructor() { }

//     static async save(props) {
//         props.uuid = props.uuid || Utils.getUUID();
//         let TransactionModel = schema.transactionModel;
//         let newTransaction = new TransactionModel(props);
//         await DB.save(newTransaction);
//         return await DB.findOne(TransactionModel, { uuid: props.uuid }, null);
//     }

//     static async findOne(props) {
//         let TransactionModel = schema.transactionModel;
//         return await DB.findOne(TransactionModel, props, null);
//     }

//     static async find(props) {
//         let TransactionModel = schema.transactionModel;
//         return await DB.find(TransactionModel, props, null);
//     }

//     static async update(searchProps, updateProps) {
//         let TransactionModel = schema.transactionModel;
//         await DB.updateOne(TransactionModel, searchProps, updateProps);
//         return await DB.findOne(TransactionModel, searchProps, null);
//     }

//     static async delete(props) {
//         let TransactionModel = schema.transactionModel;
//         return await DB.deleteOne(TransactionModel, props)
//     }

//     static async getDistinctFeild(searchProps, distinctField) {
//         let TransactionModel = schema.transactionModel;
//         return await DB.getDistinctFeild(TransactionModel, searchProps, distinctField);
//     }


//     static async createEarningEntry(props) {
//         if (!props.item_type) new Error('Required property type not Exists.');
//         if(props.item_type === constants.COUPON_TYPE.COUPON) return await this.createCouponEarningEntry(props);
//         return await this.createRewardEarningEntry(props); 
//     }

//     static async createExpenseEntry(props) {
//         console.log(props);
//         if (!props.item_type) new Error('Required property type not Exists.');
//         if(props.item_type === constants.COUPON_TYPE.COUPON) return await this.createCouponExpenseEntry(props);
//         return await this.createRewardExpenseEntry(props); 
//     }

//     static async createRewardEarningEntry(props) {
//         if (!props.reward_points || !props.item_uuid) throw new Error('Required data not Exists.');
//         props.type = constants.TRANSACTION_TYPE.EARNING;
//         props.item_type = constants.COUPON_TYPE.REWARD;
//         return await this.save(props);
//     }

//     static async createCouponEarningEntry(props) {
//         if (!props.coupon_code || !props.item_uuid) throw new Error('Required data not Exists.');
//         props.type = constants.TRANSACTION_TYPE.EARNING;
//         props.item_type = constants.COUPON_TYPE.COUPON;
//         return await this.save(props);
//     }

//     static async createRewardExpenseEntry(props) {
//         if (!props.reward_points || !props.item_uuid || !props.eraning_transaction_uuid) throw new Error('Required data not Exists.');
//         props.type = constants.TRANSACTION_TYPE.EXPENSE;
//         props.item_type = constants.COUPON_TYPE.REWARD;
//         return await this.save(props);
//     }

//     static async createCouponExpenseEntry(props) {
//         if (!props.coupon_code || !props.item_uuid || !props.eraning_transaction_uuid) throw new Error('Required data not Exists.');
//         props.type = constants.TRANSACTION_TYPE.EXPENSE;
//         props.item_type = constants.COUPON_TYPE.COUPON;
//         return await this.save(props);
//     }

//     static async getTransaction(props) {
//         return await this.find(props);
//     }

//     // returns detailed transaction for uuid
//     static async getTransactionByUUID(uuid) {
//         return await this.findOne({ uuid: uuid });
//     }

//     // returns earning transaction for particular t-uuid
//     static async getEarningTransactionByUUID(uuid) {
//         return await this.find({ uuid: uuid, type: constants.TRANSACTION_TYPE.EARNING });
//     }

//     // returns expense transaction for particular t-uuid
//     static async getExpenseTransactionByUUID(uuid) {
//         return await this.find({ uuid: uuid, type: constants.TRANSACTION_TYPE.EXPENSE });
//     }

//     // retuns corresponding expense transaction for particulat earning transaction uuid
//     static async getExpenseTransactionForEarningTransactionUUID(uuid) {
//         return await this.find({ eraning_transaction_uuid: uuid, type: constants.TRANSACTION_TYPE.EXPENSE });
//     }

//     // returns corresponding expense and earning for particular transaction uuid
//     static async getEarningExpenseTransactionForUUID(uuid) {
//         return {
//             earnings: await this.getEarningTransactionByUUID(uuid),
//             expenses: await this.getExpenseTransactionForEarningTransactionUUID(uuid)
//         };
//     }

//     // returns corrsponding earning transaction for item-uuid
//     static async getEarningTransactionByItemUUID(uuid) {
//         return await this.find({ item_uuid: uuid, type: constants.TRANSACTION_TYPE.EARNING });
//     }

//     // returns corresponding expense transaction for item-uuid
//     static async getExpenseTransactionByItemUUID(uuid) {
//         return await this.find({ item_uuid: uuid, type: constants.TRANSACTION_TYPE.EXPENSE });
//     }

//     // returns all expense/eraning transaction for particular item-uuid
//     static async getAllTransactionsForItemUUID(uuid) {
//         return {
//             earnings: await this.getEarningTransactionByItemUUID(uuid),
//             expenses: await this.getExpenseTransactionByItemUUID(uuid)
//         }
//     }

//     // returns all Earning transaction for particular user
//     static async getEarningTransactionsForUserUUID(uuid) {
//         return await this.find({ user_uuid: uuid, type: constants.TRANSACTION_TYPE.EARNING })
//     }

//     // returns all Expense transaction for particular user
//     static async getExpenseTransactionsForUserUUID(uuid) {
//         return await this.find({ user_uuid: uuid, type: constants.TRANSACTION_TYPE.EXPENSE })
//     }

//     // return all Transaction for user-uuid
//     static async getAllTransactionsForUserUUID(uuid) {
//         return {
//             earnings: await this.getEarningTransactionsForUserUUID(uuid),
//             epxenses: await this.getExpenseTransactionsForUserUUID(uuid)
//         }
//     }

//     // return all transaction for paricular user and item
//     static async getUsersEarningTransactionForItemUUID(itemUUID, userUUID){
//         return await this.find({user_uuid: userUUID, item_uuid: itemUUID, type:constants.TRANSACTION_TYPE.EARNING});
//     }
// }

// module.exports = Transaction;