const constants = require('./../constants/constants');
class SessionHandler{
    constructor(){};

    static async setAcceesCookie(req, res, accessToken){
        res.cookie(constants.COOKIE.NAME.USER, accessToken, {});
    }
}

module.exports = SessionHandler;