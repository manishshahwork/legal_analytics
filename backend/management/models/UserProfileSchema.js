const mongoose = require("mongoose");
let Utils = require('../utils/Utils');

const UserProfileSchema = mongoose.Schema({
    uuid: { type: String, required: true, unique: true },
    first_name: { type: String },
    middle_name: { type: String },
    last_name: { type: String },
    full_name: { type: String },
    login_type: { type: String },
    facebook_id: { type: String },
    google_id: { type: String },
    phone: { type: Number },
    date_of_birth: { type: String },
    salutation: { type: String },
    profile_link: { type: String },
    email: { type: String },
    created_on: { type: Number, required: true, default: Utils.getCurrentUTCTimestamp() },
    last_modified: { type: Number, required: true, default: Utils.getCurrentUTCTimestamp() },
    has_completed_profile: { type: Boolean, default: false },
    login_name: { type: String },
    password: { type: String },
    vendor_uuid: { type: String },
    role_uuid: { type: String }

});

module.exports = mongoose.model('UserProfile', UserProfileSchema, 'user_profiles');
