const mongoose = require("mongoose");
let Utils = require('../utils/Utils');

const LegalDocumentSchema = mongoose.Schema({
    uuid: { type: String },
    document_name: { type: String },
    document_url: { type: String },
    description: { type: String },
    copy_from_contract: { type: String },
    test_project: { type: Boolean },
    base_language: { type: String },
    hierarchical_type: { type: String },
    supplier: { type: String },
    affected_parties: { type: String },
    proposed_contract_amount: { type: String },
    original_contract_amount: { type: String },
    commodity: { type: String },
    regions: { type: String },
    predecessor_project: { type: String },
    option_to_renew: { type: Boolean },
    term_type: { type: String },
    effective_date: { type: Number },
    expirateion_date: { type: Number },

    processed_document_url: { type: String },
    processed_document_name: { type: String },
    processed_document_lines: { type: Number },
    is_document_uploaded: { type: Boolean },
    is_document_processed: { type: Boolean },

    assigned_to_user: { type: String },
    processed_document_text: { type: String }

});

module.exports = mongoose.model('LegalDocument', LegalDocumentSchema, 'legal_documents');
