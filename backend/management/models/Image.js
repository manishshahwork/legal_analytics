// let schema = require('./../libs/Schema');
// let Utils = require('./../utils/Utils');
// let DB = require('./../libs/DB');

// class Image {
//     constructor() { }

//     static async save(props) {
//         props.uuid = props.uuid || Utils.getUUID();
//         let saved = await this.get({ belongs_to: props.belongs_to, type: props.type });
//         if (saved.length > 0) {
//             return await this.update({ uuid: saved[0].uuid }, { data: props.data, type: props.type });
//         }
//         let ImageModel = schema.imageModel;
//         let newImage = new ImageModel(props);
//         return await DB.save(newImage);
//     }

//     static async get(props) {
//         let ImageModel = schema.imageModel;
//         return await DB.find(ImageModel, props, null);
//     }

//     static async update(searchProps, updateProps) {
//         let ImageModel = schema.imageModel;
//         return await DB.updateOne(ImageModel, searchProps, updateProps);
//     }
//     static async getCouponImages(couponUUID) {
//         if (!couponUUID) throw new Error('Invalid coupon UUID');
//         let props = {};
//         props.belongs_to = couponUUID;
//         return await this.get(props);
//     }

//     static async getProfileImages(vendorUUID) {
//         let props = {}
//         props.belongs_to = vendorUUID;
//         return await this.get(props);
//     }

//     static async getImageBelongsTo(uuid, type) {
//         let props = { belongs_to: uuid, type: type };
//         return await this.get(props);
//     }
// }

// module.exports = Image;