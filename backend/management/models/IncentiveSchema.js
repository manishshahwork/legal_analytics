// const mongoose = require("mongoose");
// const Schema = mongoose.Schema;
// let constants = require('./../constants/constants');
// let Utils = require('./../utils/Utils');

// const IncentiveSchema = new Schema({
//     uuid: { type: String, required: true, unique: true },
//     vendor_uuid: { type: String, required: true},
//     name: { type: String, required: true },
//     code: { type: String },
//     type: { type: String, required: true, default: constants.COUPON_CATEGORY.COUPON_CATEGORY },
//     summary: { type: String, required: true },
//     category: { type: String, default: constants.COUPON_CATEGORY.GENERAL },
//     image_link: { type: String },
//     image_url: { type: String },
//     video_url: { type: String },
//     description: { type: String },
//     product_url: { type: String },
//     reward_points: { type: Number },
//     is_verified: { type: Boolean, required: true, default: false },
//     sub_category: { type: String, default: constants.COUPON_CATEGORY.GENERAL },
//     discount_type: { type: String, default: constants.DISCOUNT_TYPE.DISCOUNT },
//     discount_value: { type: Number, default: 0 },
//     end_time: { type: Number, default: 0 },
//     start_time: { type: Number, required: true, default: Utils.getCurrentUTCTimestamp() },
//     created_on: { type: Number, required: true, default: Utils.getCurrentUTCTimestamp() },
//     last_modified: { type: Number, required: true, default: Utils.getCurrentUTCTimestamp() },
//     is_active: { type: Boolean, required: true, default: false },
//     is_deleted: { type: Boolean, required: true, default: false },
//     is_batch: { type: Boolean, required: false, default: false },
//     batch_name: { type: String},
//     batch_count: { type: Number, required: false},
//     user_uuid: { type: String}
// });

// module.exports = mongoose.model('incentive', IncentiveSchema, 'incentives');