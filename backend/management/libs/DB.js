let Utils = require('./../utils/Utils');

class DB {
    constructor() { }

    static save(model) {
        return new Promise(function (resolve, reject) {
            model.save(function (err) {
                if (err) reject(err);
                resolve(true);
            });
        }.bind(this));
    }

    static saveMany(model, arrayProps, bodyProps) {
        return new Promise(function (resolve, reject) {
            model.insertMany(arrayProps, function (err) {
                if (err) reject(err);
                resolve(true);
            });
        }.bind(this));
    }

    static findOne(model, searchProps, returnFields) {
        return new Promise(function (resolve, reject) {
            model.findOne(searchProps, returnFields, function (err, result) {
                if (err) reject(err);
                resolve(result);
            })
        }.bind(this));
    }

    static find(model, searchProps, returnFields) {
        return new Promise(function (resolve, reject) {
            model.find(searchProps, returnFields, function (err, result) {
                if (err) reject(err);
                resolve(result);
            })
        }.bind(this));
    }

    static updateOne(model, searchProps, updateProps) {
        updateProps.last_modified = Utils.getCurrentUTCTimestamp();
        return new Promise(function (resolve, reject) {
            model.updateOne(searchProps, updateProps, function (err) {
                if (err) reject(err);
                resolve(true);
            })
        }.bind(this));
    }

    static deleteOne(model, searchProps) {
        return new Promise(function (resolve, reject) {
            model.deleteOne(searchProps, function (err) {
                if (err) reject(err);
                resolve(true);
            })
        }.bind(this));
    }

    static async getDistinctFeild(model, searchProps, distinctField) {
        return new Promise(function (resolve, reject) {
            model.find(searchProps).distinct(distinctField, function (err, result) {
                if (err) reject(err);
                resolve(result);
            });
        }.bind(this));
    }

    static async count(model, searchProps) {
        console.log("model:: " + model + " props: ");
        console.log(searchProps);
        let k = model.count(searchProps);
        console.log("k:::");
        console.log(k);

        await model.count(searchProps, function (err, docs) {
            console.log("99999999999999999999");
            console.log(docs);
            return docs;
        })
    }
}

module.exports = DB;
