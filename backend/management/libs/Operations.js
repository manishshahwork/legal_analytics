const logger = require('./Logger');
const Incentive = require('../models/IncentiveSchema');

class Operations {
    /**
     * fieldToCount
        1 - Total
        2 - Active
        3 - Inactive
        4 - Deleted
        5 - Expired
        6 - Redeemed
     * @param {*} vendorUUID 
     * @param {*} incentiveType 
     * @param {*} fieldToCount 
     * @param {*} searchStartTime 
     * @param {*} searchEndTime 
     */
    static getSpecificIncentiveCounts(vendorUUID, incentiveType, fieldToCount, searchStartTime, searchEndTime) {
        console.log("oooooooooooooooooo");
        logger.logString("Operations:getSpecificIncentiveCounts: About to search...");
        if (!vendorUUID) throw new Error('Invalid Vendor UUID');
        //build a string query
        let query = Incentive.count();
        query.where('vendor_uuid').equals(vendorUUID);
        query.where('type').equals(incentiveType);

        if (searchStartTime != 0) query.where('start_time').gte(searchStartTime);
        if (searchEndTime != 0) query.where('end_time').lte(searchEndTime);

        if (fieldToCount == 2) query.where("is_active").equals("true");
        if (fieldToCount == 3) query.where("is_active").equals("false");
        if (fieldToCount == 4) query.where("is_deleted").equals("true");
        if (fieldToCount == 5) {
            var currentTS = Date.now();
            query.where("end_time").lte(currentTS);
        }
        //execute the query
        return query.exec();
    }
}//end of class

module.exports = Operations;