// let mongoose = require('mongoose');
// let Schema = mongoose.Schema;
// let Utils = require('./../utils/Utils');
// let constants = require('./../constants/constants');
// let keys = require('./../constants/keys')
// mongoose.Promise = global.Promise;
// // try {
// //     mongoose.connect(keys.getMongoDBURL(), { useMongoClient: true });
// // } catch (error) {
// //     console.log(error.stack);
// // }

// // define user schema
// //  let userSchema = new Schema({
// //      uuid: { type: String, required: true, unique: true },
// //      name: { type: String },
// //      role: { type: String },
// //      facebook_id: { type: String },
// //      google_id: { type: String },
// //      email: { type: String, required: true, unique: true, lowercase: true, trim: true },
// //      created_on: { type: Number, required: true, default: Utils.getCurrentUTCTimestamp() },
// //      last_modified: { type: Number, required: true, default: Utils.getCurrentUTCTimestamp() }
// //  });

//  let userSchema = new Schema({
//      uuid: { type: String, required: true, unique: true },
//      name: { type: String },
//      role: { type: String },
//      facebook_id: { type: String },
//      google_id: { type: String },
//      phone : {type: Number },
//      date_of_birth:{type: String},
//      gender : {type: String},
//      profile_link: { type: String },
//      email: { type: String, required: true, unique: true, lowercase: true, trim: true },
//      created_on: { type: Number, required: true, default: Utils.getCurrentUTCTimestamp()},
//      last_modified: { type: Number, required: true, default: Utils.getCurrentUTCTimestamp()}
//  })

// // define vendor profile schema
// let vendorProfileSchema = new Schema({
//     uuid: { type: String, unique: true, required: true },
//     vendor_uuid: { type: String, required: true, unique: true },
//     about: { type: String },
//     name: { type: String },
//     website: { type: String },
//     address: { type: String },
//     start_time: { type: String },
//     end_time: { type: String },
//     working_days: { type: Object },
//     color_scheme: { type: String },
//     profile_link: { type: String },
//     cover_link: { type: String },
//     logo_link: { type: String },
//     latitude: { type: Number },
//     longitude: { type: Number },
//     created_on: { type: Number, required: true, default: Utils.getCurrentUTCTimestamp() },
//     last_modified: { type: Number, required: true, default: Utils.getCurrentUTCTimestamp() },
// });

// // define coupon schema
// let couponSchema = new Schema({
//     uuid: { type: String, required: true, unique: true },
//     name: { type: String, required: true },
//     code: { type: String },
//     type: { type: String, required: true, default: constants.COUPON_TYPE.COUPON },
//     summary: { type: String, required: true },
//     category: { type: String, default: constants.COUPON_CATEGORY.GENERAL },
//     image_link: { type: String },
//     image_url: { type: String },
//     video_url: { type: String },
//     vendor_uuid: { type: String, required: true },
//     description: { type: String },
//     product_url: { type: String },
//     reward_points: { type: Number },
//     is_verified: { type: Boolean, required: true, default: false },
//     sub_category: { type: String, default: constants.COUPON_CATEGORY.GENERAL },
//     discount_type: { type: String, default: constants.DISCOUNT_TYPE.DISCOUNT },
//     discount_value: { type: Number, default: 0 },
//     end_time: { type: Number, default: 0 },
//     start_time: { type: Number, required: true, default: Utils.getCurrentUTCTimestamp() },
//     created_on: { type: Number, required: true, default: Utils.getCurrentUTCTimestamp() },
//     last_modified: { type: Number, required: true, default: Utils.getCurrentUTCTimestamp() },
//     is_active: { type: Boolean, required: true, default: false },
//     is_deleted: { type: Boolean, required: true, default: false },
//     is_batch: { type: Boolean, required: false, default: false },
//     batch_name: { type: String},
//     batch_count: { type: Number, required: false},
// });

// // define image schema
// let imageSchema = new Schema({
//     uuid: { type: String, required: true, unique: true },
//     type: { type: String, required: true },
//     data: { type: String, required: true },
//     belongs_to: { type: String, required: true },
//     created_on: { type: Number, required: true, default: Utils.getCurrentUTCTimestamp() },
//     last_modified: { type: Number, required: true, default: Utils.getCurrentUTCTimestamp() },
// });

// // define register schema
// let subscriptionSchema = new Schema({
//     uuid: { type: String, required: true, unique: true, default: Utils.getUUID() },
//     type: { type: Object },
//     user_uuid: { type: String, required: true },
//     vendor_uuid: { type: String, required: true },
//     created_on: { type: Number, required: true, default: Utils.getCurrentUTCTimestamp() },
//     last_modified: { type: Number, required: true, default: Utils.getCurrentUTCTimestamp() },
// });

// // define purchase schema
// let purchaseSchema = new Schema({
//     uuid: { type: String, required: true, unique: true, default: Utils.getUUID() },
//     user_uuid: { type: String, required: true },
//     coupon_uuid: { type: String, required: true },
//     rewards_earned: { type: Number, required: true, default: 0 },
//     created_on: { type: Number, required: true, default: Utils.getCurrentUTCTimestamp() },
//     last_modified: { type: Number, required: true, default: Utils.getCurrentUTCTimestamp() },
// });

// // see the document for details,
// // https://docs.google.com/document/d/1A9O8Z_CMjQdfWSU2TZmrv-8wO9qF-2zskOOqjadZyQY/edit?usp=sharing
// let transactionSchema = new Schema({
//     uuid: { type: String, required: true, unique: true, default: Utils.getUUID() },
//     user_uuid: { type: String, required: true },
//     type: { type: String, required: true },
//     item_type: { type: String, required: true },
//     item_uuid: { type: String, required: true },
//     item_name: { type: String, required: true },
//     item_summary: { type: String, required: true },
//     eraning_transaction_uuid: { type: String }, // required, when type = expense, should be handled at code level.
//     reward_points: { type: Number },
//     coupon_code: { type: String },
//     item_start_time: { type: Number, required: true },
//     item_end_time: { type: Number, required: true },
//     created_on: { type: Number, required: true, default: Utils.getCurrentUTCTimestamp() },
//     last_modified: { type: Number, required: true, default: Utils.getCurrentUTCTimestamp() }
// });

// // define batchCoupon schema
// let batchCouponSchema = new Schema({
//     uuid: { type: String, required: true, unique: true },
//     created: { type: Number, required: true, default: Utils.getCurrentUTCTimestamp() },
//     created_by: { type: String, required: true },
//     one_time_use: { type: Boolean, required: true, default: false },
//     batch_name: { type: String, required: true },
//     code: { type: String },
//     summary: { type: String, required: true },
// });


// //################################## MHS
// //## new code added
// // define vendorCustomer schema - The details of customers that are registered with the vendors
// let vendorCustomerSchema = new Schema({
//     member_since: {type: String},
//     status : {type: String},
// });

// //################################## 

// module.exports = {
//     userModel: mongoose.model('User', userSchema),
//     imageModel: mongoose.model('Image', imageSchema),
//     couponModel: mongoose.model('Coupon', couponSchema),
//     vendorModel: mongoose.model('User', userSchema),
//     purchaseModel: mongoose.model('Purchase', purchaseSchema),
//     transactionModel: mongoose.model('Transaction', transactionSchema),
//     vendorProfileModel: mongoose.model('VendorProfile', vendorProfileSchema),
//     subscriptionModel: mongoose.model('Subscription', subscriptionSchema),
//     batchCouponModel: mongoose.model('BatchCoupon', batchCouponSchema),
//     vendorCustomerModel: mongoose.model('VendorCustomer', vendorCustomerSchema)
//     //vendorModel: mongoose.model('Vendor', vendorSchema)
// };