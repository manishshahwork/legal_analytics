class Response{
    constructor(){}

    static success(req, res, message,  data){
        res.status(200).json({
            status: 200,
            message: message || 'success',
            data: data
        });
    }

    static notFound(req, res, message, data){
        res.status(404).json({
            status: 404,
            message: message || 'not found',
            data: data
        });
    }

    static existsAlready(req, res, message, data){}

    static badRequest(req, res, message, data){
        res.status(400).json({
            status: 400,
            message: message || 'bad request or insuffecient information',
            data: data
        });
    }

    static serverError(req, res, message, data){
        res.status(500).json({
            status: 500,
            message: message || 'internal server error',
            data: data
        });
    }
}

module.exports = Response;