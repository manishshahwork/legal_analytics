var uuidv1 = require('uuid/v1');
const uuidv4 = require('uuid/v4');

class Utils {
    constructor() { }

    static getUUID() {
        return uuidv1();
    }

    static getUUIDV3() {
        return uuidv3();
    }

    static getUUIDV4() {
        return uuidv4();
    }

    static getCurrentUTCTimestamp() {
        var tmLoc = new Date();
        return tmLoc.getTime();
    }

    static isValidReq(req) {
        return req.isAuthenticated();
    }
    
}

module.exports = Utils;