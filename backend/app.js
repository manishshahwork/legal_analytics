const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const mongoose = require("mongoose");
const config = require("./management/config/db");
const app = express();
const keys = require('./management/constants/keys');
const jwt = require('jwt-simple');

//connect to database
// let database_name = keys.getMongoDBURL();
let database_name = config.db_url;
try {
    mongoose.connect(database_name, { useMongoClient: true });
} catch (error) {
    console.log(error.stack);
}

//check if connected
mongoose.connection.on('connected', function (){
    console.log("Connected to database: " + database_name);
});

//check if error in db connection
mongoose.connection.on('error', function (err){
    console.log("Error in connecting database: " + err);
});

const authRoutes = require('./management/routes/v1/auth-routes');
const commonRoutes = require('./management/routes/v1/common-routes');
const couponRoutes = require('./management/routes/v1/coupon-routes');
const transactionRoutes = require('./management/routes/v1/transaction-routes');
const userRoutes = require('./management/routes/v1/user-routes');
const vendorRoutes = require('./management/routes/v1/vendor-routes');
const incentiveRoutes = require('./management/routes/v1/incentive-routes');

//port number
const port = 3000;

//allow all domain to access this - CORS middleware
app.use(cors());

//body parser middleware
app.use(bodyParser.json());

//route 
// app.use('/api/v1/auth', authRoutes);
// app.use('/api/v1/coupons', couponRoutes);
// app.use('/api/v1/transaction', transactionRoutes);
app.use('/api/v1/users', userRoutes);
app.use('/api/v1/vendors', vendorRoutes);
app.use('/api/v1/incentives', incentiveRoutes);
app.use('/api/v1/common', commonRoutes);

//dummy response
//index route
app.get("/", function(req, res){
    res.send("Invalid Endpoint.");
});

//start server
app.listen(port, function(){
    console.log("Server started on port: " + port);
});
