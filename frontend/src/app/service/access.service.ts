import { Injectable } from '@angular/core';
import { UserAccessConstants } from '../UserAccessConstants';
import { CustomLogger } from '../others/utils/CustomLogger';
import { Globals } from '../others/models/Globals';
import { ServerService } from './server.service';
import { PagePermission } from '../others/models/pagePermission.model';


@Injectable()
export class AccessService {

  pagePermissionsMap: any;
  constructor(private service: ServerService) { }


  // async initializeUserPermissionsFromRoleId(role_uuid: string) {
  //   let data = await this.service.getPermissions2(role_uuid);
  //   CustomLogger.logStringWithObject("got data::: ", data);
  // }


  initializeUserPermissions(pagePermissionsArr: Array<PagePermission>) {
    //initialize the map - IMPORTANT
    this.pagePermissionsMap = new Map();

    for (let k = 0; k < pagePermissionsArr.length; k++) {
      if (pagePermissionsArr[k].is_readable) {
        this.pagePermissionsMap.set(pagePermissionsArr[k].url, [
          pagePermissionsArr[k].is_readable, pagePermissionsArr[k].is_editable, pagePermissionsArr[k].is_deletable
        ]);
      }
    }
    CustomLogger.logStringWithObject("PERMISSIONS FOR USER: ", this.pagePermissionsMap);

  }


  // initializeUserPermissionsFromRoleId(role_uuid: string) {
  //   this.service.getPermissions(role_uuid).subscribe(
  //     data => {
  //       CustomLogger.logStringWithObject("getPermissions: data ", data);


  //       //////////////////////
  //       //initialize the map - IMPORTANT
  //       this.pagePermissionsMap = new Map();

  //       let pagePermissionsArr: Array<PagePermission> = [];
  //       pagePermissionsArr = data["data"];
  //       for(let k=0; k<pagePermissionsArr.length; k++){
  //         if(pagePermissionsArr[k].is_readable){
  //           this.pagePermissionsMap.set(pagePermissionsArr[k].url, [
  //             pagePermissionsArr[k].is_readable, pagePermissionsArr[k].is_editable, pagePermissionsArr[k].is_deletable
  //           ]);
  //         }
  //       }
  //       CustomLogger.logStringWithObject("PERMISSIONS FOR USER: ", this.pagePermissionsMap);
  //     },
  //     err => {
  //       CustomLogger.logStringWithObject("getPermissions: err ", err);
  //     }
  //   );
  // }


  //this method should be called only once during login
  initializeUserPermissions_old(roleType: string, companyCategory: string) {
    CustomLogger.logStringWithObject("INITIALIZING PERMISSIONS FOR USER OF Company Category: ", companyCategory);
    CustomLogger.logStringWithObject("INITIALIZING PERMISSIONS FOR USER OF ROLE TYPE: ", roleType);
    CustomLogger.logStringWithObject("before.... PERMISSIONS FOR USER: ", this.pagePermissionsMap);

    //////////////////////
    //initialize the map - IMPORTANT
    this.pagePermissionsMap = new Map();




    switch (roleType) {
      case UserAccessConstants.COMPANY_ROLE_OWNER:
        CustomLogger.logString("USER IS AN OWNER");
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_DASHBOARD,
          [UserAccessConstants.PERMISSION_ADD, UserAccessConstants.PERMISSION_DELETE,
          UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_VIEW]);
        // this.pagePermissionsMap.set(UserAccessConstants.PAGE_PROFILE,
        //   [UserAccessConstants.PERMISSION_ADD, UserAccessConstants.PERMISSION_DELETE,
        //   UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_VIEW]);
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_PROFILE_COMPANY,
          [UserAccessConstants.PERMISSION_ADD, UserAccessConstants.PERMISSION_DELETE,
          UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_VIEW]);
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_PROFILE_PERSONAL,
          [UserAccessConstants.PERMISSION_ADD, UserAccessConstants.PERMISSION_DELETE,
          UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_VIEW]);
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_INCENTIVES_REWARD,
          [UserAccessConstants.PERMISSION_ADD, UserAccessConstants.PERMISSION_DELETE,
          UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_VIEW]);
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_INCENTIVES_COUPON,
          [UserAccessConstants.PERMISSION_ADD, UserAccessConstants.PERMISSION_DELETE,
          UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_VIEW]);
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_CAMPAIGNS,
          [UserAccessConstants.PERMISSION_ADD, UserAccessConstants.PERMISSION_DELETE,
          UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_VIEW]);
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_NEWCAMPAIGN,
          [UserAccessConstants.PERMISSION_ADD, UserAccessConstants.PERMISSION_DELETE,
          UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_VIEW]);
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_SUPPORT,
          [UserAccessConstants.PERMISSION_ADD, UserAccessConstants.PERMISSION_DELETE,
          UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_VIEW]);
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_BILLING,
          [UserAccessConstants.PERMISSION_ADD, UserAccessConstants.PERMISSION_DELETE,
          UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_VIEW]);
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_CONFIGURATION_ASSIGN_PERMISSION,
          [UserAccessConstants.PERMISSION_ADD, UserAccessConstants.PERMISSION_DELETE,
          UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_VIEW]);
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_SETUP_ORGANIZATION_USERS,
          [UserAccessConstants.PERMISSION_ADD, UserAccessConstants.PERMISSION_DELETE,
          UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_VIEW]);
        //the owner role of company category parent alone should be given permission
        if (companyCategory == UserAccessConstants.COMPANY_CATEGORY_PARENT) {
          this.pagePermissionsMap.set(UserAccessConstants.PAGE_SETUP_COMPANY_CATEGORY,
            [UserAccessConstants.PERMISSION_ADD, UserAccessConstants.PERMISSION_DELETE,
            UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_VIEW]);
        }

        break;
      case UserAccessConstants.COMPANY_ROLE_ADMIN:
        CustomLogger.logString("USER IS AN ADMIN");
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_DASHBOARD,
          [UserAccessConstants.PERMISSION_ADD, UserAccessConstants.PERMISSION_DELETE,
          UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_VIEW]);
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_SETUP_ORGANIZATION_USERS,
          [UserAccessConstants.PERMISSION_ADD, UserAccessConstants.PERMISSION_DELETE,
          UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_VIEW]);
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_PROFILE_PERSONAL,
          [UserAccessConstants.PERMISSION_ADD, UserAccessConstants.PERMISSION_DELETE,
          UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_VIEW]);
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_INCENTIVES_REWARD,
          [UserAccessConstants.PERMISSION_ADD, UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_VIEW]);
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_INCENTIVES_COUPON,
          [UserAccessConstants.PERMISSION_ADD, UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_VIEW]);
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_CAMPAIGNS,
          [UserAccessConstants.PERMISSION_ADD, UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_VIEW]);
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_SUPPORT,
          [UserAccessConstants.PERMISSION_ADD, UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_VIEW]);
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_BILLING,
          [UserAccessConstants.PERMISSION_ADD, UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_VIEW]);
        // this.pagePermissionsMap.set(UserAccessConstants.PAGE_SETUP, [UserAccessConstants.PERMISSION_ADD, UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_VIEW]);
        break;
      case UserAccessConstants.COMPANY_ROLE_ACCOUNTANT:
        CustomLogger.logString("USER IS AN ACCOUNTANT");
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_DASHBOARD, [UserAccessConstants.PERMISSION_ADD, UserAccessConstants.PERMISSION_DELETE,
        UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_VIEW]);
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_PROFILE_PERSONAL,
          [UserAccessConstants.PERMISSION_ADD, UserAccessConstants.PERMISSION_DELETE,
          UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_VIEW]);
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_SUPPORT, [UserAccessConstants.PERMISSION_VIEW, UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_DELETE]);
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_BILLING,
          [UserAccessConstants.PERMISSION_ADD, UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_VIEW]);
        break;
      case UserAccessConstants.COMPANY_ROLE_CAMPAIGN_MANAGER:
        CustomLogger.logString("USER IS A CAMPAIGN MANAGER");
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_DASHBOARD, [UserAccessConstants.PERMISSION_ADD, UserAccessConstants.PERMISSION_DELETE,
        UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_VIEW]);
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_PROFILE_PERSONAL,
          [UserAccessConstants.PERMISSION_ADD, UserAccessConstants.PERMISSION_DELETE,
          UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_VIEW]);
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_PROFILE_PERSONAL, [UserAccessConstants.PERMISSION_ADD, UserAccessConstants.PERMISSION_DELETE,
        UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_VIEW]);
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_INCENTIVES_REWARD, [UserAccessConstants.PERMISSION_VIEW]);
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_INCENTIVES_COUPON, [UserAccessConstants.PERMISSION_VIEW]);
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_CAMPAIGNS, [UserAccessConstants.PERMISSION_VIEW, UserAccessConstants.PERMISSION_EDIT]);
        break;
      case UserAccessConstants.COMPANY_ROLE_MERCHANT:
        CustomLogger.logString("USER IS A MERCHANT");
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_DASHBOARD, [UserAccessConstants.PERMISSION_ADD, UserAccessConstants.PERMISSION_DELETE,
        UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_VIEW]);
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_PROFILE_PERSONAL,
          [UserAccessConstants.PERMISSION_ADD, UserAccessConstants.PERMISSION_DELETE,
          UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_VIEW]);
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_INCENTIVES_REWARD, [UserAccessConstants.PERMISSION_VIEW]);
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_INCENTIVES_COUPON, [UserAccessConstants.PERMISSION_VIEW]);
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_SUPPORT, [UserAccessConstants.PERMISSION_VIEW, UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_DELETE]);
        break;
      case UserAccessConstants.COMPANY_ROLE_STORE_MANAGER:
        CustomLogger.logString("USER IS A STORE MANAGER");
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_PROFILE_PERSONAL,
          [UserAccessConstants.PERMISSION_ADD, UserAccessConstants.PERMISSION_DELETE,
          UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_VIEW]);
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_DASHBOARD, [UserAccessConstants.PERMISSION_ADD, UserAccessConstants.PERMISSION_DELETE,
        UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_VIEW]);
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_PROFILE_PERSONAL, [UserAccessConstants.PERMISSION_ADD, UserAccessConstants.PERMISSION_DELETE,
        UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_VIEW]);
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_INCENTIVES_REWARD, [UserAccessConstants.PERMISSION_VIEW]);
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_INCENTIVES_COUPON, [UserAccessConstants.PERMISSION_VIEW]);
        this.pagePermissionsMap.set(UserAccessConstants.PAGE_SUPPORT, [UserAccessConstants.PERMISSION_VIEW, UserAccessConstants.PERMISSION_EDIT, UserAccessConstants.PERMISSION_DELETE]);
        break;
    }
    CustomLogger.logStringWithObject("PERMISSIONS FOR USER: ", this.pagePermissionsMap);
  }

  getPagePermissionsMap() {
    return this.pagePermissionsMap;
  }

  getCurrentPagePermission(pageName: string) {
    //if pageName is null then send all permissions
    if (pageName == undefined || pageName == null || !this.pagePermissionsMap.has(pageName)) return null;
    return this.pagePermissionsMap.get(pageName);
  }

  resetPermissions() {
    this.pagePermissionsMap = new Map();
  }

  isPageAllowedTo(pagePermissions, permissionType) {
    if (pagePermissions.indexOf(permissionType) > -1) return true;
    return false;
  }
}
