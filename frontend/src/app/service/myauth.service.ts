import { Injectable } from '@angular/core';
import { UserProfile } from '../others/models/userProfile.model';
import { CustomLogger } from '../others/utils/CustomLogger';
import { HttpClient } from '@angular/common/http';
import { CustomGlobalConstants } from '../others/utils/CustomGlobalConstants';
import { Globals } from '../others/models/Globals';
import { AccessService } from './access.service';


@Injectable()
export class MyAuthService {

  constructor(private http: HttpClient, private accessService: AccessService) { }

  
  /////////
  authenticateGoogleUser(userProfile: UserProfile){
    CustomLogger.logString("About to call server for authenticating google user.");
    // let vendorId = this.me.uuid || null;
    return this.http.post(CustomGlobalConstants.HOST_URL + '/users/authenticateGoogleUser', userProfile);    
  }


  loggedIn(){
    return !!localStorage.getItem(CustomGlobalConstants.KEY_TOKEN);
  }

  logout(){
    CustomLogger.logString("LOGGING OUT");
    localStorage.clear();
    Globals.clearAll();
    this.accessService.resetPermissions();
  }
}
