import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CustomLogger } from '../others/utils/CustomLogger';
import { CustomGlobalConstants } from '../others/utils/CustomGlobalConstants';
import { Incentive } from '../others/models/incentive.model';
import { UserProfile } from '../others/models/userProfile.model';
import { Globals } from '../others/models/Globals';
import { ToastrService } from 'ngx-toastr';
import { VendorProfile } from '../others/models/vendorProfile.model';
import { String } from 'aws-sdk/clients/cloud9';
import { PagePermission } from '../others/models/pagePermission.model';
import { LegalDocument } from '../others/models/legalDocument.model';


@Injectable()
export class ServerService {
  HOST_URL = CustomGlobalConstants.HOST_URL;
  USERS_HOST_URL = this.HOST_URL + "/users";
  COMMON_HOST_URL = this.HOST_URL + "/common";

  me: any = Globals.getVendorId();
  constructor(private http: HttpClient, private toastrService: ToastrService) {
  }

  //legal documents
  addLegalDocument(legalDocument: LegalDocument){
    return this.http.post(this.COMMON_HOST_URL + "/addLegalDocument", legalDocument);
  }
  updateLegalDocument(legalDocument: LegalDocument){
    return this.http.post(this.COMMON_HOST_URL + "/updateLegalDocument", legalDocument);
  }
  getAllLegalDocuments(){
    return this.http.get(this.COMMON_HOST_URL + "/getAllLegalDocuments");
  }
  getAllUploadedLegalDocuments(){
    return this.http.get(this.COMMON_HOST_URL + "/getAllUploadedLegalDocuments");
  }
  getLegalDocumentById(uuid){
    return this.http.get(this.COMMON_HOST_URL + "/getLegalDocumentById/" + uuid);
  }
  getAllProcessedLegalDocuments(){
    return this.http.get(this.COMMON_HOST_URL + "/getAllProcessedLegalDocuments");
  }
  

  //authenticate user
  authenticateUser(userProfile: UserProfile) {
    return this.http.post(this.HOST_URL + '/users/authenticateUser/', userProfile);
  }

  //create new user
  createNewUser(userProfile: UserProfile) {
    return this.http.post(this.HOST_URL + '/users/createNewUser/', userProfile);
  }
  updateUser(userProfile: UserProfile) {
    return this.http.post(this.HOST_URL + '/users/updateUser', userProfile);
  }


  //create new vendor
  createNewVendor(vendorProfile: VendorProfile) {
    return this.http.post(this.HOST_URL + '/vendors/createNewVendor/', vendorProfile);
  }

  /**
   * Get all incentives of the particular vendor
   * @param filter 
   * @param INCENTIVE_TYPE 
   */
  getAllIncentives(vendorId, INCENTIVE_TYPE) {
    CustomLogger.logString("SServer: getIncentives: vendorID: " + vendorId);
    let url = this.HOST_URL + '/incentives/getAllIncentives/' + vendorId + "/" + INCENTIVE_TYPE;
    CustomLogger.logString("URL to access: " + url);
    return this.http.get(url);
  }

  /**
   * Get all incentives of the particular vendor
   * @param filter 
   * @param INCENTIVE_TYPE 
   * /
  getAllIncentives_old(INCENTIVE_TYPE) {
    let vendorID = this.me.uuid;
    CustomLogger.logString("server: getIncentives: vendorID: " + vendorID);
    let url = this.HOST_URL + '/vendors/allIncentives/' + vendorID + "/" + INCENTIVE_TYPE;
    CustomLogger.logString("URL to access: " + url);
    return this.http.get(url);
  }
 

  /**
   * 
   * @param incentiveType - reward/coupon/...
   * @param searchNameString - search name string
   * @param searchType - created/expired/....
   */
  getSpecifcIncentives(vendorID, incentiveType, searchNameString: string, primarySearchType: string, searchType: string, searchStartTimeNumber: number, searchEndTimeNumber: number) {
    // let vendorID = this.me.uuid;
    CustomLogger.logString("server: getIncentives: vendorID: " + vendorID);
    if (!searchNameString || searchNameString.trim() === "") searchNameString = "NULL";
    if (!primarySearchType) primarySearchType = "ALL";
    if (!searchType) searchType = "ALL";

    let urlStr = this.HOST_URL + '/incentives/getSpecificSearchIncentives/' + incentiveType + "/" + vendorID + '/' + searchNameString + '/' + primarySearchType + '/' + searchType + '/' + searchStartTimeNumber +
      '/' + searchEndTimeNumber;
    CustomLogger.logString("urlStr to send: " + urlStr);
    return this.http.get(urlStr);
  }

  /**
     * 
     * @param incentiveType - reward/coupon/...
     * @param searchNameString - search name string
     * @param searchType - created/expired/....
     */
  // getSpecifcIncentives_old(incentiveType, searchNameString: string, primarySearchType: string, searchType: string, searchStartTimeNumber: number, searchEndTimeNumber: number) {
  //   let vendorID = this.me.uuid;
  //   CustomLogger.logString("server: getIncentives: vendorID: " + vendorID);
  //   if (!searchNameString || searchNameString.trim() === "") searchNameString = "NULL";
  //   if (!primarySearchType) primarySearchType = "ALL";
  //   if (!searchType) searchType = "ALL";

  //   let urlStr = this.HOST_URL + '/vendors/incentives/' + incentiveType + "/" + vendorID + '/' + searchNameString + '/' + primarySearchType + '/' + searchType + '/' + searchStartTimeNumber +
  //     '/' + searchEndTimeNumber;
  //   CustomLogger.logString("urlStr to send: " + urlStr);
  //   return this.http.get(urlStr);
  // }

  fetchIncentive(id) {
    return this.http.get(this.HOST_URL + '/incentives/getSingleIncentive/' + id);
  }

  // fetchIncentive_old(id) {
  //   return this.http.get(this.HOST_URL + '/coupons/' + id + '/details');
  // }

  fetchCustomerDetails(uuid) {
    return this.http.get(this.HOST_URL + '/vendors/customer/' + uuid);
  }

  // fetchMyDetails() {
  //   CustomLogger.logString("server: fetchMyDetails");
  //   return new Promise(function (resolve, reject) {
  //     if (this.me.uuid) resolve(this.me);
  //     CustomLogger.logString("server: host-url:" + this.HOST_URL);
  //     this.http.get(this.HOST_URL + '/users/me').subscribe(
  //       data => {
  //         this.me = data['data'];
  //         CustomLogger.logString("server: fetchMyDetails this.me:");
  //         CustomLogger.logObj(this.me);
  //         if (!this.me) {
  //           this.me = {
  //             uuid: 'dummy'
  //           }
  //         }
  //         resolve(this.me);
  //       },
  //       error => {
  //         CustomLogger.logObj(error);
  //         reject(error);
  //       }
  //     );
  //   }.bind(this));

  // }

  registerNewCustomer(body) {
    return this.http.post(this.HOST_URL + '/vendors/' + this.me.uuid + '/shop/registers', body);
  }

  fetchAllRegisteredCustomers() {
    return this.http.get(this.HOST_URL + '/vendors/' + this.me.uuid + '/registers/customers');
  }

  uploadImage(body) {
    return this.http.post(this.HOST_URL + '/common/images', body);
  }

  fetchImage(belongs_to, type) {
    return this.http.get(this.HOST_URL + '/common/images/' + belongs_to + '/images?type=' + type);
  }

  createBatchIncentive(body) {
    return this.http.post(this.HOST_URL + '/incentives/createIncentivesInBatch', body);
  }

  createIncentive(incentive: Incentive) {
    // CustomLogger.logString("createIncentive: this.me.uuid:::: " + this.me.uuid);
    // body.vendor_uuid = this.me.uuid;
    // return this.http.post(this.HOST_URL + '/coupons', incentive);
    return this.http.post(this.HOST_URL + '/incentives/addIncentive', incentive);
  }

  deleteIncentive(body) {
    body.vendor_uuid = this.me.uuid || null
    return this.http.post(this.HOST_URL + '/incentives/delete', body);
  }

  updateIncentive(body) {
    return this.http.post(this.HOST_URL + '/incentives/updateIncentive', body);
  }

  // updateIncentive_old(body) {
  //   //body.vendor_uuid = this.me.uuid
  //   CustomLogger.logString("updateIncentive: meeeee:" + this.me.uuid);
  //   return this.http.post(this.HOST_URL + '/coupons/update', body);
  // }

  // updateCustomer(body) {
  //   //body.vendor_uuid = this.me.uuid
  //   return this.http.post(this.HOST_URL + '/coupons', body);
  // }

  updateProfile(body) {
    return this.http.post(this.HOST_URL + '/vendors/updateProfile', body);
  }

  // fetchProfile() {
  //   return this.http.get(this.HOST_URL + '/vendors/' + this.me.uuid + '/profile');
  // }

  // getIncentiveTempId() {
  //   return new Promise(function (resolve, reject) {
  //     this.http.get(this.HOST_URL + '/coupons/temp/tempId').subscribe(
  //       data => {
  //         resolve(data.data.uuid);
  //       },
  //       error => {
  //         reject(error);
  //       }
  //     );
  //   }.bind(this));
  // }

  // createCouponsInBatch(body) {
  //   body.createdBy = this.me.uuid
  //   console.log('body from server service:' + JSON.stringify(body));
  //   return this.http.post(this.HOST_URL + '/coupons/createCouponsInBatch', body);
  // }

  // getAllBatchCoupons(filter) {
  //   //let vendorID = this.me.uuid;
  //   return this.http.get(this.HOST_URL + '/coupons/getAllBatchCoupons');
  // }

  //get all rewards specific details 
  getAllIncentiveCountsFromDB(vendorID, incentiveType, searchStartTime, searchEndTime): Observable<any> {
    // CustomLogger.logString("vendor:" + vendorID);
    // return this.http.get(this.HOST_URL + '/coupons/getAllIncentivesSpecificDetails/' + vendorID + "/" + incentiveType + '/' + searchStartTime + "/" + searchEndTime);
    let url = this.HOST_URL + '/incentives/getAllIncentiveCounts/' + vendorID + "/" +
      incentiveType + "/" + searchStartTime + "/" + searchEndTime;
    CustomLogger.logString("getAllIncentiveCountsFromDB: URL:" + url);
    return this.http.get(url);
  }


  // //get all rewards specific details
  // getAllRewardsSpecificDetailsFromDB(vendorID, searchStartTime, searchEndTime): Observable<any> {
  //   CustomLogger.logString("getAllRewardsSpecificDetailsFromDB for vendor:" + vendorID);
  //   return this.http.get(this.HOST_URL + '/coupons/' + vendorID + "/" + searchStartTime + "/" + searchEndTime + '/getAllRewardsSpecificDetails');    
  // }

  //get suggested rewards
  // getSuggestedRewardsFromDB(): Observable<any> {
  //   // let vendorID = this.me.uuid || null;
  //   return this.http.get(this.HOST_URL + '/coupons/' + vendorID + '/getSuggestedRewards');
  // }

  createIncentivesFromUpload(incentiveArr): Observable<any> {
    //body.createdBy = this.me.uuid
    // console.log('body from server service:' + JSON.stringify(incentiveArr));
    return this.http.post(this.HOST_URL + '/incentives/createIncentivesFromUpload', incentiveArr);
  }


  // createIncentivesFromUpload(incentiveArr): Observable<any> {
  //   //body.createdBy = this.me.uuid
  //   console.log('body from server service:' + JSON.stringify(incentiveArr));
  //   return this.http.post(this.HOST_URL + '/coupons/createIncentivesFromUpload', incentiveArr);
  // }

  // getCurrentVendorUUID() {
  //   return this.me.uuid;
  // }

  /**
   * 
   * @param incentiveUuidsArr 
   */
  deactivateBatchIncentives(vendorId, incentiveUuidsArr): Observable<any> {
    CustomLogger.logString("About to call server for deactivating many incentives.");
    return this.http.post(this.HOST_URL + '/vendors/deactivateBatchIncentives/' + vendorId, incentiveUuidsArr);
  }


  getUserProfile(userId) {
    return this.http.get(this.HOST_URL + '/users/profile/' + userId);
  }

  getVendorDetails(vendorId) {
    let url = this.HOST_URL + '/vendors/getVendorProfile/' + vendorId;
    CustomLogger.logString("Url for fetch vendor profile: " + url);
    return this.http.get(url);
  }

  userUpdateCompletedProfile(userId, updateFlag) {
    return this.http.post(this.HOST_URL + '/users/updateCompletedProfile/' + userId, { "updateFlag": updateFlag });
  }

  // fetchVendorDetails_orig() {
  //   CustomLogger.logString("server: fetchVendorDetails");
  //   return new Promise(function (resolve, reject) {
  //     if (this.me.uuid) resolve(this.me);
  //     CustomLogger.logString("server: host-url:" + this.HOST_URL);
  //     this.http.get(this.HOST_URL + '/users/me').subscribe(
  //       data => {
  //         this.me = data['data'];
  //         CustomLogger.logString("server: fetchVendorDetails this.me:");
  //         CustomLogger.logObj(this.me);
  //         if (!this.me) {
  //           this.me = {
  //             uuid: 'dummy'
  //           }
  //         }
  //         resolve(this.me);
  //       },
  //       error => {
  //         CustomLogger.logObj(error);
  //         reject(error);
  //       }
  //     );
  //   }.bind(this));

  // }


  getVendorGlobals(token) {
    let url = this.HOST_URL + '/users/getGlobalData/' + token;
    CustomLogger.logString("getvendorglobals url: " + url);
    return this.http.get(url);
  }

  ////////////////alerts
  showMessage(message) {
    console.log("inside  showSuccessMessage ");
    this.toastrService.success(message, "some title");
  }


  ////////////////////////////////user permissions
  //get all company categories
  getCompanyCategories(user_uuid: string) {
    return this.http.get(this.USERS_HOST_URL + "/getCompanyCategories/" + user_uuid);
  }

  //get all company roles for category
  getCompanyRoles(company_category_uuid: string) {
    return this.http.get(this.USERS_HOST_URL + "/getCompanyRoles/" + company_category_uuid);
  }

  //get all permissions for role
  getPermissions(company_role_uuid: String) {
    return this.http.get(this.USERS_HOST_URL + "/getPermissions/" + company_role_uuid);
  }

  //get all permissions for role
  getPermissions2(company_role_uuid: String) {
    return this.http.get(this.USERS_HOST_URL + "/getPermissions/" + company_role_uuid).toPromise();
  }

  //update page permissions
  updatePermissionsArray(pagePermissionsArr:Array<PagePermission>){
    return this.http.post(this.USERS_HOST_URL + "/updatePermissionsArray", pagePermissionsArr);
  }

}//end of class
