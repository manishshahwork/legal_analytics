import { Injectable } from '@angular/core';
import { LegalDocument } from '../others/models/legalDocument.model';
import { HttpClient } from '@angular/common/http';
import jsPDF from 'jspdf';
import { saveAs } from 'file-saver';
import { CustomLogger } from '../others/utils/CustomLogger';
var FileSaver = require('file-saver');

@Injectable({
  providedIn: 'root'
})
export class PythonService {

  // HOST_URL = "http://ec2-13-233-138-244.ap-south-1.compute.amazonaws.com:3002/api/summary";
  HOST_URL = "http://ec2-13-234-78-184.ap-south-1.compute.amazonaws.com:3002/api/summary";
  constructor(private http: HttpClient) { }

  processDocument(fileUrl, totalLinesStr) {
    let totalLines = Number(totalLinesStr);
    CustomLogger.logString(" about to call python api for fileUrl:" + fileUrl + " lines:" + totalLines + " and http:" + this.HOST_URL);
    return this.http.post(this.HOST_URL, { "file_url": fileUrl, "total_lines": totalLines });
  }

  // getFileContent(fileName: string) {
  //   fileName = '1.txt';
  //   this.http.get(fileName).subscribe(data => {
  //     console.log(data);
  //   })
  // }

  getEntireTextFromDataArray(stringArr:Array<any>){
    let str = "";
    for(let k=0; k<stringArr.length; k++){
      str += stringArr[k] + " ";
    }
    return str;
  }

  generatePDFFromText(text: string) {
    var doc = new jsPDF();
    doc.text(text, 10, 10);
    doc.save("test1.pdf");

    // var pdfOutput = doc.output();
    // console.log("pdfOutput::", pdfOutput);

    // var blob = new Blob([text]);
    // FileSaver.saveAs(blob, "./test1.pdf");

    // doc.save('a4.pdf');

  }

}
