import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyprofileComponent } from '../views/profile/myprofile-component';
import { FormsModule } from '@angular/forms';
import { BusinessprofileComponent } from '../views/profile/businessprofile-component';
import { CompanyCategoryComponent } from '../views/setup/companyCategory-component';
import { OrganizationUsersComponent } from '../views/setup/organizationUsers-component';
import { AssignPermissionComponent } from '../views/configuration/assignPermission.component';
import { DataTableModule } from 'angular-6-datatable';
import { CrudCompanyCategoryComponent } from '../views/configuration/companyCategory/crudCompanyCategory.component';
import { CompanyCategoryListComponent } from '../views/configuration/companyCategory/companyCategoryList.component';
import { NotificationComponent } from '../views/notification/notification.component';
import { ConnectedDevicesComponent } from '../views/connectedDevices/connectedDevices.component';
import { IncidentComponent } from '../views/incident/incident.component';
import { InvestigationReportComponent } from '../views/investigationReport/investigationReport.component';
import { SettingsComponent } from '../views/settings/settings.component';
import { ReportsComponent } from '../views/reports/reports.component';
import { UploadDocumentComponent } from '../views/legalDocument/uploadDocument.component';
import { ShowDocumentListComponent } from '../views/legalDocument/showDocumentList.component';
import { ShowSingleDocumentComponent } from '../views/legalDocument/showSingleDocument.component';
import { RouterModule } from '@angular/router';
import { ProcessedDocumentListComponent } from '../views/legalDocument/processedDocumentList.component';
import { AddLegalDocumentComponent } from '../views/legalDocument/addLegalDocument.component';
import { ProcessDocumentComponent } from '../views/legalDocument/processDocument.component';

@NgModule({
  declarations: [
    MyprofileComponent,
    BusinessprofileComponent,
    CompanyCategoryComponent,
    OrganizationUsersComponent,
    AssignPermissionComponent,
    CrudCompanyCategoryComponent,
    CompanyCategoryListComponent,
    NotificationComponent,
    ConnectedDevicesComponent,
    IncidentComponent,
    InvestigationReportComponent,
    SettingsComponent,
    ReportsComponent,
    UploadDocumentComponent,
    ShowDocumentListComponent,
    ProcessedDocumentListComponent,
    AddLegalDocumentComponent,
    ProcessDocumentComponent

  ],
  imports: [
    CommonModule,
    FormsModule,
    DataTableModule,
    RouterModule
  ], 
  exports : [
    FormsModule
  ]
})
export class SharedModule { } 
