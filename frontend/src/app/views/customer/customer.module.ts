import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule} from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CustomerRoutingModule } from './customer-routing.module';
import { CustomerComponent } from './customer.component';


@NgModule({
  imports: [
    CommonModule, 
    CustomerRoutingModule, 
    FormsModule
  ],
  declarations: [ CustomerComponent ]
})
export class CustomerModule { } 
