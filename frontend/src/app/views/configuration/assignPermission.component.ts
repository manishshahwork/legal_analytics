import { Component, OnInit } from '@angular/core';
import { ServerService } from '../../service/server.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UserProfile } from '../../others/models/userProfile.model';
import { Globals } from '../../others/models/Globals';
import { CustomLogger } from '../../others/utils/CustomLogger';
import { UserAccessConstants } from '../../UserAccessConstants';
import { CustomBaseComponent } from '../../others/models/CustomBaseComponent';
import { PagePermission } from '../../others/models/pagePermission.model';
import { CompanyCategoryComponent } from '../setup/companyCategory-component';
import { CompanyRole } from '../../others/models/companyRole.model';
import { CompanyCategory } from '../../others/models/companyCategory';

@Component({
  templateUrl: 'assignPermission.component.html'
})
export class AssignPermissionComponent extends CustomBaseComponent implements OnInit {

  tableDataArr: any;
  htmlSpace = " ";
  selectedCompanyCategory = "";

  companyCategoriesArr: Array<CompanyCategory> = [];
  companyRolesArr: Array<CompanyRole> = [];

  constructor(private service: ServerService, private activatedRoute: ActivatedRoute, private router: Router) {
    super();
  }


  ngOnInit() {
    this.fillCompanyCategories(Globals.getUserId());
    // this.getTestTableDataArr();
  }

  fillCompanyCategories(user_uuid) {
    //get all company categories
    this.service.getCompanyCategories(user_uuid).subscribe(
      data => {
        CustomLogger.logStringWithObject("getCompanyCategories: data: ", data);
        this.companyCategoriesArr = data["data"];
        this.fillCompanyRoles[this.companyCategoriesArr[0].uuid];//1st element
      },
      err => {
        CustomLogger.logStringWithObject("getCompanyCategories: error: ", err);
      }
    );
  }

  fillCompanyRoles(company_category_uuid) {
    CustomLogger.logStringWithObject(" fillCompanyRoles:: ", company_category_uuid);

    //get all company roles
    this.service.getCompanyRoles(company_category_uuid).subscribe(
      data => {
        CustomLogger.logStringWithObject("getCompanyRoles: data: ", data);
        this.companyRolesArr = data["data"];
        this.fillPermissions(this.companyRolesArr[0].uuid);//1st element
      },
      err => {
        CustomLogger.logStringWithObject("getCompanyRoles: error: ", err);
      }
    );
  }

  fillPermissions(company_role_uuid) {
    CustomLogger.logStringWithObject(" fillPermissions:: ", company_role_uuid);

    //get all permissions for pages
    this.service.getPermissions(company_role_uuid).subscribe(
      data => {
        CustomLogger.logStringWithObject("getPermissions: data: ", data);
        this.tableDataArr = data["data"];
      },
      err => {
        CustomLogger.logStringWithObject("getPermissions: error: ", err);
      }
    );
  }


  getTestTableDataArr() {
    this.tableDataArr = [];

    //category 1
    let category1 = new CompanyCategory();
    category1.uuid = "1";
    category1.name = UserAccessConstants.COMPANY_CATEGORY_PARENT;

    let role1 = new CompanyRole();
    role1.uuid = "1";
    role1.company_category_uuid = category1.uuid;
    role1.name = UserAccessConstants.COMPANY_ROLE_OWNER;

    let pagePermission1 = new PagePermission();
    pagePermission1.uuid = "1";
    pagePermission1.is_deletable = true;
    pagePermission1.is_editable = true;
    pagePermission1.is_readable = false;
    pagePermission1.name = "Dashboard";
    pagePermission1.url = UserAccessConstants.PAGE_DASHBOARD;
    pagePermission1.company_role_uuid = role1.uuid;
    //get role name from role_uuid
    this.tableDataArr.push(pagePermission1);
    let pagePermission2 = new PagePermission();
    pagePermission2 = new PagePermission();
    pagePermission2.uuid = "2";
    pagePermission2.is_deletable = false;
    pagePermission2.is_editable = true;
    pagePermission2.is_readable = true;
    pagePermission2.name = "Rewards";
    pagePermission2.url = UserAccessConstants.PAGE_INCENTIVES_REWARD;
    pagePermission2.company_role_uuid = role1.uuid;
    this.tableDataArr.push(pagePermission2);

    let role2 = new CompanyRole();
    role2.uuid = "2";
    role2.company_category_uuid = category1.uuid;
    role2.name = UserAccessConstants.COMPANY_ROLE_OWNER;

    let pagePermission3 = new PagePermission();
    pagePermission3.uuid = "1";
    pagePermission3.is_deletable = true;
    pagePermission3.is_editable = true;
    pagePermission3.is_readable = false;
    pagePermission3.name = "Dashboard";
    pagePermission3.url = UserAccessConstants.PAGE_DASHBOARD;
    pagePermission3.company_role_uuid = role2.uuid;
    //get role name from role_uuid
    this.tableDataArr.push(pagePermission3);
    let pagePermission4 = new PagePermission();
    pagePermission4 = new PagePermission();
    pagePermission4.uuid = "2";
    pagePermission4.is_deletable = false;
    pagePermission4.is_editable = true;
    pagePermission4.is_readable = true;
    pagePermission4.name = "Rewards";
    pagePermission4.url = UserAccessConstants.PAGE_INCENTIVES_REWARD;
    pagePermission4.company_role_uuid = role2.uuid;
    this.tableDataArr.push(pagePermission4);

    CustomLogger.logStringWithObject("tabledata: ", this.tableDataArr);

  }

  onUpdatePermissions() {
    CustomLogger.logStringWithObject("onUpdatePermissions: table data:: ", this.tableDataArr);
    this.service.updatePermissionsArray(this.tableDataArr).subscribe(
      data => {
        CustomLogger.logStringWithObject("updatePermissionsArray: error: ", data);
       },
      err => {
        CustomLogger.logStringWithObject("updatePermissionsArray: error: ", err);
       }
    );
  }
}
