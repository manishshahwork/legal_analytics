import { Component, OnInit } from '@angular/core';
import { CustomBaseComponent } from '../../../others/models/CustomBaseComponent';
import { ServerService } from '../../../service/server.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Globals } from '../../../others/models/Globals';
import { CustomLogger } from '../../../others/utils/CustomLogger';
import { CompanyCategory } from '../../../others/models/companyCategory';


@Component({
  templateUrl: 'crudCompanyCategory.component.html'
})
export class CrudCompanyCategoryComponent extends CustomBaseComponent implements OnInit {

  tableDataArr : any;
  constructor(private service: ServerService, private activatedRoute: ActivatedRoute, private router: Router) {
    super();
  }


  ngOnInit() {
    this.getCompanyCategories(Globals.getUserId());
  }

  getCompanyCategories(user_uuid) {
    //get all company categories
    this.service.getCompanyCategories(user_uuid).subscribe(
      data => {
        CustomLogger.logStringWithObject("getCompanyCategories: data: ", data);
        this.tableDataArr = data["data"];
      },
      err => {
        CustomLogger.logStringWithObject("getCompanyCategories: error: ", err);
      }
    );
  }

  onClickEdit(companyCategory:CompanyCategory){
    CustomLogger.logStringWithObject("about to edit... : ", companyCategory);
  }

}//endof class
