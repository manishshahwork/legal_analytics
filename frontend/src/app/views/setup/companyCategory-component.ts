import { Component, OnInit } from '@angular/core';
import { VendorProfile } from '../../others/models/vendorProfile.model';
import { CustomLogger } from '../../others/utils/CustomLogger';
import { Globals } from '../../others/models/Globals';
import { CustomGlobalConstants } from '../../others/utils/CustomGlobalConstants';
import { ServerService } from '../../service/server.service';
import { Router } from '@angular/router';
import { CustomMisc } from '../../others/utils/CustomMisc';
import { UserProfile } from '../../others/models/userProfile.model';
import { UserAccessConstants } from '../../UserAccessConstants';

@Component({
  templateUrl: 'companyCategory-component.html'
})
export class CompanyCategoryComponent implements OnInit {
  newVendorProfile: VendorProfile;
  newUserProfile: UserProfile;
  
  showCompanyCategoryTypesFlag = false;
  htmlRoleTypesArr = UserAccessConstants.ROLE_TYPES_FOR_PARENT;
  htmlCompanyCategoryTypesArr = UserAccessConstants.COMPANY_CATEGORY_TYPES_FOR_PARENT;

  //image specifics
  htmlProfileImageLink: string = CustomGlobalConstants.DEFAULT_PROFILE_IMAGE_FILE_LINK;
  currentProfileImageFile: File;
  htmlCoverImageLink: string = CustomGlobalConstants.DEFAULT_PROFILE_IMAGE_FILE_LINK;
  currentCoverImageFile: File;
  htmlLogoImageLink: string = CustomGlobalConstants.DEFAULT_PROFILE_IMAGE_FILE_LINK;
  currentLogoImageFile: File;
  uploadProfileImageFlag: boolean = false;
  uploadCoverImageFlag: boolean = false;
  uploadLogoImageFlag: boolean = false;

  constructor(private server: ServerService, private router: Router) {
    
  }

  ngOnInit() {
    this.newUserProfile = new UserProfile();
    this.newUserProfile.role_uuid = UserAccessConstants.COMPANY_ROLE_OWNER;
    
    this.newVendorProfile = new VendorProfile();
    this.newVendorProfile.company_category_uuid = UserAccessConstants.COMPANY_CATEGORY_FRANCHISE;    
    this.newVendorProfile.parent_uuid = Globals.getVendorId();

    CustomLogger.logStringWithObject("initial newVendorProfile", this.newVendorProfile);
    if(Globals.getCompanyCategoryId() == UserAccessConstants.COMPANY_CATEGORY_PARENT)
      this.showCompanyCategoryTypesFlag = true;
  }

  /**
   * 
   * @param event 
   * @param imageNo  1=Profile, 2=Cover, 3=Logo
   */
  async onImageUpload(event, imageNo) {
    let imageFile = event.target.files[0];
    CustomLogger.logStringWithObject("will upload for imageNo:" + imageNo + " the file::: ", imageFile);

    //preview the image
    var reader = new FileReader();
    reader.readAsDataURL(imageFile); // read file as data url
    reader.onload = (event: any) => { // called once readAsDataURL is completed
      switch (imageNo) {
        case 1: this.currentProfileImageFile = imageFile;
          this.htmlProfileImageLink = event.target.result;
          this.uploadProfileImageFlag = true;
          break;
        case 2: this.currentCoverImageFile = imageFile;
          this.htmlCoverImageLink = event.target.result;
          this.uploadCoverImageFlag = true;
          break;
        case 3: this.currentLogoImageFile = imageFile;
          this.htmlLogoImageLink = event.target.result;
          this.uploadLogoImageFlag = true;
          break;
      }
    }
  }

  async onSubmit() {
    CustomLogger.logStringWithObject("vendor profile: ", this.newVendorProfile);

    //upload image files to server
    let tmpVendorProfile = this.newVendorProfile;

    if (this.uploadProfileImageFlag) {
      //temporary object to hold values
      await CustomMisc.uploadFileToRemoteServer(this.currentProfileImageFile).then(function (dataLocation) {
        tmpVendorProfile.profile_link = dataLocation.toString();
        CustomLogger.logString("datalocstr:" + dataLocation.toString());
      });
    }

    if (this.uploadCoverImageFlag) {
      await CustomMisc.uploadFileToRemoteServer(this.currentCoverImageFile).then(function (dataLocation) {
        tmpVendorProfile.cover_link = dataLocation.toString();
        CustomLogger.logString("datalocstr:" + dataLocation.toString());
      });
    }

    if (this.uploadLogoImageFlag) {
      await CustomMisc.uploadFileToRemoteServer(this.currentLogoImageFile).then(function (dataLocation) {
        tmpVendorProfile.logo_link = dataLocation.toString();
        CustomLogger.logString("datalocstr:" + dataLocation.toString());
      });
    }

    //convert back to original object
    this.newVendorProfile = tmpVendorProfile;

    //fill up important fields
    this.newVendorProfile.parent_uuid = Globals.getVendorId();

    //create new vendor
    CustomLogger.logStringWithObject("about to save vendor profile: ", this.newVendorProfile);
    this.server.createNewVendor(this.newVendorProfile).subscribe(
      data => {
        CustomLogger.logStringWithObject("createNewVendor", data);
        let tmpVendorProfile = this.newVendorProfile;
        tmpVendorProfile = data["data"];
        ///////////////////////
        //create new user
        this.newUserProfile.vendor_uuid = tmpVendorProfile.uuid;
        CustomLogger.logStringWithObject("New user will be added.", this.newUserProfile);
        this.server.createNewUser(this.newUserProfile).subscribe(
          data1 => { 
            CustomLogger.logStringWithObject("createNewUser Success", data1);
            this.router.navigate([UserAccessConstants.PAGE_DASHBOARD]);
          },
          err1 => { 
            CustomLogger.logStringWithObject("createNewUser Error: " , err1);
          }
        );
        alert('New Vendor Profile created');
        // this._router.navigate([CustomGlobalConstants.ROUTE_DASHBOARD]);
      },
      err => {
        CustomLogger.logStringWithObject("createNewVendor Error: " , err);
        alert('New Vendor Profile failed');
        // this._router.navigate([CustomGlobalConstants.ROUTE_DASHBOARD]);
      }
    );
  }
}