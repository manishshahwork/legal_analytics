import { Component, OnInit } from '@angular/core';
import { UserProfile } from '../../others/models/userProfile.model';
import { UserAccessConstants } from '../../UserAccessConstants';
import { CustomLogger } from '../../others/utils/CustomLogger';
import { useAnimation } from '@angular/animations';
import { Globals } from '../../others/models/Globals';
import { ServerService } from '../../service/server.service';
import { Router } from '@angular/router';
import { CompanyRole } from '../../others/models/companyRole.model';

@Component({
  templateUrl: 'organizationUsers-component.html'
})
export class OrganizationUsersComponent implements OnInit {

  userProfile: UserProfile;
  // htmlCompanyCategoryTypesArr = [];
  htmlRoleTypesArr:Array<CompanyRole> = [];
  constructor(private service: ServerService, private router: Router) {

  }

  ngOnInit() {
    this.userProfile = new UserProfile();
    this.fillUpDefaults();
  }

  fillUpDefaults() {
    // this.htmlCompanyCategoryTypesArr = UserAccessConstants.COMPANY_CATEGORY_TYPES;
    // this.htmlRoleTypesArr = UserAccessConstants.COMPANY_ROLE_TYPES;
    // this.userProfile.role_uuid = UserAccessConstants.COMPANY_ROLE_TYPES[0];

    this.userProfile.vendor_uuid = Globals.getVendorId();
    this.userProfile.has_completed_profile = true;
    this.service.getCompanyRoles(Globals.getCompanyCategoryId()).subscribe(
      data => { 
        CustomLogger.logStringWithObject("getCompanyRoles data: ", data);
        this.htmlRoleTypesArr = data["data"];
      },
      err => { 
        CustomLogger.logStringWithObject("getCompanyRoles error from db: ", err);
      }
    );
  }

  onSubmit() {
    CustomLogger.logStringWithObject("userProfile: ", this.userProfile);
    this.service.createNewUser(this.userProfile).subscribe(
      data => {
        CustomLogger.logStringWithObject("result from db: ", data);
        this.router.navigate([UserAccessConstants.PAGE_DASHBOARD]);
      },
      err => {
        CustomLogger.logStringWithObject("error from db: ", err);
      }
    );
  }
}
