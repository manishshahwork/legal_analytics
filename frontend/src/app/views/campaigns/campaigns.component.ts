import { Component, OnInit } from '@angular/core';
import { AccessService } from '../../service/access.service';
import { CustomLogger } from '../../others/utils/CustomLogger';
import { Globals } from '../../others/models/Globals';
import { UserAccessConstants } from '../../UserAccessConstants';

@Component({
  templateUrl: 'campaigns.component.html',
  styleUrls: ['campaigns.component.css']
})
export class CampaignsComponent implements OnInit {

  sampleText: any;
  constructor(private accessService: AccessService) { }

  ngOnInit() {
    let pagePermissions = Globals.getCurrentPagePermission();
    this.sampleText = "This page can be: ";
    this.sampleText += this.accessService.isPageAllowedTo(pagePermissions, UserAccessConstants.PERMISSION_ADD) ? " #--added--#" : "";
    this.sampleText += this.accessService.isPageAllowedTo(pagePermissions, UserAccessConstants.PERMISSION_DELETE) ? " #--delete--#" : "";
    this.sampleText += this.accessService.isPageAllowedTo(pagePermissions, UserAccessConstants.PERMISSION_EDIT) ? " #--edit--#" : "";
    this.sampleText += this.accessService.isPageAllowedTo(pagePermissions, UserAccessConstants.PERMISSION_VIEW) ? " #--view--#" : "";
  }
}
