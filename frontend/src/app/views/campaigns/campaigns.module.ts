import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule} from 'ngx-bootstrap';
import { CampaignsRoutingModule } from './campaigns-routing.module';
import { CampaignsComponent } from './campaigns.component';
import { CommonModule } from '@angular/common';
import { BsDatepickerModule } from 'ngx-bootstrap';


@NgModule({
  imports: [
    CampaignsRoutingModule,
    ChartsModule,
    BsDropdownModule,
    CommonModule,
    BsDatepickerModule.forRoot()
  ],
  declarations: [ CampaignsComponent,  ]
})
export class CampaignsModule { } 
