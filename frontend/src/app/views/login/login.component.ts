import { Component, OnInit } from '@angular/core';
import { AuthService, FacebookLoginProvider, GoogleLoginProvider } from 'angular-6-social-login';
import { Router } from '@angular/router';
import { MyAuthService } from '../../service/myauth.service';
import { CustomLogger } from '../../others/utils/CustomLogger';
import { UserProfile } from '../../others/models/userProfile.model';
import { CustomGlobalConstants } from '../../others/utils/CustomGlobalConstants';
import { Globals } from '../../others/models/Globals';
import { ServerService } from '../../service/server.service';
import { CustomMisc } from '../../others/utils/CustomMisc';
import { AccessService } from '../../service/access.service';
import { UserAccessConstants } from '../../UserAccessConstants';
import { error } from '@angular/compiler/src/util';
import { CompanyRole } from '../../others/models/companyRole.model';
import { VendorProfile } from '../../others/models/vendorProfile.model';
import { PythonService } from '../../service/python.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: 'login.component.html'
})
export class LoginComponent implements OnInit {

  picklistCompanyCategoryTypesArr = UserAccessConstants.COMPANY_CATEGORY_TYPES;
  companyRoleTypesArr: Array<CompanyRole> = [];
  userProfile: UserProfile;

  constructor(
    private socialAuthService: AuthService,
    private router: Router,
    private _myauth: MyAuthService,
    private _service: ServerService,
    private _accessService: AccessService,
    private _pythonService: PythonService
  ) {
    CustomLogger.logString("Login Component check valid user (before): " + Globals.isUserValid());
    Globals.setUserValid(false);
    CustomLogger.logString("Login Component check valid user (after): " + Globals.isUserValid());
  }

  ngOnInit() {

    // let strArr = ["my name is", "who cares"];
    // let k = this._pythonService.convertStringArrayToString(strArr);
    // console.log("K: " + k);
    // console.log("to: " + strArr.toString());

    // this._pythonService.generatePDFFromText("111111111 sad ");
    this.userProfile = new UserProfile();
    // this.userProfile.role_uuid = "1";//1 - is the owner of the company
    //fill up company role types
    this._service.getCompanyRoles(UserAccessConstants.COMPANY_CATEGORY_PARENT_UUID).subscribe(
      data => {
        CustomLogger.logStringWithObject("getCompanyRoles: data:", data);
        this.companyRoleTypesArr = data["data"];
      },
      err => {
        CustomLogger.logStringWithObject("getCompanyRoles: err:", err);
      }
    );
  }

  onClickLogin() {
    CustomLogger.logStringWithObject("Login:", this.userProfile);

    //1st if login is admin then create it in database if not existing for company "Parent"
    this._service.authenticateUser(this.userProfile).subscribe(
      data => {
        CustomLogger.logStringWithObject("authenticateUser data: ", data);

        if (data["data"] != null) {
          this.userProfile = data["data"];
          // CustomLogger.logStringWithObject("Authentication data: ", data);
          CustomLogger.logString("MESSAGE: " + data['message']);
          let token = data['message'];

          //STORE THE SECRET TOKEN
          localStorage.setItem(CustomGlobalConstants.KEY_TOKEN, token);

          //set globals
          CustomLogger.logString("Current Globals.... ");
          Globals.showAll();

          Globals.setRoleId(this.userProfile.role_uuid);
          Globals.setUserId(this.userProfile.uuid);
          Globals.setVendorId(this.userProfile.vendor_uuid);

          //get company category from the vendor
          this._service.getVendorDetails(this.userProfile.vendor_uuid).subscribe(
            data1 => {
              CustomLogger.logStringWithObject("getVendorDetails data: ", data1);
              let vendorProfile = new VendorProfile();
              vendorProfile = data1["data"];
              if (vendorProfile == null)
                throw new Error("No vendor found for this user...");
              Globals.setCompanyCategoryId(vendorProfile.company_category_uuid);
              Globals.setVendorName(vendorProfile.vendor_name);

              //set application role permission
              // this._accessService.initializeUserPermissions(this.userProfile.role_uuid, vendorProfile.company_category);
              CustomLogger.logString("About to set page permissions for user of role: " + this.userProfile.role_uuid);

              this._service.getPermissions(this.userProfile.role_uuid).subscribe(
                data2 => {
                  CustomLogger.logStringWithObject("getPermissions data: ", data2);
                  this._accessService.initializeUserPermissions(data2["data"]);
                  //redirect the user based on his profile status
                  if (!this.userProfile.has_completed_profile) {
                    CustomLogger.logString("should send to complete personal profile...");
                    Globals.setUserValid(false);
                    this.router.navigate([UserAccessConstants.PAGE_PROFILE_PERSONAL]);
                  } else {
                    CustomLogger.logString("send to dashboard");
                    Globals.setUserValid(true);
                    this.router.navigate([UserAccessConstants.PAGE_DASHBOARD]);
                  }
                },
                err2 => {
                  CustomLogger.logStringWithObject("getPermissions Error: ", err2);
                }
              );
            },
            err1 => {
              CustomLogger.logStringWithObject("getVendorDetails Error: ", err1);
              alert(err1);
            }
          );
        } else {
          CustomLogger.logString("Invalid Login Credentials but still logging in as default user.");
          //create dummy user
          this.userProfile = new UserProfile();
          Globals.setRoleId("1");
          Globals.setUserId("1");
          Globals.setVendorId("1");
          Globals.setCompanyCategoryId("1");
          Globals.setVendorName("Owner");
          Globals.setUserValid(true);
          this.router.navigate([UserAccessConstants.PAGE_DASHBOARD]);
        }
      },
      err => {
        CustomLogger.logStringWithObject("Error authenticateUser: ", err);
        alert(err);
      }
    );


  }

}
