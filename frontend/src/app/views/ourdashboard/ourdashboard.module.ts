import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { CommonModule } from '@angular/common';
import { OurdashboardRoutingModule } from './ourdashboard-routing.module';
import { OurdashboardComponent } from './ourdashboard.component';
import { FormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap';


@NgModule({
  imports: [
    OurdashboardRoutingModule,
    ChartsModule,
    BsDropdownModule,
    CommonModule,
    FormsModule,
    BsDatepickerModule.forRoot()
  ],
  declarations: [ OurdashboardComponent ]
})
export class OurdashboardModule { } 
