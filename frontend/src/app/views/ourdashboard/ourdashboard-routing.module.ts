import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OurdashboardComponent } from './ourdashboard.component';



const routes: Routes = [
  {
    path: '',
    component: OurdashboardComponent,
    data: {
      title: 'Our Dashboard'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OurdashboardRoutingModule {} 
