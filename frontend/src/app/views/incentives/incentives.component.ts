import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationEnd } from "@angular/router";
import { Papa } from 'ngx-papaparse';
import * as saveAs from 'file-saver';
import { CustomGlobalConstants } from '../../others/utils/CustomGlobalConstants';
import { ServerService } from '../../service/server.service';
import { Globals } from '../../others/models/Globals';
import { CustomLogger } from '../../others/utils/CustomLogger';
import { CustomMisc } from '../../others/utils/CustomMisc';
import { Incentive } from '../../others/models/incentive.model';

@Component({
  templateUrl: 'incentives.component.html',
  styleUrls: ['incentives.component.css']
})
export class IncentivesComponent implements OnInit {
  incentives: any;

  //type of incentive
  INCENTIVE_TYPE: string = CustomGlobalConstants.INCENTIVE_TYPE_REWARD;//default

  searchIncentivesString: any;
  selected: string;
  suggestedIncentivesArr: string[] = [];
  couponTempID: String = null;

  //html specific
  htmlTotalIncentives: string = "";
  displayHtmlIncentiveTypeText: string = "";
  searchIncentivesType: any;
  htmlSearchIncentivesTypeArr: string[] = [];
  searchStartTimeString;
  searchEndTimeString: any;
  myDateValue: Date;
  htmlShowBatchIncetiveButton: any;
  htmlSearchPrimaryIncentivesTypeArr: string[] = [];
  searchPrimaryIncentivesType: any;

  //date time picker specific
  mySettings = {
    bigBanner: true,
    timePicker: true,
    format: 'dd-MMM-yyyy hh:mm a',
    defaultOpen: false,
    closeOnSelect: false
  }

  //array to keep track of which coupons are clicked
  incentivesClickedArr = [];


  //flags
  htmlShowDeactivateButton = false;

  constructor(
    private router: Router,
    private _activatedRoute: ActivatedRoute,
    private server: ServerService,
    private _papaParser: Papa) {
    if (!Globals.isUserValid()) {
      router.navigate(['profile']);
    }
    //there is a bug in angular that it doesn;t reload the page when same router links are clicked
    //this workaround was received from https://stackoverflow.com/questions/40983055/how-to-reload-the-current-route-with-the-angular-2-router/40983262
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        //scroll to top
        this.router.navigated = false;
        window.scrollTo(0, 0);
      }
    })
  }

  ngOnInit() {
    //based on incentive type, modify respective fields/properties
    this.INCENTIVE_TYPE = CustomGlobalConstants.INCENTIVE_TYPE_REWARD;//default
    //get incentive type from params
    this._activatedRoute.params.subscribe(
      params => {
        CustomLogger.logString("received params.....");
        CustomLogger.logObj(params);
        this.INCENTIVE_TYPE = params.incentiveType;
      }
    );

    if(this.INCENTIVE_TYPE == null || this.INCENTIVE_TYPE == undefined || this.INCENTIVE_TYPE == "")
      this.INCENTIVE_TYPE = CustomGlobalConstants.INCENTIVE_TYPE_REWARD
    //set the current incentive type
    CustomLogger.logString("Populating page with incentive of type: " + this.INCENTIVE_TYPE);
    switch (this.INCENTIVE_TYPE) {
      case CustomGlobalConstants.INCENTIVE_TYPE_REWARD:
        this.displayHtmlIncentiveTypeText = CustomGlobalConstants.CONSTANT_REWARD;
        this.htmlSearchIncentivesTypeArr = CustomGlobalConstants.PICKLIST_REWARDS_SEARCH;
        this.searchIncentivesType = CustomGlobalConstants.PICKLIST_REWARDS_SEARCH[0];
        break;
      case CustomGlobalConstants.INCENTIVE_TYPE_COUPON:
        this.displayHtmlIncentiveTypeText = CustomGlobalConstants.CONSTANT_COUPON;
        this.htmlSearchIncentivesTypeArr = CustomGlobalConstants.PICKLIST_COUPONS_SEARCH;
        this.searchIncentivesType = CustomGlobalConstants.PICKLIST_COUPONS_SEARCH[0];
        this.htmlShowBatchIncetiveButton = true;
        break;
    }

    this.htmlSearchPrimaryIncentivesTypeArr = CustomGlobalConstants.PICKLIST_PRIMARY_INCENTIVES_SEARCH;
    this.searchPrimaryIncentivesType = CustomGlobalConstants.PICKLIST_PRIMARY_INCENTIVES_SEARCH[0];

    this.fetchAllIncentives();
  }

  async fetchAllIncentives() {
    // await this.server.fetchMyDetails();//gets this.me.uuid
    this.server.getAllIncentives(Globals.getVendorId(), this.INCENTIVE_TYPE).subscribe(
      data => {
        this.receivedIncentives(data['data']);
        this.setFormDefaults();
      },
      error => {
        CustomLogger.logStringWithObject("Error in fetching incentives...", error);
        // alert('fetchCoupons, alert not handled');
      }
    );
  }


  setFormDefaults() {
    //defaults
    this.searchStartTimeString = "";
    this.searchEndTimeString = "";
    CustomLogger.logString("Search type: " + this.searchIncentivesType);
    this.htmlShowDeactivateButton = false;
    if (this.searchIncentivesType == "Active")
      this.htmlShowDeactivateButton = true;
    this.incentivesClickedArr = [];//initialize to empty
  }


	/**
	 * fetch specific Incentives based on the Incentives search criteria
	 */
  async fetchSpecificIncentives() {
    CustomLogger.logString("111 find specific Incentives...");
    // await this.server.fetchMyDetails();//gets this.me.uuid
    CustomLogger.logStringWithObject("222 find specific Incentives...", this.searchPrimaryIncentivesType);
    let searchStartTimeNumber = 0;
    let searchEndTimeNumber = 0;

    if (!this.searchStartTimeString || this.searchStartTimeString == null || this.searchStartTimeString == "")
      searchStartTimeNumber = 0;
    else {
      searchStartTimeNumber = CustomMisc.getStartingDayTimestamp(new Date(this.searchStartTimeString));
    }
    if (!this.searchEndTimeString || this.searchEndTimeString == "")
      searchEndTimeNumber = 0;
    else {
      searchEndTimeNumber = CustomMisc.getEndingDayTimestamp(new Date(this.searchEndTimeString));
    }
    this.server.getSpecifcIncentives(Globals.getVendorId(), this.INCENTIVE_TYPE, this.searchIncentivesString, this.searchPrimaryIncentivesType, this.searchIncentivesType,
      searchStartTimeNumber, searchEndTimeNumber).subscribe(
        data => {
          this.receivedIncentives(data['data']);
          this.setFormDefaults();
        },
        error => {
          alert('fetchCoupons, alert not handled');
        }
      );
  }

	/**
	 * 
	 * @param newIncentives populate the Incentives section in html
	 */
  receivedIncentives(newIncentives) {
    CustomLogger.logString("Received Incentives...");
    CustomLogger.logObj(newIncentives);
    for (let i in newIncentives) {
      if (!newIncentives[i].image_link) {
        // Incentives[i].image_link = './../../../assets/free_dinner.jpg'
        newIncentives[i].image_link = CustomGlobalConstants.DEFAULT_INCENTIVE_IMAGE_FILE_LINK;
      }
      //fill up the suggested Incentives section
      this.suggestedIncentivesArr.push(newIncentives[i].name);
    }
    this.incentives = newIncentives;
    this.htmlTotalIncentives = this.incentives.length;
    this.initTestCheckBoxArr();
  }

  onClickCreateIncentive() {
    //this.router.navigate(['/rewards/add'], { queryParams: { t: 'r' } });		
    this.router.navigate(['incentives/add', this.INCENTIVE_TYPE])
    // this.router.navigate(['crud-incentive', 'reward'])
    // this.router.navigate(['crud-incentive']);
  }

	/**
	 * function to call when search for specific Incentives button is pressed
	 */
  searchIncentives() {
    this.fetchSpecificIncentives();
  }

  onClickUpdate(incentive) {
    CustomLogger.logObj(incentive);
    ////this.router.navigate(['/coupons/edit/', reward.uuid]);
    this.router.navigate(['incentives/modify', this.INCENTIVE_TYPE, CustomGlobalConstants.CRUD_UPDATE, incentive.uuid]);
  }

  onClickDownloadIncentives() {
    let options1 = {
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true,
      showTitle: true,
      useBom: true,
      noDownload: false,
      headers: ["id", "Name", "Summary", "Reward Points", "Image - URL Location", "UUID", "Vendor UID", "",]
    };
    let options = {
      headers: ["id", "Summary", "Image - URL Location", "", "Video Link", "Vendor UID", "", "", "Name", "Reward Points",
        "UUID", "Discount Value", "Is Vefified", "Is Active", "Last Modified",
        "Created Time", "Start Time", "End Time", "-NA-", "Discount Type", "Sub Category", "Is Deleted",
        "Category", "Type"]
    };

    CustomLogger.logString("will download following Incentives");
    CustomLogger.logObj(this.incentives);
    // let res = new Angular5Csv(this.rewards, "rewards", options);
    console.log("saved");
    // console.log(res);
    this.unparseAndCopyCSV(this.incentives);
  }

  unparseAndCopyCSV(incentiveArr: Array<Incentive>) {
    console.log("about to unparse....");
    var fileContents = this._papaParser.unparse(incentiveArr);
    var file = new Blob([fileContents], { type: "text/plain;charset=utf-8" });
    // let file = new Blob(this.fileContents, { type: 'text/csv;charset=utf-8' });
    let fileNameToSave = CustomGlobalConstants.REWARD_DEFAULT_FILE_NAME_FOR_DOWNLOAD;
    if (this.INCENTIVE_TYPE == CustomGlobalConstants.INCENTIVE_TYPE_COUPON)
      fileNameToSave = CustomGlobalConstants.COUPON_DEFAULT_FILE_NAME_FOR_DOWNLOAD;
    saveAs(file, fileNameToSave);

  }

  // async onClickUploadFile(event: any) {
  //   if (event.target.files && event.target.files.length > 0) {
  //     let csvFile = event.target.files[0];
  //     // let fileName = file.name;
  //     await this.server.fetchMyDetails();//gets this.me.uuid
  //     await this.parseCSVAndAddIncentives(csvFile);
  //   }
  // }


  currentUploadFile = null;
  onClickUploadFile(event: any) {
    if (event.target.files && event.target.files.length > 0) {
      this.currentUploadFile = event.target.files[0];
      // let fileName = file.name;

    }
  }
  async onClickUploadIncentives() {
    if (this.currentUploadFile == null) {
      alert("Kindly first choose the file to upload.");
      return;
    }
    // await this.server.fetchMyDetails();//gets this.me.uuid
    await this.parseCSVAndAddIncentives(this.currentUploadFile);
    alert("Incentives successfully uploaded.");
  }


	/**
	 * Parse and add Incentive csv file with following conitions on the file and is using PapaParser 
	 * (1) First line should be headers name 
	 * (2) There should be exactly 7 columns
	 * (3) The column items should be of following format
	 *    Column			Property
	 * ************		*********************
	 *  1st				Name
	 *  2nd				Summary
	 *  3rd 			Category
	 *  4th				Sub Category
	 *  5th				Image Link
	 *  6th				Reward Points
	 *  7th				Video URL
	 *  
	 */
  async parseCSVAndAddIncentives(csvFileToParse: File) {
    CustomLogger.logString("parseCSV. Ready to parse file: " + csvFileToParse.name);
    CustomLogger.logString("Current Vendor UUID: " + Globals.getVendorId);
    let currentVendorUUID = Globals.getVendorId();
    //let csvData = '"Hello","World!"';
    // let csvData = 'c:/tmp/test1.csv';
    // let fi = new File(null, 'c:/tmp/test1.csv');

    let options = {
      complete: (results, file) => {
        console.log('Parsed: ', results, file);
        CustomLogger.logString("File:");
        CustomLogger.logObj(file);
        CustomLogger.logString("Results:");
        CustomLogger.logObj(results);
        CustomLogger.logString(results.data.length);
        let incentiveArr: Array<Incentive> = [];
        let lineNo = 0;
        for (let res of results.data) {
          CustomLogger.logString("RES:" + lineNo);
          CustomLogger.logObj(res);
          //ignore the first line as it would be heading          
          lineNo++;
          if (lineNo == 1) continue;//this is the header so ingnore it

          //create incentive objects
          let incentive = new Incentive();
          incentive.type = this.INCENTIVE_TYPE;
          incentive.vendor_uuid = currentVendorUUID;
          incentive.is_deleted = false;
          incentive.is_active = true;

          incentive.name = res[0];
          incentive.summary = res[1];
          incentive.category = res[2];
          incentive.sub_category = res[3];
          let tmpImageLink = res[4];
          incentive.image_link = (tmpImageLink == undefined || tmpImageLink.length == 0) ? CustomGlobalConstants.DEFAULT_INCENTIVE_IMAGE_FILE_LINK : tmpImageLink;
          incentive.reward_points = res[5];
          incentive.video_url = res[6];
          incentiveArr.push(incentive);
        }

        //print all incentive
        CustomLogger.logString("All incentives.... ")
        CustomLogger.logObj(incentiveArr);

        this.server.createIncentivesFromUpload(incentiveArr).subscribe(
          data => {
            CustomLogger.logString("Got reply from server.... .... ");
            CustomLogger.logObj(data);
            this.fetchAllIncentives();//display all Incentives
          },
          err => {
            CustomLogger.logString("Got error.... " + err);
          }
        );

      },
      // Add other options here
      skipEmptyLines: true
    };
    try {
      this._papaParser.parse(csvFileToParse, options);
      CustomLogger.logString("Success in parsing.");
    } catch (error) {
      CustomLogger.logString("Error in parsing....")
      CustomLogger.logObj(error);
    }

  }

  onClickDeactivateBatchIncentives() {
    CustomLogger.logString("Will deactivate the following incentives");
    // CustomLogger.logObj(this.incentives);
    CustomLogger.logObj(this.incentivesClickedArr);
    if (this.incentivesClickedArr.length == 0) {
      alert("Kindly select items for deactivating");
      return;
    }
    alert("Total '" + this.incentivesClickedArr.length + "' " + this.displayHtmlIncentiveTypeText + " will be deactivated (soon).");
    // return;

    //put some logic here as to which incentives can be deactivated
    this.server.deactivateBatchIncentives(Globals.getVendorId(), this.incentivesClickedArr).subscribe(
      data => {
        CustomLogger.logString("received response from server....");
        CustomLogger.logObj(data);
        alert("Selected  " + this.displayHtmlIncentiveTypeText + " deactivated.");
        this.fetchAllIncentives();

      },
      error => {
        CustomLogger.logString("received error from server...." + error);
      }
    )
  }

  onIncentiveCheckboxClick(event) {
    CustomLogger.logStringWithObject("clicked check box...", event);
    CustomLogger.logString(event);
    let isCheckboxClicked = event.target.checked;
    let incentiveId = event.target.value;
    let incentiveName = event.target.name;

    if (isCheckboxClicked) {
      this.incentivesClickedArr.push(incentiveId);
      // this.incentivesClickedArr.push(incentiveName);
    } else {
      this.incentivesClickedArr.splice(this.incentivesClickedArr.indexOf(incentiveId), 1)
      // this.incentivesClickedArr.splice(this.incentivesClickedArr.indexOf(incentiveName), 1)
    }
    CustomLogger.logObj(this.incentivesClickedArr);
  }

  onClickDeactivateAllIncentives() {
    let incentivesNameArr: Array<string> = [];
    for (let k = 0; k < this.incentives.length; k++) {
      incentivesNameArr.push(this.incentives[k].name);
    }
    alert("Total " + incentivesNameArr.length + " Coupons i.e. ['" + incentivesNameArr + "'] will be deactivated (soon)");
  }


  ///////////////////////////////////////////////

  names = [];
  newNames = [];
  selectedAll: any;

  checkboxAllIncentives($event) {
    CustomLogger.logStringWithObject("selectALL: " + this.selectedAll, $event);
    if (this.selectedAll) {
      CustomLogger.logString("all incentives are activated");
      this.incentivesClickedArr = [];
      for (let k = 0; k < this.incentives.length; k++) {
        this.incentivesClickedArr.push(this.incentives[k].uuid);
      }
      CustomLogger.logStringWithObject("Following incentives will be activated...", this.incentivesClickedArr);

    } else {
      CustomLogger.logString("all incentives are deee");
      this.incentivesClickedArr = [];//initialize to 0
    }
    CustomLogger.logObj(this.names);
    for (var i = 0; i < this.names.length; i++) {
      console.log("ytp: " + this.names[i].type);
      this.names[i].selected = this.selectedAll;
      this.names[i].checked = this.selectedAll;

    }
  }


  initTestCheckBoxArr() {
    this.names = [];
    this.newNames = [];
    for (let k = 0; k < this.incentives.length; k++) {
      this.names.push({
        name: this.incentives[k].uuid,
        selected: false
      })
    }

  }

  selectAll($event) {
    CustomLogger.logStringWithObject("selectALL: " + this.selectedAll, $event);
    CustomLogger.logObj(this.names);
    for (var i = 0; i < this.names.length; i++) {
      this.names[i].selected = this.selectedAll;
    }
  }
  checkIfAllSelected($event) {
    CustomLogger.logStringWithObject("checkIfAllSelected: " + this.selectedAll, $event);
    this.selectedAll = this.names.every(function (item: any) {
      return item.selected == true;
    })
  }

}//end of class
