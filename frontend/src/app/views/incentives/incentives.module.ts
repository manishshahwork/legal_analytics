import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule, TypeaheadModule, BsDatepickerModule} from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { IncentivesRoutingModule } from './incentives-routing.module';
import { IncentivesComponent } from './incentives.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  imports: [
    IncentivesRoutingModule,
    ChartsModule,
    BsDropdownModule,
    CommonModule,
    FormsModule,
    TypeaheadModule.forRoot(),
    BsDatepickerModule.forRoot()
  ],
  declarations: [ IncentivesComponent ]
})
export class IncentivesModule { } 
