import { Component, OnInit, Input, TemplateRef } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { Router } from "@angular/router";
import * as AWS from 'aws-sdk/global';
import * as S3 from 'aws-sdk/clients/s3';
import { Incentive } from '../../others/models/incentive.model';
import { CustomGlobalConstants } from '../../others/utils/CustomGlobalConstants';
import { ServerService } from '../../service/server.service';
import { CustomLogger } from '../../others/utils/CustomLogger';
import { CustomMisc } from '../../others/utils/CustomMisc';
import { Globals } from '../../others/models/Globals';

@Component({
  templateUrl: 'crud-incentive.component.html'
})
export class CrudIncentiveComponent implements OnInit {

  incentive = new Incentive();

  // datePickerConfig: Partial<BsDatepickerConfig>;//confi for chaning datepicker config
  currentIncentiveType: string = CustomGlobalConstants.INCENTIVE_TYPE_COUPON; //coupon/reward/rebate - the most important variable of this html
  currentCrudType: string = "c"; //c-create, r-readonly, u-update, d-delete
  currentImageFile: File;
  
  isAddMode: boolean = true;
  incentiveTempID: String = null;
  message: string = "";
  rewardPoints: string = "";
  uploadImageFlag: boolean = false;
  htmlImageLink: string = CustomGlobalConstants.DEFAULT_IMAGE_FILE_LINK;

  displayHtmlIncentiveTypeText: String = CustomGlobalConstants.CONSTANT_COUPON;
  displayHtmlCrudtypeText: String = CustomGlobalConstants.CRUD_CREATE;

  htmlStartTime: any;
  htmlEndTime: any;
  htmlCategoriresArr: string[] = [];
  htmlSubCategoriresArr: string[] = [];
  htmlDiscountTypeArr: string[] = [];

  //flags
  donotDeleteFlag = true; //flag for forcefully expiring the incentive but not deleting it
  isCurrentCrudTypeCreateFlag = false;
  isRewardFlag: boolean = false;//specifically for displaying reward specific form elements
  isCouponFlag: boolean = false;//specifically for displaying coupon specific form elements
  isBatchIncentiveFlag: boolean = false;

  //bsdatepicker config
  customMinDate: Date;
  customMaxDate: Date;


  constructor(
    private server: ServerService,
    private routerA: ActivatedRoute,
    private router: Router
  ) {

  }

  // async fetchTempID() {
  //   try {
  //     this.incentiveTempID = new String(await this.server.getIncentiveTempId());
  //   } catch (error) {
  //     console.log(error);
  //   }
  // }

  async onImageUpload(event) {
    //let file: File = files.item(0);
    let imageFile = event.target.files[0];
    this.currentImageFile = imageFile;
    ////this.uploadImageFile(file);
    CustomLogger.logString("will upload file::: ");
    CustomLogger.logObj(this.currentImageFile);
    this.uploadImageFlag = true; // flag required in order to update image on server

    //preview the image

    var reader = new FileReader();
    reader.readAsDataURL(imageFile); // read file as data url
    reader.onload = (event: any) => { // called once readAsDataURL is completed
      // console.log("inside onload event...");
      this.htmlImageLink = event.target.result;
      // console.log("htmlImageLink:::' " + this.htmlImageLink);
    }
  }

  ngOnInit() {
    //defaults
    this.uploadImageFlag = false;
    //set the vendor id in the incentive.
    this.incentive.vendor_uuid = Globals.getVendorId();

    //html specific
    CustomLogger.logString("INCENTIVEEEEEEEEEEEEEEE");

    this.routerA.params.subscribe(
      params => {
        console.log("Params Values ::: ");
        console.log(params);
        //#### MHS 
        //see if its reward/coupon/
        this.currentIncentiveType = params[CustomGlobalConstants.PARAM_INCENTIVE_TYPE];
        //set the incentive type
        this.incentive.type = this.currentIncentiveType;//important assignment

        //common
        this.htmlCategoriresArr = CustomGlobalConstants.PICKLIST_INCENTIVES_CATEGORY;
        this.incentive.category = CustomGlobalConstants.PICKLIST_INCENTIVES_CATEGORY[0];
        this.htmlSubCategoriresArr = CustomGlobalConstants.PICKLIST_INCENTIVES_SUB_CATEGORY;
        this.incentive.sub_category = CustomGlobalConstants.PICKLIST_INCENTIVES_SUB_CATEGORY[0];
        this.htmlDiscountTypeArr = CustomGlobalConstants.PICKLIST_DISCOUNT_TYPE;

        this.modifyFlagsAsPerIncentiveType();


        //check if its add or modify(update/read/delete)
        this.currentCrudType = params[CustomGlobalConstants.PARAM_CRUD_TYPE];
        CustomLogger.logString("current crud type: " + this.currentCrudType);
        CustomLogger.logString("current incentive type: " + this.currentIncentiveType);

        //new incentive ?
        if (!this.currentCrudType) {
          CustomLogger.logString("New Incentive of type '" + this.currentIncentiveType + "'")
          this.currentCrudType = CustomGlobalConstants.CRUD_CREATE;
          this.displayHtmlCrudtypeText = CustomGlobalConstants.CONSTANT_CREATE;
          this.isCurrentCrudTypeCreateFlag = true;
          // this.fetchTempID();
          this.isAddMode = true;
          //starting dates of date picker
          this.customMinDate = new Date();
          //fill the vendor id
          this.incentive.vendor_uuid = Globals.getVendorId();
        } else {
          CustomLogger.logString("Incentive to be updated of type '" + this.currentIncentiveType + "'")
          //get the incentive id and check which type of modify crud operation it is i.e. read/update/delete
          let incentiveId = params[CustomGlobalConstants.PARAM_ID];
          //update or view incentive ?
          if (this.currentCrudType === CustomGlobalConstants.CRUD_UPDATE || this.currentCrudType === CustomGlobalConstants.CRUD_READ) {
            this.fetchIncentiveDetailsForEditing(incentiveId);
            this.displayHtmlCrudtypeText = CustomGlobalConstants.CONSTANT_UPDATE;
            this.isAddMode = false;
          }
        }
      }
    )
  }

  modifyFlagsAsPerIncentiveType() {
    //specific
    switch (this.currentIncentiveType) {
      case CustomGlobalConstants.INCENTIVE_TYPE_REWARD:
        this.isRewardFlag = true;
        this.isCouponFlag = false;
        this.displayHtmlIncentiveTypeText = CustomGlobalConstants.CONSTANT_REWARD;
        break;
      case CustomGlobalConstants.INCENTIVE_TYPE_COUPON:
        this.isRewardFlag = false;
        this.isCouponFlag = true;
        this.displayHtmlIncentiveTypeText = CustomGlobalConstants.CONSTANT_COUPON;
        this.incentive.discount_type = CustomGlobalConstants.PICKLIST_DISCOUNT_TYPE[0];
        break;
    }
  }

  fetchIncentiveDetailsForEditing(id) {
    this.server.fetchIncentive(id).subscribe(
      data => {
        console.log("Fetch Incentive for updating");
        console.log(data['data']);
        if (data['data']) {
          console.log("Data Received from Server");
          this.incentive = data['data'];
          this.updateHTMLSpecificFields();

          console.log("Incentive to display");
          console.log(this.incentive);
        } else {
          this.incentiveFetchingFailed();
        }
      },
      error => {
        console.log(error);
        this.incentiveFetchingFailed();
      }
    );
  }

  updateHTMLSpecificFields() {
    //update html image link
    this.htmlImageLink = this.incentive.image_link;
    this.htmlStartTime = new Date(this.incentive.start_time);
    this.htmlEndTime = new Date(this.incentive.end_time);
    // console.log("start time:::: ");
    //var htmlStartTime = this.getTime(this.incentive.start_time);
    // console.log(this.htmlStartTime);
    // this.htmlDateRange = this.htmlStartTime + " - " + this.htmlEndTime;

  }

  incentiveFetchingFailed() {
    alert('Incentive fetching failed.')
    this.router.navigate(['/coupons']);
  }


  async onClickSubmit() {
    console.log("lllllllllllink:" + this.htmlImageLink);
    console.log("locallll : " + localStorage.getItem('htmlImageLink'));
    console.log("Before defaulting....");
    console.log(this.incentive);
    // CustomLogger.logString("date range.....");
    // CustomLogger.logObj(this.htmlDateRange);
    // CustomLogger.logString("start date");
    // CustomLogger.logObj(this.htmlDateRange[0]);

    //upload files to server
    if (this.uploadImageFlag) {
      //temporary object to hold values
      let tmpIncentive = this.incentive;
      await CustomMisc.uploadFileToRemoteServer(this.currentImageFile).then(function (dataLocation) {
        CustomLogger.logString("gottttttttttttt");
        CustomLogger.logObj(dataLocation);
        CustomLogger.logString("tmpIncentive");
        CustomLogger.logObj(tmpIncentive);
        //CustomLogger.logString("this.incentive");
        //CustomLogger.logObj(this.incentive);
        //this.incentive.image_link = dataLocation;
        tmpIncentive.image_link = dataLocation.toString();
        CustomLogger.logString("datalocstr:" + dataLocation.toString());
        // CustomLogger.logObj(tmpIncetive);
      });
      //convert back to original object
      this.incentive = tmpIncentive;
      CustomLogger.logString("this.incentive");
      CustomLogger.logObj(this.incentive);
    }


    //fill in missing fields
    if (this.isAddMode) {
      // this.incentive.uuid = this.incentiveTempID.toString();
      this.incentive.vendor_uuid = Globals.getVendorId();
    }
    //let start_time = this.incentive.start_time.valueOf();
    console.log("html start:::: " + this.htmlStartTime);
    console.log(Math.floor(this.htmlStartTime));

    //console.log(Math.floor(this.incentive.start_time))
    //let end_time = this.incentive.end_time.valueOf();

    this.incentive.start_time = Math.floor(this.htmlStartTime);
    this.incentive.end_time = Math.floor(this.htmlEndTime);
    console.log("After defaulting.... Form to be sent:");
    console.log(this.incentive);

    this.checkForExpiration();

    if (this.isAddMode) {
      this.createIncentive();
    } else {
      this.updateIncentive();
    }
  }

  //check if the incentive is expired or not
  checkForExpiration() {
    //get current date in timestamp
    let currentTime = Date.now();
    if (this.incentive.end_time >= currentTime) console.log("NNNOOOOOO");
    else console.log("YYYYYYYYESSSSSSSSSSSS");
  }

  createIncentive() {
    CustomLogger.logStringWithObject("Will create/update incentive:", this.incentive);
    CustomLogger.logStringWithObject("Before creating incentive: Vendor ID ", Globals.getVendorId());
    
    //add vendor uuid
    this.incentive.vendor_uuid = Globals.getVendorId();
    this.incentive.user_uuid = Globals.getUserId();
    //check if the incentive is in batch or not
    if (this.isBatchIncentiveFlag) {
      this.server.createBatchIncentive(this.incentive).subscribe(
        data => {
          if (this.currentIncentiveType === CustomGlobalConstants.INCENTIVE_TYPE_COUPON) {
            this.message = "Coupon created successfully, you can check them on your coupons list.";
            // alert('Coupon created successfully, you can check them on your coupons list.')
            this.router.navigate([CustomGlobalConstants.ROUTE_COUPON]);
          } else if (this.currentIncentiveType === CustomGlobalConstants.INCENTIVE_TYPE_REWARD) {
            this.message = "Reward created successfully, you can check them on your rewards list.";
            // alert('Reward created successfully, you can check them on your rewards list.')
            this.router.navigate([CustomGlobalConstants.ROUTE_REWARD]);
          }
        },
        error => {
          alert(error.message);
          this.message = "Failed to create coupon, make sure that you are logged in."
        }
      );

    } else {
      this.server.createIncentive(this.incentive).subscribe(
        data => {
          if (this.currentIncentiveType === CustomGlobalConstants.INCENTIVE_TYPE_COUPON) {
            this.message = "Coupon created successfully, you can check them on your coupons list.";
            // alert('Coupon created successfully, you can check them on your coupons list.')
            this.router.navigate([CustomGlobalConstants.ROUTE_COUPON]);
          } else if (this.currentIncentiveType === CustomGlobalConstants.INCENTIVE_TYPE_REWARD) {
            this.message = "Reward created successfully, you can check them on your rewards list.";
            // alert('Reward created successfully, you can check them on your rewards list.')
            this.router.navigate([CustomGlobalConstants.ROUTE_REWARD]);
          }
        },
        error => {
          alert(error.message);
          this.message = "Failed to create coupon, make sure that you are logged in."
        }
      );
    }
  }

  updateIncentive() {
    console.log("updatttttttttttttttting.... ");
    console.log(this.incentive);
    this.server.updateIncentive(this.incentive).subscribe(
      data => {
        if (this.currentIncentiveType === CustomGlobalConstants.INCENTIVE_TYPE_COUPON) {
          this.router.navigate([CustomGlobalConstants.ROUTE_COUPON]);
          this.message = "Coupon updated successfully, you can check them on your coupons list.";
          // alert('Coupon updated successfully, you can check them on your coupons list.')
        } else if (this.currentIncentiveType === CustomGlobalConstants.INCENTIVE_TYPE_REWARD) {
          this.router.navigate([CustomGlobalConstants.ROUTE_REWARD]);
          this.message = "reward updated successfully, you can check them on your reward list.";
          // alert('reward updated successfully, you can check them on your reward list.')
        }
      },
      error => {
        alert(error.message);
        this.message = "Failed to update coupon, make sure that you are loged in."
      }
    );
  }

  onClickCancel() {
    console.log("cancel button clicked");
    if (this.currentIncentiveType === CustomGlobalConstants.INCENTIVE_TYPE_COUPON) {
      this.router.navigate([CustomGlobalConstants.ROUTE_COUPON]);
    } else if (this.currentIncentiveType === CustomGlobalConstants.INCENTIVE_TYPE_REWARD) {
      this.router.navigate([CustomGlobalConstants.ROUTE_REWARD]);
    }
  }

  onClickDelete() {
    CustomLogger.logString("will delete this reward");
    if (this.incentive.is_active) {
      alert("Cannot delete this " + this.displayHtmlIncentiveTypeText + " as it is still active.");
    } else {
      //first update the incentive to deactivate and then delete it. - code to be added.
      if (this.donotDeleteFlag) {
        alert(this.displayHtmlIncentiveTypeText + " will be marked as 'delete' but will not be actually deleted");
        //change the status
        this.incentive.is_active = false;
        this.incentive.is_deleted = true;
        this.updateIncentive();
      }
      else {
        alert("Item will be completely deleted");
        this.server.deleteIncentive({ vendor_uuid: this.incentive.vendor_uuid }).subscribe(
          data => {
            // alert('Reward deleted');
            CustomLogger.logString("reward deleted....");
            if (this.currentIncentiveType === CustomGlobalConstants.INCENTIVE_TYPE_COUPON)
              this.router.navigate(['/coupons']);
            else if (this.currentIncentiveType === CustomGlobalConstants.INCENTIVE_TYPE_REWARD)
              this.router.navigate(['/rewards']);
          }, error => {
            alert('Failed to delete ' + this.currentIncentiveType);
            this.router.navigate(['/']);
          }
        );
      }
    }
  }

  onBatchRadioChange(changeBool) {
    this.isBatchIncentiveFlag = changeBool;
    this.incentive.is_batch = changeBool;// very important
  }

}