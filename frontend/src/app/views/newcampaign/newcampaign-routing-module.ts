import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewcampaignComponent } from './newcampaign.component';


const routes: Routes = [
  {
    path: '',
    component: NewcampaignComponent,
    data: {
      title: 'newcampaigns'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class newcampaignRoutingModule {} 
