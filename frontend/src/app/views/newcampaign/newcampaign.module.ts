import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule} from 'ngx-bootstrap';

import { CommonModule } from '@angular/common';
import { newcampaignRoutingModule } from './newcampaign-routing-module';
import { NewcampaignComponent } from './newcampaign.component';
import { BsDatepickerModule } from 'ngx-bootstrap';


@NgModule({
  imports: [
newcampaignRoutingModule,
    ChartsModule,
    BsDropdownModule,
    CommonModule,
    BsDatepickerModule.forRoot()
  ],
  declarations: [ NewcampaignComponent ]
})
export class newcampaignModule { } 
