import { Component, OnInit } from '@angular/core';
import { Incident } from '../../others/models/incident.model';

@Component({
  templateUrl: 'incident.component.html'
})
export class IncidentComponent implements OnInit {

  incident: Incident;
  constructor() { }

  ngOnInit() {
    
  }

}
