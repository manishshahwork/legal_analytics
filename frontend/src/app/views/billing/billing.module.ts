import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule} from 'ngx-bootstrap';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BillingRoutingModule } from './billing-routing.module';
import { BillingComponent } from './billing.component';


@NgModule({
  imports: [
    CommonModule, 
    BillingRoutingModule, 
    FormsModule
  ],
  declarations: [ BillingComponent ]
})
export class BillingModule { } 
