import { Component, OnInit, HostListener } from '@angular/core';
import { ServerService } from '../../service/server.service';
import { CustomLogger } from '../../others/utils/CustomLogger';
import { Globals } from '../../others/models/Globals';
import { PythonService } from '../../service/python.service';
import { LegalDocument } from '../../others/models/legalDocument.model';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ShowSingleDocumentComponent } from './showSingleDocument.component';


@Component({
    templateUrl: 'showDocumentList.component.html'
})
export class ShowDocumentListComponent implements OnInit {

    tableDataArr: any;
    totalLinesToProcess: any;
    constructor(private service: ServerService, private pythonService: PythonService, private router: Router, private modalService: BsModalService) {
    }

    ngOnInit() {
        this.getAllLegalDocuments();
    }

    getAllLegalDocuments() {
        this.service.getAllLegalDocuments().subscribe(
            data => {
                CustomLogger.logStringWithObject("getAllLegalDocuments data: ", data);
                this.tableDataArr = data["data"];
            },
            err => {
                CustomLogger.logStringWithObject("getAllLegalDocuments err: ", err);
            }
        );
    }

    modifyTableData(event) {
        CustomLogger.logStringWithObject("Modify table data..", event);

    }

    onClickUrl(fileUrl) {
        CustomLogger.logStringWithObject("Will view file:", fileUrl);
        this.openModalWithComponent(fileUrl);
    }

    onClickProcess(legalDocument: LegalDocument) {
        CustomLogger.logStringWithObject("Will process document:", legalDocument);
        if (!legalDocument.is_document_uploaded) {
            alert("Sorry! This Contract was Not Uploaded and thus cannot be processed.");
        } else {
            this.router.navigate(["processDocument", legalDocument.uuid]);
        }
    }

    onClickView(legalDocument: LegalDocument) {
        CustomLogger.logStringWithObject("Will view document:", legalDocument);
        if (!legalDocument.is_document_uploaded) {
            this.router.navigate(["addLegalDocument", legalDocument.uuid]);
        } else {
            this.openModalWithComponent(legalDocument.document_url);
        }
    }

    bsModalRef: BsModalRef;
    openModalWithComponent(fileUrl) {
        const initialState = {
            list: [
            ],
            class: 'modal-lg',
            title: 'Legal Document',
            pdfSrc: fileUrl
        };
        this.bsModalRef = this.modalService.show(ShowSingleDocumentComponent, { initialState });
        console.log("dashboard: bsmodal:");
        console.log(this.bsModalRef);
        // this.bsModalRef.content.title = 'Close';


        this.bsModalRef.content.event.subscribe(data => {
            console.log('Child component\'s event was triggered', data);
            this.bsModalRef.hide();            
          });
    }

    @HostListener('window:blur', ['$event'])
    onBlur(event: any): void {
        // Do something
        console.log("bbbbbbbbbbbbbb");
    }

}
