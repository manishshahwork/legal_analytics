import { Component, OnInit } from '@angular/core';
import { ServerService } from '../../service/server.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LegalDocument } from '../../others/models/legalDocument.model';
import { CustomLogger } from '../../others/utils/CustomLogger';
import { CustomGlobalConstants } from '../../others/utils/CustomGlobalConstants';
import { CustomMisc } from '../../others/utils/CustomMisc';
import { PythonService } from '../../service/python.service';


@Component({
    templateUrl: 'processDocument.component.html',
    styleUrls: ['processDocument.component.css']
})
export class ProcessDocumentComponent implements OnInit {

    showSpinnerFlag = false;
    legalDocument: LegalDocument;
    constructor(private service: ServerService, private activatedRoute: ActivatedRoute, private router: Router, private pythonService: PythonService) {
        this.legalDocument = new LegalDocument();
    }

    ngOnInit() {
        // alert("Python script will be called to view the Processed document");
        CustomLogger.logStringWithObject("route params: ", this.activatedRoute.params);
        this.activatedRoute.params.subscribe(
            params => {
                this.service.getLegalDocumentById(params.uuid).subscribe(
                    data => {
                        CustomLogger.logStringWithObject("getLegalDocumentById data: ", data);
                        this.legalDocument = data["data"];
                        CustomLogger.logStringWithObject("legalDocument name: ", this.legalDocument.document_name);
                    },
                    err => {
                        CustomLogger.logStringWithObject("getLegalDocumentById err: ", err);
                    }
                );
            }
        );
    }

    async onSubmit() {
        CustomLogger.logStringWithObject("About to process: ", this.legalDocument);
        alert("This contract will now be processed.");
        this.legalDocument.is_document_processed = true;//set the flag
        this.legalDocument.processed_document_text = "Summary of the Legal Document.... "; //default message in case python api fails

        //call python api
        this.showSpinnerFlag = true;
        this.pythonService.processDocument(this.legalDocument.document_url, this.legalDocument.processed_document_lines).subscribe(
            data => {
                this.showSpinnerFlag = false;
                // alert("Contract is successfully processed.");
                CustomLogger.logStringWithObject("SUCCESS in summarizing and data received: ", data);
                let dataArr = data["data"];
                this.legalDocument.processed_document_text = this.pythonService.getEntireTextFromDataArray(dataArr);
                //update the document
                this.service.updateLegalDocument(this.legalDocument).subscribe(
                    data => {
                        CustomLogger.logStringWithObject("updateLegalDocument data: ", data);
                        this.router.navigate(["processedDocumentList"]);
                    },
                    err => {
                        CustomLogger.logStringWithObject("updateLegalDocument err: ", err);
                        this.router.navigate(["processedDocumentList"]);
                    }
                );
            },
            err => {
                this.showSpinnerFlag = false;
                CustomLogger.logStringWithObject("ERROR In Calling Python API : ", err);
                alert("Sorry. Some error in summarizing '" + err["message"] + "'");
                // this.legalDocument.processed_document_text = err["message"];
                //update the document - code is repeated intentionally 
                this.service.updateLegalDocument(this.legalDocument).subscribe(
                    data => {
                        CustomLogger.logStringWithObject("updateLegalDocument data: ", data);
                        this.router.navigate(["processedDocumentList"]);
                    },
                    err => {
                        CustomLogger.logStringWithObject("updateLegalDocument err: ", err);
                        this.router.navigate(["processedDocumentList"]);
                    }
                );
            },
        );


        /////////////////////////
        //received data from pythonservice
        // this.legalDocument.processed_document_name = "Processed-" + this.legalDocument.document_name;
        // this.legalDocument.processed_document_url = this.legalDocument.document_url;


        //convert the text into a pdf file and store it on aws
        // let processedFileName = this.pythonService.generatePDFFromText(this.legalDocument.processed_document_text);
        // let processedFileName = "./assets/file1.pdf";
        // let file: File;
        // file.name = "fie1";


        // CustomLogger.logString("processedFileName: " + processedFileName);
        // //store the file on aws
        // let tmpObj = this.legalDocument;
        // CustomMisc.uploadFileToRemoteServer(processedFileName).then(function (dataLocation) {
        //     CustomLogger.logString("gottttttttttttt");
        //     CustomLogger.logObj(dataLocation);
        //     CustomLogger.logString("tmpIncentive");
        //     CustomLogger.logObj(tmpObj);
        //     //CustomLogger.logString("this.incentive");
        //     //CustomLogger.logObj(this.incentive);
        //     //this.incentive.image_link = dataLocation;
        //     tmpObj.processed_document_url = dataLocation.toString();
        //     CustomLogger.logString("datalocstr:" + dataLocation.toString());
        //     // CustomLogger.logObj(tmpIncetive);
        // });
        // this.legalDocument = tmpObj;


        // //update the document
        // this.service.updateLegalDocument(this.legalDocument).subscribe(
        //     data => {
        //         CustomLogger.logStringWithObject("updateLegalDocument data: ", data);
        //         this.router.navigate(["showDocumentList"]);
        //     },
        //     err => {
        //         CustomLogger.logStringWithObject("updateLegalDocument err: ", err);
        //         this.router.navigate(["showDocumentList"]);
        //     }
        // );
        /////////////////////////



    }
}//end of class