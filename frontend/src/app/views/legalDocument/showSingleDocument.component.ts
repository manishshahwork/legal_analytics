import { Component, OnInit, EventEmitter } from '@angular/core';
import { ServerService } from '../../service/server.service';
import { CustomLogger } from '../../others/utils/CustomLogger';
import { Globals } from '../../others/models/Globals';
import { PythonService } from '../../service/python.service';
import { LegalDocument } from '../../others/models/legalDocument.model';
import { ActivatedRoute } from '@angular/router';



@Component({
    templateUrl: 'showSingleDocument.component.html'
})
export class ShowSingleDocumentComponent implements OnInit {

    showWindowFlag = true;
    //pdf
    // pdfSrc:string = '../../../assets/Agreement.pdf';
    pdfSrc: string;
    title = "Test";
    displayTextFlag = false;
    displayText = "Testing....";

    // fileUrlToDisplay: string = '../../../assets/Agreement.pdf';

    tableDataArr: any;
    totalLinesToProcess: any;

    public eventEmitter: EventEmitter<any> = new EventEmitter();
    
    constructor(private service: ServerService, private activatedRoute:ActivatedRoute, private pythonService: PythonService) {
    }

    ngOnInit() {
        CustomLogger.logStringWithObject("pdfSrc::: " , this.pdfSrc);
        CustomLogger.logStringWithObject("params" , this.activatedRoute.params);
        this.activatedRoute.params.subscribe(
            params=>{
                // this.fileUrlToDisplay = params.fileUrlToDisplay;
            }
        );

    }

    onCloseWindow() {
        console.log("close...");
        console.log(this.eventEmitter); 
        this.showWindowFlag = false;
      }
}
