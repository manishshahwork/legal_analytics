import { Component, OnInit } from '@angular/core';
import { ServerService } from '../../service/server.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LegalDocument } from '../../others/models/legalDocument.model';
import { CustomLogger } from '../../others/utils/CustomLogger';
import { CustomGlobalConstants } from '../../others/utils/CustomGlobalConstants';
import { CustomMisc } from '../../others/utils/CustomMisc';

@Component({
  templateUrl: 'uploadDocument.component.html'
})
export class UploadDocumentComponent implements OnInit {

  //for file upload
  currentFile: File;
  CURRENT_UPLOAD_FILE_NAME = "CURRENT_UPLOAD_FILE_NAME";
  uploadFlag: boolean = false;
  htmlImageLink: string = CustomGlobalConstants.DEFAULT_IMAGE_FILE_LINK;

 

  legalDocument: LegalDocument;
  constructor(private service: ServerService, private activatedRoute: ActivatedRoute, private router: Router) {
  }

  ngOnInit() { 
    this.legalDocument = new LegalDocument();
    this.legalDocument.is_document_uploaded = true; //flag for all uploaded documents
    this.legalDocument.assigned_to_user = "Admin";
  }


  onFileUpload(event) {
    CustomLogger.logStringWithObject("Respective document will be uploaded...", event.target.value);
    //let file: File = files.item(0);
    let file = event.target.files[0];
    this.currentFile = file;
    localStorage.setItem(this.CURRENT_UPLOAD_FILE_NAME, this.currentFile.name);
    ////this.uploadFile(file);
    CustomLogger.logString("will upload file::: ");
    CustomLogger.logObj(this.currentFile);
    this.uploadFlag = true; // flag required in order to update file on server
    // this.htmlUploadedName = this.currentFile.name;

    //preview the file
    var reader = new FileReader();
    reader.readAsDataURL(file); // read file as data url
    reader.onload = (event: any) => { // called once readAsDataURL is completed
      // console.log("inside onload event...");
      this.htmlImageLink = event.target.result;
      // console.log("htmlImageLink:::' " + this.htmlImageLink);
    }
  }


  async onSubmit() {
    if (this.uploadFlag) {
      //temporary object to hold values
      let tmpObj = this.legalDocument;
      await CustomMisc.uploadFileToRemoteServer(this.currentFile).then(function (dataLocation) {
        CustomLogger.logString("gottttttttttttt");
        CustomLogger.logObj(dataLocation);
        CustomLogger.logString("tmpObj");
        CustomLogger.logObj(tmpObj);
        CustomLogger.logStringWithObject("CURRENT_UPLOAD_FILE_NAME:", localStorage.getItem("CURRENT_UPLOAD_FILE_NAME"));
        //CustomLogger.logString("this.incentive");
        //CustomLogger.logObj(this.incentive);
        //this.incentive.image_link = dataLocation;
        tmpObj.document_url = dataLocation.toString();
        CustomLogger.logString("datalocstr:" + dataLocation.toString());
        // CustomLogger.logString("htmlUploadedImageName:" + this.htmlUploadedImageName);
        // tmpObj.document_name = localStorage.getItem("CURRENT_UPLOAD_FILE_NAME");
        // CustomLogger.logObj(tmpIncetive);

      });
      //convert back to original object
      this.legalDocument = tmpObj;
      CustomLogger.logString("this.legalDocument");
      CustomLogger.logObj(this.legalDocument);

      //save it to server
      this.service.addLegalDocument(this.legalDocument).subscribe(
        data => {
          CustomLogger.logStringWithObject("addLegalDocument data: ", data);
          this.router.navigate(["ourdashboard"]);
        },
        err => {
          CustomLogger.logStringWithObject("addLegalDocument err: ", err);
        }
      );
    }
  }
}//end of class