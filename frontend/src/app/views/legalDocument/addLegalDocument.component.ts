import { Component, OnInit } from '@angular/core';
import { ServerService } from '../../service/server.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LegalDocument } from '../../others/models/legalDocument.model';
import { CustomLogger } from '../../others/utils/CustomLogger';
import { CustomGlobalConstants } from '../../others/utils/CustomGlobalConstants';
import { CustomMisc } from '../../others/utils/CustomMisc';

@Component({
    templateUrl: 'addLegalDocument.component.html'
})
export class AddLegalDocumentComponent implements OnInit {

    legalDocument: LegalDocument;
    is_document_new = true;
    constructor(private service: ServerService, private activatedRoute: ActivatedRoute, private router: Router) {
        this.legalDocument = new LegalDocument();
    }

    ngOnInit() {
        CustomLogger.logStringWithObject("route params: ", this.activatedRoute.params);
        this.activatedRoute.params.subscribe(
            params => {
                CustomLogger.logStringWithObject("route params: ", params);
                if (params.uuid) {
                    CustomLogger.logString("has uuid::::: ");
                    this.service.getLegalDocumentById(params.uuid).subscribe(
                        data => {
                            CustomLogger.logStringWithObject("getLegalDocumentById data:", data);
                            this.legalDocument = data["data"];
                            this.is_document_new = false;
                        },
                        err => {
                            CustomLogger.logStringWithObject("getLegalDocumentById err:", err);
                        }
                    );
                } else {
                    CustomLogger.logString("no uuid::::: ");
                }
            }
        );
    }
    async onSubmit() {
        CustomLogger.logString("this.legalDocument.assigned_to_user:'" + this.legalDocument.assigned_to_user + '"');
        if(this.legalDocument.assigned_to_user == undefined || this.legalDocument.assigned_to_user == "") this.legalDocument.assigned_to_user = "Admin";
        //save it to server
        if (this.is_document_new) {
            this.service.addLegalDocument(this.legalDocument).subscribe(
                data => {
                    CustomLogger.logStringWithObject("addLegalDocument data: ", data);
                    this.router.navigate(["ourdashboard"]);
                },
                err => {
                    CustomLogger.logStringWithObject("addLegalDocument err: ", err);
                    this.router.navigate(["ourdashboard"]);
                }
            );
        } else {
            this.service.updateLegalDocument(this.legalDocument).subscribe(
                data => {
                    CustomLogger.logStringWithObject("updateLegalDocument data: ", data);
                    this.router.navigate(["ourdashboard"]);
                },
                err => {
                    CustomLogger.logStringWithObject("updateLegalDocument err: ", err);
                    this.router.navigate(["ourdashboard"]);
                }
            );
        }
    }

    onBatchRadioChange(changeBool) {
        this.legalDocument.option_to_renew = changeBool;// very important
    }

}//end of class