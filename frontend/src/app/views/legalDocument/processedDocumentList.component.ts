import { Component, OnInit } from '@angular/core';
import { ServerService } from '../../service/server.service';
import { CustomLogger } from '../../others/utils/CustomLogger';
import { Globals } from '../../others/models/Globals';
import { PythonService } from '../../service/python.service';
import { LegalDocument } from '../../others/models/legalDocument.model';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ShowSingleDocumentComponent } from './showSingleDocument.component';


@Component({
    templateUrl: 'processedDocumentList.component.html'
})
export class ProcessedDocumentListComponent implements OnInit {

    tableDataArr: any;
    totalLinesToProcess: any;
    bsModalRef: BsModalRef;

    constructor(private service: ServerService, private pythonService: PythonService, private router: Router, private modalService: BsModalService) {
    }

    ngOnInit() {
        this.getAllLegalDocuments();
    }

    getAllLegalDocuments() {
        this.service.getAllProcessedLegalDocuments().subscribe(
            data => {
                CustomLogger.logStringWithObject("getAllProcessedLegalDocuments data: ", data);
                this.tableDataArr = data["data"];
            },
            err => {
                CustomLogger.logStringWithObject("getAllProcessedLegalDocuments err: ", err);
            }
        );
    }

    modifyTableData(event) {
        CustomLogger.logStringWithObject("Modify table data..", event);
    }

    onViewProcessedDocument(legalDocument:LegalDocument){
        CustomLogger.logStringWithObject("Will view file:", legalDocument.processed_document_url);
        const initialState = {
            list: [
            ],
            class: 'modal-lg',
            title: 'Processed Legal Document',
            displayTextFlag: true,
            displayText:legalDocument.processed_document_text
        };
        this.bsModalRef = this.modalService.show(ShowSingleDocumentComponent, { initialState });
    }


    onViewOriginalDocument(fileUrl){
        const initialState = {
            list: [
            ],
            class: 'modal-lg',
            title: 'Legal Document',
            pdfSrc: fileUrl
        };
        this.bsModalRef = this.modalService.show(ShowSingleDocumentComponent, { initialState });
    }

}
