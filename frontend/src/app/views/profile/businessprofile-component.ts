import { Component, OnInit } from '@angular/core';

import * as AWS from 'aws-sdk/global';
import * as S3 from 'aws-sdk/clients/s3';
import { Boolean } from 'aws-sdk/clients/cognitosync';
import { Router } from '@angular/router';
import { VendorProfile } from '../../others/models/vendorProfile.model';
import { DaysOfWeek } from '../../others/models/vendorProfile.model';
import { UserProfile } from '../../others/models/userProfile.model';
import { ServerService } from '../../service/server.service';
import { CustomGlobalConstants } from '../../others/utils/CustomGlobalConstants';
import { CustomLogger } from '../../others/utils/CustomLogger';
import { CustomMisc } from '../../others/utils/CustomMisc';
import { Globals } from '../../others/models/Globals';
import { UserAccessConstants } from '../../UserAccessConstants';

@Component({
  templateUrl: 'businessprofile-component.html'
})
export class BusinessprofileComponent {


  //for tabs
  currentPageNumber: number = 1;

  vendorProfile: VendorProfile;
  userProfile: UserProfile;

  //image specifics
  htmlProfileImageLink: string = CustomGlobalConstants.DEFAULT_PROFILE_IMAGE_FILE_LINK;
  currentProfileImageFile: File;
  htmlCoverImageLink: string = CustomGlobalConstants.DEFAULT_PROFILE_IMAGE_FILE_LINK;
  currentCoverImageFile: File;
  htmlLogoImageLink: string = CustomGlobalConstants.DEFAULT_PROFILE_IMAGE_FILE_LINK;
  currentLogoImageFile: File;
  uploadProfileImageFlag: boolean = false;
  uploadCoverImageFlag: boolean = false;
  uploadLogoImageFlag: boolean = false;


  constructor(private server: ServerService, private _router: Router) {
    this.vendorProfile = new VendorProfile();
    this.userProfile = new UserProfile();
  }

  ngOnInit() {
    this.fetchVendorDetails();
  }


  async fetchVendorDetails() {
    CustomLogger.logString("About to get Vendor details for id:'" + Globals.getVendorId() + "'");
    await this.server.getVendorDetails(Globals.getVendorId()).subscribe(
      data => {
        CustomLogger.logStringWithObject("fetchVendorDetails:data: ", data);
        this.vendorProfile = data["data"];
        if (this.vendorProfile.working_days == null || this.vendorProfile.working_days == undefined)
          this.vendorProfile.working_days = new DaysOfWeek();
        // this.vendorProfile = new VendorProfile();
        CustomLogger.logStringWithObject("my-profile:before vendorProfile: ", this.vendorProfile);
        // this.vendorProfile = CustomMisc.copyOnlyFilledValues(this.vendorProfile, data["data"][0]);

        //set image links
        if (this.vendorProfile.profile_link != "") this.htmlProfileImageLink = this.vendorProfile.profile_link;
        if (this.vendorProfile.cover_link != "") this.htmlCoverImageLink = this.vendorProfile.cover_link;
        if (this.vendorProfile.logo_link != "") this.htmlLogoImageLink = this.vendorProfile.logo_link;
        CustomLogger.logStringWithObject("my-profile:after vendorProfile: ", this.vendorProfile);
      },
      error => {
        CustomLogger.logStringWithObject("fetchVendorDetails: error: ", error);
      }
    );
  }


  onLocationChangeConfirm(result) {
    console.log('location change confirmed', result);
    this.vendorProfile.longitude = result.lng;
    this.vendorProfile.latitude = result.lat;
  }


  async uploadfile(file, type) {
    const bucket = new S3(
      {
        accessKeyId: 'AKIAJ7DS4TBACBOVO76Q',
        secretAccessKey: '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',
        region: 'asia-pacific-5'
      }
    );

    const params = {
      Bucket: 'kryptoblocks-image-upload-bucket',
      Key: file.name,
      Body: file,
      ContentType: 'image/jpeg',
      ContentDisposition: 'inline',
      ACL: 'public-read'
    };

    bucket.upload(params, function (err, data) {
      if (err) {
        console.log('There was an error uploading your file: ', err);
        return false;
      }
      alert('Successfully uploaded photo.');
      this.uploadFlag = true;
      console.log('Successfully uploaded file. data', data);
      if (type === 'profile') {
        localStorage.setItem('profileLink', data.Location);
        this.profileLink = localStorage.getItem('profileLink');
      } else if (type === 'cover') {
        localStorage.setItem('coverLink', data.Location);
        this.coverLink = localStorage.getItem('coverLink');
      } else {
        localStorage.setItem('logoLink', data.Location);
        this.logoLink = localStorage.getItem('logoLink');
      }
      // this.imageLink = data.Location;
      // console.log('data.Location'+ data.Location + 'image link ' + this.imageLink);
      return true;
    });
  }

  async upload(files, type) {
    let file: File = files.item(0);
    this.uploadfile(file, type);
  }


  async onSubmit() {
    CustomLogger.logStringWithObject("vendor profile to be updated: ", this.vendorProfile);

    //upload image files to server
    let tmpVendorProfile = this.vendorProfile;

    if (this.uploadProfileImageFlag) {
      //temporary object to hold values
      await CustomMisc.uploadFileToRemoteServer(this.currentProfileImageFile).then(function (dataLocation) {
        tmpVendorProfile.profile_link = dataLocation.toString();
        CustomLogger.logString("datalocstr:" + dataLocation.toString());

      });
    }

    if (this.uploadCoverImageFlag) {
      await CustomMisc.uploadFileToRemoteServer(this.currentCoverImageFile).then(function (dataLocation) {
        tmpVendorProfile.cover_link = dataLocation.toString();
        CustomLogger.logString("datalocstr:" + dataLocation.toString());

      });
    }
    if (this.uploadLogoImageFlag) {
      await CustomMisc.uploadFileToRemoteServer(this.currentLogoImageFile).then(function (dataLocation) {
        tmpVendorProfile.logo_link = dataLocation.toString();
        CustomLogger.logString("datalocstr:" + dataLocation.toString());

      });
    }

    //convert back to original object
    this.vendorProfile = tmpVendorProfile;
    this.server.updateProfile(this.vendorProfile).subscribe(
      data => { 
        CustomLogger.logStringWithObject("updateProfile data: ", data);
        this._router.navigate([UserAccessConstants.PAGE_DASHBOARD]);
      },
      err => { 
        CustomLogger.logStringWithObject("updateProfile error: ", err);
        alert ("Error in updating Business Profile '" + err + "'");
        this._router.navigate([UserAccessConstants.PAGE_DASHBOARD]);
      }
    );

    // //make the profile updated
    // await this.server.userUpdateCompletedProfile(this.userProfile.uuid, true).subscribe(
    //   data => {
    //     CustomLogger.logStringWithObject("After user profile update: ", data);
    //     //now update the vendor profile
    //     CustomLogger.logStringWithObject("vendor to be updated:", this.vendorProfile);
    //     this.server.updateProfile(this.vendorProfile).subscribe(
    //       data => {
    //         alert('Profile updated');
    //         //now validate the user
    //         Globals.setUserValid(true);
    //         this._router.navigate([CustomGlobalConstants.ROUTE_DASHBOARD]);
    //       },
    //       error => {
    //         console.log(error);
    //         alert('Profile updating failed');
    //         this._router.navigate([CustomGlobalConstants.ROUTE_DASHBOARD]);
    //       }
    //     );
    //   },
    //   err => {
    //     CustomLogger.logStringWithObject("After user profile update error: ", err);
    //   }
    // );

    CustomLogger.logStringWithObject("user profile updated and this.vendorProfile is", this.vendorProfile);
  }


  showPageNumber(pageNumber) {
    this.currentPageNumber = pageNumber;
  }

  /**
   * 
   * @param event 
   * @param imageNo  1=Profile, 2=Cover, 3=Logo
   */
  async onImageUpload(event, imageNo) {
    let imageFile = event.target.files[0];
    CustomLogger.logStringWithObject("will upload for imageNo:" + imageNo + " the file::: ", imageFile);

    //preview the image
    var reader = new FileReader();
    reader.readAsDataURL(imageFile); // read file as data url
    reader.onload = (event: any) => { // called once readAsDataURL is completed
      switch (imageNo) {
        case 1: this.currentProfileImageFile = imageFile;
          this.htmlProfileImageLink = event.target.result;
          this.uploadProfileImageFlag = true;
          break;
        case 2: this.currentCoverImageFile = imageFile;
          this.htmlCoverImageLink = event.target.result;
          this.uploadCoverImageFlag = true;
          break;
        case 3: this.currentLogoImageFile = imageFile;
          this.htmlLogoImageLink = event.target.result;
          this.uploadLogoImageFlag = true;
          break;
      }
    }
  }
}