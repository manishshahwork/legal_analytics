import { Component, OnDestroy } from '@angular/core';
import { navItems, NavData } from './../../_nav';
import { MyAuthService } from '../../service/myauth.service';
import { Router } from '@angular/router';
import { UserAccessConstants } from '../../UserAccessConstants';
import { Globals } from '../../others/models/Globals';
import { AccessService } from '../../service/access.service';
import { CustomLogger } from '../../others/utils/CustomLogger';

@Component({
  selector: 'app-dashboard',
  templateUrl: './default-layout.component.html'
})
export class DefaultLayoutComponent implements OnDestroy {
  public myNavItems = navItems;
  // public navItems: Array<NavData>;

  //html texts
  htmlTitle = "Legal Analytics";

  public sidebarMinimized = true;
  private changes: MutationObserver;
  public element: HTMLElement = document.body;
  constructor(
    private _myauthservice: MyAuthService,
    private _router: Router,
    private _accessService: AccessService
  ) {

    // this.fillUpRespectiveNavItems();
    // this.htmlTitle = Globals.getVendorName() + " - (" + Globals.getRoleId() + ")";

    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = document.body.classList.contains('sidebar-minimized');
    });

    this.changes.observe(<Element>this.element, {
      attributes: true,
      attributeFilter: ['class']
    });
  }

  ngOnDestroy(): void {
    this.changes.disconnect();
  }

  mylogout() {
    this._myauthservice.logout();
  }

  fillUpRespectiveNavItems() {
    let allNavItems = this.myNavItems;

    //fill up navitems map : key=url and value=NavData
    let allNavItemsMap: Map<string, NavData> = new Map();
    for (let k = 0; k < allNavItems.length; k++) {
      allNavItemsMap.set(allNavItems[k].url, allNavItems[k]);
    }

    //get page permission map for the current user (based on his role)
    let currentUserPagePermissionsMap = new Map();
    currentUserPagePermissionsMap = this._accessService.getPagePermissionsMap();
    let currentUserPagePermissionsMapKeys = Array.from(currentUserPagePermissionsMap.keys());

    //pull out all the navItems whose url exists in currentUserPagePermissionsMap and fill up new navItemsArr viz.customNavItemsArr
    let customNavItemsArr: Array<NavData> = [];
    let navData: NavData;
    console.log("currentUserPagePermissionsMapKeys:", currentUserPagePermissionsMapKeys);
    for (let x = 0; x < allNavItems.length; x++) {
      navData = allNavItems[x];
      console.log("navData[" + x + "]:", navData);
      if (currentUserPagePermissionsMapKeys.indexOf(navData.url) > -1) {
        console.log("This navdata needs to be included", navData);
        customNavItemsArr.push(navData);
      }
      //check for children
      if (navData.children) {
        // let tmpNavData = navData;
        let tmpNavData = { 'name': navData.name, 'url': navData.url, 'icon': navData.icon, 'children': [] };
        for (let k = 0; k < navData.children.length; k++) {
          let child = navData.children[k];
          console.log("child:", child);
          if (currentUserPagePermissionsMapKeys.indexOf(child.url) > -1) {
            console.log("before: tmpNavData:", tmpNavData);
            tmpNavData.children.push(child);
            // console.log("about to delete child:", child);
            // //remove the child
            // delete tmpNavData.children[child];
            console.log("after: tmpNavData:", tmpNavData);
          }
        }
        //if child are added only then add the navData
        if (tmpNavData.children.length > 0)
          customNavItemsArr.push(tmpNavData);
      }
    }
    CustomLogger.logStringWithObject("new customNavItemsArr", customNavItemsArr);

    // allNavItemsMap.forEach((value: NavData, key: string) => {
    //   console.log("key1:", key, " value1:", value);


    //   if(value.children){
    //     console.log("this key1 '" + key + "' has children");
    //     let childrenArr = value.children;
    //     console.log("childrenArr:: ", childrenArr.length);
    //     for (let k=0; k<childrenArr.length; k++){

    //       //create new NavData
    //       let newNavData = null;
    //       console.log("childrenArr[k]:: ", childrenArr[k]);
    //       if(currentUserPagePermissionsMap.get(childrenArr[k].url)){
    //         console.log("this child will be added");
    //         if(newNavData == null){
    //           newNavData
    //         }
    //       }
    //     }
    //   }

    //   // //check if value has a child
    //   // var navItem = allNavItemsMap.get(key);
    //   // console.log("navItem:: ", navItem);
    //   // if (navItem != undefined && navItem.children) {
    //   //   console.log("this key '" + key + "' has children");
    //   // }
    //   // if (navItem != undefined && navItem != null)
    //   //   customNavItemsArr.push(allNavItemsMap.get(key));
    // });
    // //fill up the navitems with customNavItemsArr
    // // this.navItems = customNavItemsArr;



    //pull out all the navItems whose url exists in currentUserPagePermissionsMap and fill up new navItemsArr viz.customNavItemsArr

    // currentUserPagePermissionsMap.forEach((value: NavData, key: string) => {
    //   console.log("key:", key, " value:", value);
    //   //check if value has a child

    //   // CustomLogger.logStringWithObject("key::", key);
    //   // CustomLogger.logStringWithObject("value::", value);
    //   var navItem = allNavItemsMap.get(key);
    //   console.log("navItem:: ", navItem);
    //   if (navItem != undefined && navItem.children) {
    //     console.log("this key '" + key + "' has children");
    //   }
    //   if (navItem != undefined && navItem != null)
    //     customNavItemsArr.push(allNavItemsMap.get(key));
    // });


    //fill up the navitems with customNavItemsArr
    this.myNavItems = customNavItemsArr;

    CustomLogger.logStringWithObject("allNavItems", allNavItems);
    CustomLogger.logStringWithObject("allNavItemsMap", allNavItemsMap);
    CustomLogger.logStringWithObject("currentUserPagePermissionsMap", currentUserPagePermissionsMap);
    CustomLogger.logStringWithObject("customNavItemsArr", customNavItemsArr);
    CustomLogger.logStringWithObject("New nav items.... ", customNavItemsArr);


  }

}
