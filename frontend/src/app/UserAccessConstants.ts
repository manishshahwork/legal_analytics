export class UserAccessConstants {

    static readonly LOGIN_VENDOR = "Vendor";
    static readonly LOGIN_CUSTOMER = "Customer";

    static readonly PERMISSION_VIEW = "VIEW";
    static readonly PERMISSION_ADD = "ADD";
    static readonly PERMISSION_EDIT = "EDIT";
    static readonly PERMISSION_DELETE = "DELETE";

    static readonly PERMISSION_TYPES = [
        UserAccessConstants.PERMISSION_VIEW,
        UserAccessConstants.PERMISSION_ADD,
        UserAccessConstants.PERMISSION_EDIT,
        UserAccessConstants.PERMISSION_DELETE
    ];

    //organization_level or roles - Admin, Owner, Store Manager, Merchant, Campaign Manager, Accountant
    static readonly COMPANY_ROLE_ADMIN = "Admin";
    static readonly COMPANY_ROLE_OWNER = "Owner";
    static readonly COMPANY_ROLE_STORE_MANAGER = "Store Manager";
    static readonly COMPANY_ROLE_MERCHANT = "Merchant";
    static readonly COMPANY_ROLE_CAMPAIGN_MANAGER = "Campaign Manager";
    static readonly COMPANY_ROLE_ACCOUNTANT = "Accountant";
    static readonly COMPANY_ROLE_TYPES = [
        UserAccessConstants.COMPANY_ROLE_ADMIN,
        UserAccessConstants.COMPANY_ROLE_STORE_MANAGER,
        UserAccessConstants.COMPANY_ROLE_MERCHANT,
        UserAccessConstants.COMPANY_ROLE_CAMPAIGN_MANAGER,
        UserAccessConstants.COMPANY_ROLE_ACCOUNTANT
    ];
    static readonly ROLE_TYPES_FOR_PARENT = [
        UserAccessConstants.COMPANY_ROLE_OWNER,
        UserAccessConstants.COMPANY_ROLE_ADMIN,
        UserAccessConstants.COMPANY_ROLE_STORE_MANAGER,
        UserAccessConstants.COMPANY_ROLE_MERCHANT,
        UserAccessConstants.COMPANY_ROLE_CAMPAIGN_MANAGER,
        UserAccessConstants.COMPANY_ROLE_ACCOUNTANT
    ];

    ////////////////////////////////////////////////////
    //individual components/pages
    static readonly PAGE_DASHBOARD = "/ourdashboard";
    static readonly PAGE_INCENTIVES_REWARD = "/incentives/reward";
    static readonly PAGE_INCENTIVES_COUPON = "/incentives/coupon";
    static readonly PAGE_CAMPAIGNS = "/campaigns";
    static readonly PAGE_BILLING = "/billing"; 
    static readonly PAGE_CUSTOMER = "/customer"; 
    // static readonly PAGE_SETUP = "/setup";
    static readonly PAGE_SETUP_COMPANY_CATEGORY = "/setup/companyCategory";
    static readonly PAGE_SETUP_ORGANIZATION_USERS = "/setup/organizationUsers";
    // static readonly PAGE_PROFILE = "/profile";
    static readonly PAGE_PROFILE_PERSONAL = "/profile/myprofile";
    static readonly PAGE_PROFILE_COMPANY = "/profile/businessprofile";
    static readonly PAGE_SUPPORT = "/support"; 
    static readonly PAGE_NEWCAMPAIGN = "/newcampaign"; 
    static readonly PAGE_CONFIGURATION_ASSIGN_PERMISSION = "/configuration/assignPermission";
    static readonly PAGE_CONFIGURATION_CRUD_COMPANY_CATEGORY = "/configuration/crudCompanyCategory";
    static readonly PAGE_CONFIGURATION_COMPANY_CATEGORY_LIST = "/configuration/crudCompanyCategory";
    
    ///////////////////////////////////////////////////////////////////////////////////
    //company categories - Parent/Franchise/Branch/Partner
    static readonly COMPANY_CATEGORY_PARENT_UUID = "1";
    static readonly COMPANY_CATEGORY_PARENT = "Parent";
    static readonly COMPANY_CATEGORY_FRANCHISE = "Franchise";
    static readonly COMPANY_CATEGORY_BRANCH = "Branch";
    static readonly COMPANY_CATEGORY_PARTNER = "Partner";
    static readonly COMPANY_CATEGORY_TYPES = [
        UserAccessConstants.COMPANY_CATEGORY_PARENT,
        UserAccessConstants.COMPANY_CATEGORY_FRANCHISE,
        UserAccessConstants.COMPANY_CATEGORY_BRANCH,
        UserAccessConstants.COMPANY_CATEGORY_PARTNER       
    ];
    static readonly COMPANY_CATEGORY_TYPES_FOR_PARENT = [
        UserAccessConstants.COMPANY_CATEGORY_FRANCHISE,
        UserAccessConstants.COMPANY_CATEGORY_BRANCH,
        UserAccessConstants.COMPANY_CATEGORY_PARTNER       
    ];

} 