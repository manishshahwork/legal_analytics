/**
 * All constants used in the application will go in this class file
 */
export class CustomGlobalConstants {

    //server url
    static readonly HOST_URL = "http://localhost:3000/api/v1";
    // static readonly HOST_URL = "https://localhost:8443/api/v1";
    // static readonly HOST_URL = "/api/v1";

    static readonly SECRET_KEY = "KRYPTO_SECRET";

    //Global Keys 
    static readonly KEY_TOKEN = "token";
    static readonly KEY_USER_ID = "user_id";
    
    //User type
    static readonly USER_ROLE_VENDOR = "vendor";
    static readonly USER_ROLE_CUSTOMER = "customer";

    //CRUD OPERATION CONSTANTS
    static readonly CRUD_CREATE = "c";
    static readonly CRUD_READ = "r";
    static readonly CRUD_UPDATE = "u";
    static readonly CRUD_DELETE = "d";

    //incentive types - should be same in server side
    static readonly INCENTIVE_TYPE_COUPON = "coupon"; //
    static readonly INCENTIVE_TYPE_REWARD = "reward";

    //categories
    static readonly INCENTIVE_CATEGORY_GENERAL = "GENERAL";
    static readonly INCENTIVE_SUB_CATEGORY_GENERAL = "sub cat 1";

    //discount types
    static readonly DISCOUNT_TYPE_DISCOUNT = "discount";
    static readonly DISCOUNT_TYPE_PRICE_SLASH = "price";


    //params constants
    static readonly PARAM_ID = "id";
    static readonly PARAM_INCENTIVE_TYPE = "incentiveType";
    static readonly PARAM_CRUD_TYPE = "crudType";

    //constants (Html display)
    static readonly CONSTANT_COUPON = "Coupon";
    static readonly CONSTANT_REWARD = "Reward";

    static readonly CONSTANT_CREATE = "Create";
    static readonly CONSTANT_READ = "Read";
    static readonly CONSTANT_UPDATE = "Update";
    static readonly CONSTANT_DELETE = "Delete";

    //html pick list constants
    static readonly PICKLIST_REWARDS_SEARCH: Array<string> = ['All', 'Active', 'Inactive', 'Expired', 'Deleted'];
    static readonly PICKLIST_COUPONS_SEARCH: Array<string> = ['All', 'Active', 'Inactive', 'Expired', 'Deleted'];
    static readonly PICKLIST_INCENTIVES_CATEGORY: Array<string> = ['GAMING', 'BANKS', 'AIRLINES', 'SALOONS', 'CAFFE',
        'GROCERIES', 'FOOD', 'HOTEL', 'HEALTH and FITNESS', 'RETAIL', 'SERVICES'];
    static readonly PICKLIST_INCENTIVES_SUB_CATEGORY: Array<string> = ['sub cat 1', 'sub cat 2', 'sub cat 3', 'sub cat 4'];
    static readonly PICKLIST_DISCOUNT_TYPE: Array<string> = ['Discount', 'Price Cut'];
    static readonly PICKLIST_PRIMARY_INCENTIVES_SEARCH: Array<string> = ['All', 'Normal', 'Batch'];
    
    //some default constants
    static readonly DEFAULT_INCENTIVE_IMAGE_FILE_LINK = "../../../assets/img/free_dinner.jpg";
    static readonly DEFAULT_IMAGE_FILE_LINK = "../../../assets/img/thumb_img_file.png";
    static readonly DEFAULT_PROFILE_IMAGE_FILE_LINK = "../../assets/img/thumb_img_file.png";

    //route navigation constants (as per app.module.ts)
    static readonly ROUTE_COUPON = "/incentives/coupon";
    static readonly ROUTE_REWARD = "/incentives/reward";
    static readonly ROUTE_PROFILE = "/my-profile";
    static readonly ROUTE_DASHBOARD = "ourdashboard";    

    //default file names to download
    static readonly REWARD_DEFAULT_FILE_NAME_FOR_DOWNLOAD = "rewards.csv";
    static readonly COUPON_DEFAULT_FILE_NAME_FOR_DOWNLOAD = "coupons.csv";


    //company categories
    // static readonly PICKLIST_COMPANY_CATEGORIES: Array<string> = ["Parent", "Franchise", "Branch", "Partner"];

    constructor() {

    }
}

