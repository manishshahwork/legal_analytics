export class CustomLogger {
    static logString(str){
        console.log("#### " + str + " ####");
    }

    static logObj(obj){
        console.log(obj);
    }

    static logStringWithObject(str, obj){
        this.logString(str);
        this.logObj(obj);
    }
}