import { S3 } from "aws-sdk/clients/all";

export class CustomMisc {

  static async uploadFileToRemoteServer(file) {
    const bucket = new S3(
      {
        accessKeyId: 'AKIAJ7DS4TBACBOVO76Q',
        secretAccessKey: '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',
        region: 'asia-pacific-5'
      }
    );

    const params = {
      Bucket: 'kryptoblocks-image-upload-bucket',
      Key: file.name,
      Body: file,
      ContentType: 'image/jpeg',
      ContentDisposition: 'inline',
      ACL: 'public-read'
    };

    return new Promise(function (resolve, reject) {
      bucket.upload(params, function (err, data) {
        if (err) {
          console.log('There was an error uploading your file: ', err);
          reject(err);
        }
        // alert('Successfully uploaded photo.');
        console.log('Successfully uploaded file. data', data);
        resolve(data.Location);
      });
    });
  }

  static getStartingDayTimestamp(date: Date) {
    date.setHours(0, 0, 0, 0);
    return date.getTime();
  }

  static getEndingDayTimestamp(date: Date) {
    date.setHours(23, 59, 59, 0);
    return date.getTime();
  }

  static copyOnlyFilledValues(baseObject, objectToCopy) {
    for (var k in objectToCopy)
      baseObject[k] = objectToCopy[k];
    return baseObject;
  }

}//end of class