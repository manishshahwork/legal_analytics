export class LegalDocument {
    uuid: string;
    document_name: string;
    document_url: string;
    description: string;
    copy_from_contract: string;
    test_project: boolean;
    base_language: string;
    hierarchical_type: string;
    supplier: string;
    affected_parties: string;
    proposed_contract_amount: string;
    original_contract_amount: string;
    commodity: string;
    regions: string;
    predecessor_project: string;
    option_to_renew: boolean;
    term_type: string;
    effective_date: number;
    expiration_date: number;

    processed_document_url: string;
    processed_document_name: string;
    processed_document_lines: number;

    is_document_uploaded: boolean = false;
    is_document_processed: boolean = false;

    assigned_to_user: string;
    processed_document_text: string;
}