export class PagePermission {
    uuid: string;
    name: string; //name
    url: string; //url
    company_role_uuid: string;//which role this is associated with
    is_readable: boolean = false;
    is_editable: boolean = false;
    is_deletable: boolean = false;
}