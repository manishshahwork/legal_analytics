export class Incident {
    uuid: string;
    device_name: string;
    device_ip: string;
    message: string;
    occurence_time: number;
    device_country: string;
    device_zip: string;
    device_state: string;
    device_city: string;
    device_address: string;

    listed_ip_addresses: string;
    unlisted_ip_addresses: string;
    os_version: string;
    compromised_devices: string;
    risk_devices: string;
    threat_activity: string;
    
}