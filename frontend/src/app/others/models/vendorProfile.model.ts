export class VendorProfile {
    uuid: string;//unique id
    // user_uuid: string;//which user it belongs to
    about: string;
    vendor_name: string;
    vendor_website: string;
    vendor_address: string;
    start_time: string;
    end_time: string;
    working_days: DaysOfWeek = new DaysOfWeek();
    color_scheme: string;
    profile_link: string = "";
    cover_link: string = "";
    logo_link: string = "";
    latitude: number;
    longitude: number;
    created_on: number = Math.floor(Date.now());
    last_modified: number = Math.floor(Date.now());
    parent_uuid: string; //parent vendor (it will be uuid if it is a parent himself)
    company_category_uuid: string; //Parent/Franchise/Branch/Partner
}

export class DaysOfWeek {
    monday: boolean = false;
    tuesday: boolean = false;
    wednesday: boolean = false;
    thursday: boolean = false;
    friday: boolean = false;
    saturday: boolean = false;
    sunday: boolean = false;
}