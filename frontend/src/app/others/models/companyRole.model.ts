export class CompanyRole {
    uuid: string;
    company_category_uuid: string; //which category this role belongs to    
    name: string;
}