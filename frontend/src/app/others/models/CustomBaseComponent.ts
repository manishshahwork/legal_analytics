export class CustomBaseComponent  {
    isReadable: boolean;
    isEditable: boolean;

    constructor(){
        this.isReadable = false;
        this.isEditable = false;
    }
}