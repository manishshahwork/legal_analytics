import { UserAccessConstants } from '../../UserAccessConstants';

export class UserProfile {
    uuid: string;//unique id
    first_name: string="";
    middle_name: string="";
    last_name: string="";
    full_name: string = this.first_name + " " + this.middle_name + " " + this.last_name;
    login_type: string = UserAccessConstants.LOGIN_VENDOR; //vendor/customer/...
    facebook_id: string;
    google_id: string;
    phone: number;
    date_of_birth: string;
    salutation: string;
    profile_link: string;
    email: string;
    created_on: number;
    last_modified: number;
    has_completed_profile: boolean = true;
    login_name: string;
    password: string;
    vendor_uuid: string;//which vendor (company) it is a user of
    role_uuid: string;//organization level - Admin, Owner, Store Manager, Merchant, Campaign Manager, Accountant    
}
