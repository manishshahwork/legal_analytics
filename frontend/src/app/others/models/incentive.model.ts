import { CustomGlobalConstants } from "../utils/CustomGlobalConstants";

/**
 * A generic model depicting coupon or rewards or rebates or discounts etc.
 */
export class Incentive {
    uuid: string;    
    vendor_uuid: string;
    name: string;
    code: string;
    type: string = CustomGlobalConstants.INCENTIVE_TYPE_COUPON;
    summary: string = "";
    category: string = CustomGlobalConstants.INCENTIVE_CATEGORY_GENERAL;
    image_link: string = "";
    image_url: string = "";
    video_url: string = "";
    description: string = "";
    product_url: string = "";
    reward_points: number = 0;
    is_verified: boolean = false;
    sub_category: string = CustomGlobalConstants.INCENTIVE_SUB_CATEGORY_GENERAL;
    discount_type: string = CustomGlobalConstants.DISCOUNT_TYPE_DISCOUNT;
    discount_value: number = -1;
    end_time: number;
    start_time: number;
    created_on: number;
    last_modified: number;
    is_active: boolean = true;//for activate/de-activate functionality
    is_deleted: boolean = false;//flag for delete
    is_batch: boolean = false;
    batch_name: string;
    batch_count: number = 0;//total number of incentives
    user_uuid: string;
    
    constructor() {
        //set defaults
        this.discount_value = 0;
        this.end_time = Math.floor(Date.now());
        this.start_time = Math.floor(Date.now());
        this.created_on = Math.floor(Date.now());
        this.last_modified = Math.floor(Date.now());
    }
}
