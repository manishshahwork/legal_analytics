export class Globals {
    private static is_valid: boolean = true;//should be false on startup
    private static user_id: string = "";
    private static vendor_id: string = "";
    private static company_category_id: string = "";
    private static role_id: string = "";
    private static current_page_permission = [];
    private static vendor_name: string = "";
    private static company_category_name: string="";
    private static company_role_name: string="";

    static isUserValid() {
        return this.is_valid;
    }

    static setUserValid(is_valid = false) {
        this.is_valid = is_valid;
    }

    static getVendorId() {
        return this.vendor_id;
    }

    static setVendorId(vendorId) {
        this.vendor_id = vendorId;
    }

    static getUserId() {
        return this.user_id;
    }
    static setUserId(userId) {
        this.user_id = userId;
    }
    static getCompanyCategoryId() {
        return this.company_category_id;
    }
    static setCompanyCategoryId(companyCategoryId) {
        this.company_category_id = companyCategoryId;
    }

    static getCurrentPagePermission() {
        return this.current_page_permission;
    }
    static setCurrentPagePermission(currentPagePermission: []) {
        this.current_page_permission = currentPagePermission;
    }

    static getRoleId() {
        return this.role_id;
    }
    static setRoleId(roleId) {
        this.role_id = roleId;
    }

    static getVendorName() {
        return this.vendor_name;
    }
    static setVendorName(vendorName) {
        this.vendor_name = vendorName;
    }

    static getCompanyCategoryName(){
        return this.company_category_name;
    }
    static setCompanyCategoryName(companyCategoryName) {
        this.company_category_name = companyCategoryName;
    }

    static getCompanyRoleName(){
        return this.company_role_name;
    }
    static setCompanyRoleName(companyRoleName) {
        this.company_role_name = companyRoleName;
    }

    static clearAll() {
        this.is_valid = false;
        this.user_id = "";
        this.vendor_id = "";
        this.company_category_id = "";
        this.role_id = "";
        this.current_page_permission = [];
        this.vendor_name = "";
    }

    static showAll() {
        console.log("is_valid:" + this.is_valid);
        console.log("user_id:" + this.user_id);
        console.log("vendor_id:" + this.vendor_id);
        console.log("company_category:" + this.company_category_id);
        console.log("role:" + this.role_id);
        console.log("current_page_permission:" + this.current_page_permission);
        console.log("vendor_name:" + this.vendor_name);
    }
}