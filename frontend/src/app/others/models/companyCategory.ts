export class CompanyCategory {
    uuid: string;
    user_uuid: string;
    name: string;
}