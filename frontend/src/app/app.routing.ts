import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';
import { CrudIncentiveComponent } from './views/crud-incentive/crud-incentive.component';
import { IncentivesComponent } from './views/incentives/incentives.component';
import { MyAuthGuard } from './myauth.guard';
import { UserAccessConstants } from './UserAccessConstants';
import { MyprofileComponent } from './views/profile/myprofile-component';
import { BusinessprofileComponent } from './views/profile/businessprofile-component';
import { CompanyCategoryComponent } from './views/setup/companyCategory-component';
import { OrganizationUsersComponent } from './views/setup/organizationUsers-component';
import { AssignPermissionComponent } from './views/configuration/assignPermission.component';
import { CrudCompanyCategoryComponent } from './views/configuration/companyCategory/crudCompanyCategory.component';
import { CompanyCategoryListComponent } from './views/configuration/companyCategory/companyCategoryList.component';
import { NotificationComponent } from './views/notification/notification.component';
import { ConnectedDevicesComponent } from './views/connectedDevices/connectedDevices.component';
import { IncidentComponent } from './views/incident/incident.component';
import { InvestigationReportComponent } from './views/investigationReport/investigationReport.component';
import { SettingsComponent } from './views/settings/settings.component';
import { ReportsComponent } from './views/reports/reports.component';
import { UploadDocumentComponent } from './views/legalDocument/uploadDocument.component';
import { ShowDocumentListComponent } from './views/legalDocument/showDocumentList.component';
import { ShowSingleDocumentComponent } from './views/legalDocument/showSingleDocument.component';
import { ProcessedDocumentListComponent } from './views/legalDocument/processedDocumentList.component';
import { AddLegalDocumentComponent } from './views/legalDocument/addLegalDocument.component';
import { ProcessDocumentComponent } from './views/legalDocument/processDocument.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: {
      title: 'Register Page'
    }
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'notification',
        component: NotificationComponent,
      },
      {
        path: 'connectedDevices',
        component: ConnectedDevicesComponent,
      },
      {
        path: 'incident',
        component: IncidentComponent,
      },
      {
        path: 'investigationReport',
        component: InvestigationReportComponent,
      },
      {
        path: 'settings',
        component: SettingsComponent,
      },
      {
        path: 'reports',
        component: ReportsComponent,
      },
      {
        path: 'uploadDocument',
        component: UploadDocumentComponent,
      },
      {
        path: 'showDocumentList',
        component: ShowDocumentListComponent,
      },
      {
        path: 'processedDocumentList',
        component: ProcessedDocumentListComponent,
      },
      {
        path: 'processDocument/:uuid',
        component: ProcessDocumentComponent,
      },
      {
        path: 'addLegalDocument',
        component: AddLegalDocumentComponent,
      },
      {
        path: 'addLegalDocument/:uuid',
        component: AddLegalDocumentComponent,
      },
      {
        path: 'showSingleDocument',
        component: ShowSingleDocumentComponent
      },




      {
        path: 'base',
        loadChildren: './views/base/base.module#BaseModule'
      },
      {
        path: 'buttons',
        loadChildren: './views/buttons/buttons.module#ButtonsModule'
      },
      {
        path: 'charts',
        loadChildren: './views/chartjs/chartjs.module#ChartJSModule'
      },
      {
        path: 'dashboard',
        loadChildren: './views/dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'icons',
        loadChildren: './views/icons/icons.module#IconsModule'
      },
      {
        path: 'notifications',
        loadChildren: './views/notifications/notifications.module#NotificationsModule'
      },
      {
        path: 'theme',
        loadChildren: './views/theme/theme.module#ThemeModule'
      },
      {
        path: 'widgets',
        loadChildren: './views/widgets/widgets.module#WidgetsModule'
      },
      {
        path: 'billing',
        loadChildren: './views/billing/billing.module#BillingModule',
        //canActivate: [MyAuthGuard],
        data: { 'page_name': UserAccessConstants.PAGE_BILLING }
      },
      {
        path: 'customer',
        loadChildren: './views/customer/customer.module#CustomerModule',
        //canActivate: [MyAuthGuard],
        data: { 'page_name': UserAccessConstants.PAGE_CUSTOMER }
      },
      {
        path: 'support',
        loadChildren: './views/support/support.module#SupportModule',
        //canActivate: [MyAuthGuard],
        data: { 'page_name': UserAccessConstants.PAGE_SUPPORT }
      },
      {
        path: 'campaigns',
        loadChildren: './views/campaigns/campaigns.module#CampaignsModule',
        //canActivate: [MyAuthGuard],
        data: { 'page_name': UserAccessConstants.PAGE_CAMPAIGNS }
      },

      {
        path: 'newcampaign',
        loadChildren: './views/newcampaign/newcampaign.module#newcampaignModule',
        //canActivate: [MyAuthGuard],
        data: { 'page_name': UserAccessConstants.PAGE_NEWCAMPAIGN }

      },

      {
        path: 'ourdashboard',
        loadChildren: './views/ourdashboard/ourdashboard.module#OurdashboardModule',
        //canActivate: [MyAuthGuard],
        data: { 'page_name': UserAccessConstants.PAGE_DASHBOARD }
      },
      {
        path: 'incentives/:incentiveType',
        component: IncentivesComponent,
        //canActivate: [MyAuthGuard],
        data: { 'page_name': UserAccessConstants.PAGE_INCENTIVES_REWARD }
      },
      {
        path: 'incentives/add/:incentiveType',
        component: CrudIncentiveComponent,
        //canActivate: [MyAuthGuard],
        data: { 'page_name': UserAccessConstants.PAGE_INCENTIVES_REWARD }
      },
      {
        path: 'incentives/modify/:incentiveType/:crudType/:id',
        component: CrudIncentiveComponent,
        //canActivate: [MyAuthGuard],
        data: { 'page_name': UserAccessConstants.PAGE_INCENTIVES_REWARD }
      },
      // {
      //   path: 'profile',
      //   loadChildren: './views/profile/profile.module#ProfileModule',
      //   //canActivate: [MyAuthGuard],
      //   data: { 'page_name': UserAccessConstants.PAGE_PROFILE, 'is_listing': true }
      // },

      {
        path: 'profile/myprofile',
        component: MyprofileComponent,
        //canActivate: [MyAuthGuard],
        data: { 'page_name': UserAccessConstants.PAGE_PROFILE_PERSONAL }
      },
      {
        path: 'profile/businessprofile',
        component: BusinessprofileComponent,
        //canActivate: [MyAuthGuard],
        data: { 'page_name': UserAccessConstants.PAGE_PROFILE_COMPANY }
      },



      // {
      //   path: 'setup',
      //   loadChildren: './views/setup/setup.module#SetupModule',
      //   //canActivate: [MyAuthGuard],
      //   data: { 'page_name': UserAccessConstants.PAGE_SETUP, 'is_listing': true }
      // },
      {
        path: 'setup/companyCategory',
        component: CompanyCategoryComponent,
        //canActivate: [MyAuthGuard],
        data: { 'page_name': UserAccessConstants.PAGE_SETUP_COMPANY_CATEGORY }
      },
      {
        path: 'setup/organizationUsers',
        component: OrganizationUsersComponent,
        //canActivate: [MyAuthGuard],
        data: { 'page_name': UserAccessConstants.PAGE_SETUP_ORGANIZATION_USERS }
      },

      // {
      //   path: 'configuration',
      //   loadChildren: './views/configuration/configuration.module#ConfigurationModule',
      //   //canActivate: [MyAuthGuard],
      //   data: { 'page_name': UserAccessConstants.PAGE_CONFIGURATION, 'is_listing': true }
      // },
      {
        path: 'configuration/assignPermission',
        component: AssignPermissionComponent,
        //canActivate: [MyAuthGuard],
        data: { 'page_name': UserAccessConstants.PAGE_CONFIGURATION_ASSIGN_PERMISSION }
      },
      {
        path: 'configuration/companyCategoryList',
        component: CompanyCategoryListComponent,
        //canActivate: [MyAuthGuard],
        data: { 'page_name': UserAccessConstants.PAGE_CONFIGURATION_ASSIGN_PERMISSION }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }