import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { MyAuthService } from './service/myauth.service';
import { Globals } from './others/models/Globals';
import { AccessService } from './service/access.service';


@Injectable()
export class MyAuthGuard implements CanActivate {

  constructor(
    private _router: Router,
    private _myauthService: MyAuthService, 
    private _accessService: AccessService
  ) { }

  // canActivate() : boolean {
  //   // console.log("ccccccccccccccc");
  //   if(this._myauthService.loggedIn() && Globals.getVendorId() != ""){
  //     return true;
  //   } else {
  //     this._router.navigate([""]);
  //     return false;
  //   }
  // }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this._myauthService.loggedIn() && Globals.getVendorId() != "") {
      //set page permissions
      let page_name = route.data["page_name"];
      let is_listing = route.data["is_listing"];
      console.log("### pageName: " + page_name);
      console.log("### is_listing: " + is_listing);

      if (is_listing) return true;//need to work on this

      let pagePermission = this._accessService.getCurrentPagePermission(page_name);
      if (!pagePermission) {
        alert("You are not authorized to view this page");
        this._myauthService.logout();
        this._router.navigate([""]);
        return false;
      }
      Globals.setCurrentPagePermission(this._accessService.getCurrentPagePermission(page_name));
      return true;
    } else {
      this._myauthService.logout();
      this._router.navigate([""]);
      return false;
    }
  }


} 
