import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LocationStrategy, HashLocationStrategy, CommonModule } from '@angular/common';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

import { AppComponent } from './app.component';

// Import containers
import { DefaultLayoutComponent } from './containers';

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';

const APP_CONTAINERS = [
  DefaultLayoutComponent
];

import {
  AppAsideModule,
  AppBreadcrumbModule,
  AppHeaderModule,
  AppFooterModule,
  AppSidebarModule,
} from '@coreui/angular';

// Import routing module
import { AppRoutingModule } from './app.routing';

// Import 3rd party components
import { BsDropdownModule, TypeaheadModule, ModalBackdropComponent } from 'ngx-bootstrap';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

import { ServerService } from './service/server.service';
import { MyAuthService } from './service/myauth.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { PapaParseModule } from 'ngx-papaparse';
import { CrudIncentiveComponent } from './views/crud-incentive/crud-incentive.component';
import { IncentivesComponent } from './views/incentives/incentives.component';
import { SocialLoginModule, AuthServiceConfig, FacebookLoginProvider, GoogleLoginProvider } from 'angular-6-social-login';
import { MyAuthGuard } from './myauth.guard';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { ToastrModule } from 'ngx-toastr';
import { AccessService } from './service/access.service';
import { NewcampaignComponent } from './views/newcampaign/newcampaign.component';
import { MyprofileComponent } from './views/profile/myprofile-component';
import { SharedModule } from './shared/shared.module';
import { ShowSingleDocumentComponent } from './views/legalDocument/showSingleDocument.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ModalContainerComponent } from 'ngx-bootstrap/modal';

@NgModule({
  imports: [
    SharedModule,
    BrowserModule,
    AppRoutingModule,
    AppAsideModule,
    AppBreadcrumbModule.forRoot(),
    AppFooterModule,
    AppHeaderModule,
    AppSidebarModule,
    PerfectScrollbarModule,
    BsDropdownModule.forRoot(),
    TabsModule.forRoot(),
    ChartsModule,
    HttpClientModule,
    PapaParseModule,
    BsDatepickerModule.forRoot(),
    TypeaheadModule.forRoot(),
    SocialLoginModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      timeOut: 3000,
      positionClass: 'toast-top-center'
    }),
    PdfViewerModule

  ],
  declarations: [
    AppComponent,
    ...APP_CONTAINERS,
    P404Component,
    P500Component,
    LoginComponent,
    RegisterComponent,
    IncentivesComponent,
    CrudIncentiveComponent,
    ShowSingleDocumentComponent,
    ModalBackdropComponent,
    ModalContainerComponent

  ],
  providers: [
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    },
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy,
    },
    ServerService,
    MyAuthService,
    MyAuthGuard,
    AccessService,
    BsModalService
  ],
  bootstrap: [AppComponent],
  entryComponents: [ModalBackdropComponent, ModalContainerComponent,]
})
export class AppModule { }

export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
    [
      {
        id: FacebookLoginProvider.PROVIDER_ID,
        provider: new FacebookLoginProvider("My-Facebook-app-id")
      },
      {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider("15250737000-19chnc1jti560qblfcgt3o4am7s6dj27.apps.googleusercontent.com")
      },
    ]
  );
  return config;
}
